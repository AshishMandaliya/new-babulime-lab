//
//  SecureViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 02/10/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit
import LocalAuthentication

class SecureViewController: BaseViewController {
    
    @IBOutlet weak var lockedView: UIView!
    
    @IBOutlet weak var lblTitleLocked: UILabel!
    @IBOutlet weak var lblTitleLockedMsg: UILabel!
    @IBOutlet weak var btnUseTouchId: UIButton!
    
    var dismissVC: ()->() = {}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
        setLanguage()
        self.lockedView.isHidden = true
        touchID()
    }
    
    fileprivate func setLanguage(){
        self.lblTitleLocked.text = self.getLanguageData(strKey: "titleLocked")
        self.lblTitleLockedMsg.text = self.getLanguageData(strKey: "titleLockedMsg")
        self.btnUseTouchId.setTitle(self.getLanguageData(strKey: "btnTitleTouchID"), for: .normal)
    }
    
    @IBAction func onClickUseTouchId(_ sender: UIButton){
        self.lockedView.isHidden = true
        self.touchID()
    }
    
    fileprivate func touchID(){
        let context = LAContext()
        var error: NSError?

        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Touch ID is required to use BABU LIME LAB.!"

            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [weak self] success, authenticationError in

                DispatchQueue.main.async {
                    if success {
                        // navigate
                        self?.dismissVC()
                        
                    } else {
                        // error
//                        self?.alert(title: "Authentication failed", message: "You could not be verified; please try again.")
                        self?.lockedView.isHidden = false
                    }
                }
            }
        } else {
            // no biometry
            self.alert(title: "Biometry unavailable", message: "Your device is not configured for biometric authentication.") {
                self.dismissVC()
            }
        }
    }
}

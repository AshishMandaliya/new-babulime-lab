//
//  ServerDownViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 15/10/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class ServerDownViewController: BaseViewController {
    
    @IBOutlet weak var mainView: RefreshableView!
    @IBOutlet weak var lblTitleServerDown: UILabel!
    @IBOutlet weak var lblTitleServerDownMsg: UILabel!
    @IBOutlet weak var btnContactUs: UIButton!
    
    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLanguage()
        mainView.setupUI()

        mainView.refreshFunction = {
            print("simulating refresh for 2 seconds...")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                [weak self] in
                guard let self = self else { return }
                // do what you want to update the contents of theRefreshableView
                // ...
                // then tell theRefreshableView to endRefreshing
                API.shared.serverCheckAPI { (msg, err) in
                    if let msg = msg{
                        print(msg)
                        self.mainView.endRefreshing()
                        self.view.removeFromSuperview()
                        
                       if  UserData.shared.getUser().userInfo?.is_admin == 1
                        {
                            UserData.shared.getUser().isLogin ? appDelegate.rootToAdminHome() : appDelegate.rootToLogin()
                        }
                        
                        else
                       {
                        UserData.shared.getUser().isLogin ? appDelegate.rootToHome() : appDelegate.rootToLogin()
                       }
                        
                    }else{
                        self.mainView.endRefreshing()
                    }
                }
                
                
            }
        }
            
    }
    
    fileprivate func setLanguage(){
        self.lblTitleServerDown.text = self.getLanguageData(strKey: "titleServerDown")
        self.lblTitleServerDownMsg.text = self.getLanguageData(strKey: "titleServerDownMsg")
        self.btnContactUs.setTitle(self.getLanguageData(strKey: "btnTitleContactUs"), for: .normal)
    }
}

class RefreshableView : UIView {

    @IBOutlet var scrollView: UIScrollView!

    private let refreshControl = UIRefreshControl()

    var refreshFunction: (() -> ())?

    func setupUI() {

        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)

        scrollView.refreshControl = refreshControl
    }

    @objc func didPullToRefresh() {
        print("calling refreshFunction in controller")
        self.refreshFunction?()
    }

    func endRefreshing() {
        print("end refreshing called from controller")
        self.refreshControl.endRefreshing()
    }

}

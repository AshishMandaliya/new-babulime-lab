//
//  SplitsViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 07/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class SplitsViewController: UISplitViewController, UISplitViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        if AuthenticationManager.sharedInstance.needsAuthentication{
            willEnterForeground()
        }
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @objc func willEnterForeground(){
        let tView = UIView(frame: self.view.frame)
        tView.backgroundColor = .clear
        tView.isUserInteractionEnabled = false
        self.view.addSubview(tView)
        
        if AuthenticationManager.sharedInstance.needsAuthentication{
            let vc: SecureViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            self.add(vc, parent: self.view)
            vc.dismissVC = {
                vc.view.removeFromSuperview()
                vc.removeFromParent()
                AuthenticationManager.sharedInstance.needsAuthentication = false
//                self.splitViewController?.viewWillAppear(true)
                self.viewWillAppear(true)
            }
        }
    }
    
    @objc func serverDownVC(){
        print("serverDown")
    }
    
    
}

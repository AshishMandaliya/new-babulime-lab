//
//  ListExpViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 14/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class ListExpViewController: BaseViewController {
    
    @IBOutlet weak var txtDate: UITextField!{
        didSet{
            self.txtDate.setLeftIcon(#imageLiteral(resourceName: "calendar"))
        }
    }
    
    @IBOutlet weak var tblListExperiment: UITableView!{
        didSet{
            self.tblListExperiment.delegate = self
            self.tblListExperiment.dataSource = self
        }
    }
    @IBOutlet weak var lblCountExperiment: UILabel!
    @IBOutlet weak var btnNotification: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    var isHiddenMenu = false
    var dataArray: [dictionary] = []
    var data: dictionary = [:]
    
    let dateTimePicker = UIDatePicker()
    var experiment_id = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLanguage()
        
        //Nav
//        let back = UIBarButtonItem()
//        back.title = getLaunguageData(strKey: "btnBack")
//        self.navigationItem.backBarButtonItem = back
        
        dateTimePicker.datePickerMode = .date
        dateTimePicker.maximumDate = Date()
        dateTimePicker.addTarget(self, action: #selector(onClickDate), for: .valueChanged)
        
        txtDate.inputView = dateTimePicker

        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    @objc func handleInternetActivity() {

        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)! {

            print("Internet Ok")

        } else {

            print("No Internet Available")
            let vc: NoInternetViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.isInternet = {
                self.remove()
            }
            self.add(vc, parent: self.view)
        }
    }
    
    fileprivate func setLanguage(){
        self.lblTitle.text = self.getLanguageData(strKey: "titleChooseExp")
        self.txtDate.placeholder = self.getLanguageData(strKey: "txtAddDate")
    }
    
    
    private func getData(date: String = ""){
        self.startLoader()
        var dic: dictionary = [
            "token": self.userInfo.access_token,
            "experiment_id": self.experiment_id,
        ]
        if !date.isEmpty{
            dic["date"] = date
        }
        API.shared.getExperimentSample(parameters: dic) { (arr) in
            self.stopLoader()
            self.dataArray.removeAll()
            self.dataArray = arr
            self.lblCountExperiment.text = "\(self.dataArray.count) \(self.getLanguageData(strKey: "titleExps"))"
            self.tblListExperiment.reloadData()
        }
    }
    
    // MARK:- Action
    
    @IBAction func onClickMenu(_ sender: UIButton){
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onClickDate(_ sender: UIDatePicker){
        self.txtDate.text = formatDateForDisplay(date: sender.date, format: "MMMM d, yyyy")
        self.getData(date: formatDateForDisplay(date: sender.date, format: "yyyy-MM-dd"))
        self.view.endEditing(true)
    }
    
    fileprivate func formatDateForDisplay(date: Date, format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    

}


// MARK:- UITableViewDelegate, UITableViewDataSource
extension ListExpViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListExpCell", for: indexPath) as? ListExpCell else { return UITableViewCell() }
        
        cell.selectionStyle = .none
        
        cell.lblName.text = dataArray[indexPath.row]["name"] as? String
        cell.lblTime.text = dataArray[indexPath.row]["date"] as? String
        
        cell.btnViewEdit.setTitle(self.getLanguageData(strKey: "btnViewEdit"), for: .normal)
        cell.btnResult.setTitle(self.getLanguageData(strKey: "btnAddResult"), for: .normal)
        
        cell.actionViewEdit = { [weak self] in
            let vc: EditExpViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.experiment_id = self?.experiment_id ?? 0
            vc.experiment_result_id = self?.dataArray[indexPath.row]["id"] as? Int ?? -1
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        cell.actionResult = { [weak self] in
            
            //final result data
            self?.startLoader()
            let dic: dictionary = [
                "token": self?.userInfo.access_token ?? "",
                "experiment_id": self?.experiment_id ?? -1,
                "experiment_result_id": self?.dataArray[indexPath.row]["id"] as? Int ?? -1,
            ]
            
            API.shared.viewExperimentDetail(parameters: dic) { (dataDetail) in
                self?.stopLoader()
                self?.data = dataDetail
                let vc: FinalResultViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
                vc.experiment_id = self?.experiment_id ?? 0
                vc.experiment_result_id = self?.dataArray[indexPath.row]["id"] as? Int ?? -1
                let dic = self?.data["final_result"] as? [dictionary] ?? []
                vc.arrExperiment.append(FinalResult(data: dic.first ?? [:]))
                let arrParameters = vc.arrExperiment[0].data
                vc.arrParameters1 = arrParameters.filter { (dic) -> Bool in
                    if dic["is_actual_result"] as? Bool == true{
                        return true
                    }
                    return false
                }
                vc.arrParameters2 = arrParameters.filter { (dic) -> Bool in
                    if dic["is_actual_result"] as? Bool == false{
                        return true
                    }
                    return false
                }
                vc.isEdit = true
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        cell.actionNotification = { [weak self] in
            self?.alert(title: self?.getLanguageData(strKey: "titleNotification") ?? "", message: self?.getLanguageData(strKey: "alertMsgNotification") ?? "", actions: [self?.getLanguageData(strKey: "alertViewBtn") ?? "", self?.getLanguageData(strKey: "alertNotNowBtn") ?? "", self?.getLanguageData(strKey: "btnCancel") ?? ""], style: [.default, .default, .cancel]) { (index) in
                switch index {
                case 0 :
                    print("View")
                    let dic: dictionary = [
                        "token": self?.userInfo.access_token ?? "",
                        "experiment_id": self?.experiment_id ?? -1,
                        "experiment_result_id": self?.dataArray[indexPath.row]["id"] as? Int ?? -1,
                    ]
                    
                    API.shared.viewExperimentDetail(parameters: dic) { (dataDetail) in
                        self?.stopLoader()
                        self?.data = dataDetail
                        let vc: ReminderCreateViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
                        vc.experiment_id = self?.experiment_id ?? -1
                        vc.isEdit = true
                        vc.data = ViewReminder(data: self?.data["reminder"] as? dictionary ?? [:])
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                case 1 :
                    print("Not Now")
                case 2 :
                    self?.dismiss(animated: true, completion: nil)
                default:
                    self?.dismiss(animated: true, completion: nil)
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}


class ListExpCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnViewEdit: UIButton!{
        didSet{
            self.btnViewEdit.addTarget(self, action: #selector(onClickViewEdit), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnResult: UIButton!{
        didSet{
            self.btnResult.addTarget(self, action: #selector(onClickResult), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnNotification: UIButton!{
        didSet{
            self.btnNotification.addTarget(self, action: #selector(onClickNotification), for: .touchUpInside)
        }
    }
    
    var actionViewEdit: ()->() = {}
    var actionResult: ()->() = {}
    var actionNotification: ()->() = {}
    
    @objc func onClickViewEdit(_ sender: UIButton){
        self.actionViewEdit()
    }
    
    @objc func onClickResult(_ sender: UIButton){
        self.actionResult()
    }
    
    @objc func onClickNotification(_ sender: UIButton){
        self.actionNotification()
    }
}

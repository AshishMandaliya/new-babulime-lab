//
//  CreateEx1ViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 07/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class CreateEx1ViewController: BaseViewController, UITextViewDelegate {
    
    @IBOutlet weak var txtName: UITextField!{
        didSet{
            self.txtName.becomeFirstResponder()
            self.txtName.addTarget(self, action: #selector(txt), for: .editingChanged)
        }
    }
    
    @IBOutlet weak var txtReason: UITextView!{
        didSet{
            self.txtReason.delegate = self
        }
    }

    @IBOutlet weak var txtRecipeType: UITextField!{
        didSet{
            self.txtRecipeType.setRightView(image: #imageLiteral(resourceName: "downArrow"))
        }
    }
    @IBOutlet weak var txtChainExp: UITextField!{
        didSet{
            self.txtChainExp.setRightView(image: #imageLiteral(resourceName: "downArrow"))
        }
    }
    @IBOutlet weak var txtRecipeTypeOther: UITextField!{
        didSet{
            self.txtRecipeTypeOther.addTarget(self, action: #selector(txt), for: .editingChanged)
        }
    }
    @IBOutlet weak var btnNext: UIButton!{
        didSet{
//            self.btnNext.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor?.withAlphaComponent(0.5)], for: .disabled)
//            self.btnNext.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor], for: .normal)
            
            
            self.btnNext.setTitleColor(UIColor.mainColor?.withAlphaComponent(0.5), for: .disabled)
            self.btnNext.setTitleColor(UIColor.mainColor, for: .normal)
            self.btnNext.isEnabled = false
        }
    }
    @IBOutlet weak var segPriority: UISegmentedControl!
    
    @IBOutlet weak var titleNav: UILabel!
    @IBOutlet weak var titleBasicInfo: UILabel!
    @IBOutlet weak var titleRecipeType: UILabel!
    @IBOutlet weak var titlePriority: UILabel!
    @IBOutlet weak var titleChainExp: UILabel!
    
    var isHiddenMenu = false
    var arrRecipe = ["Parcel", "Paste", "Other"]
    var arrChain: [dictionary] = [
        [
            "id": 0,
            "name": "None",
        ]
    ]
    var experiment_id: Int = 0
    var exp_id: Int = 0
    let pickerView1 = UIPickerView()
    let pickerView2 = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        let back = UIBarButtonItem()
//        back.title = getLaunguageData(strKey: "titleBasicInfo")
//        self.navigationItem.backBarButtonItem = back
//
//        let left = UIBarButtonItem()
//        left.image = #imageLiteral(resourceName: "collapsible")
//        self.navigationItem.leftBarButtonItems = [back,left]
        
        
        
        pickerView1.delegate = self
        pickerView1.dataSource = self
        
        pickerView2.delegate = self
        pickerView2.dataSource = self

        txtRecipeType.delegate = self
        txtChainExp.delegate = self

        txtRecipeType.inputView = pickerView1
        txtChainExp.inputView = pickerView2
        txtChainExp.text = arrChain[pickerView2.selectedRow(inComponent: 0)]["name"] as? String ?? ""
        
        API.shared.getChainExperiments(parameters: ["token": userInfo.access_token]) { (data) in
            self.arrChain.append(contentsOf: data.map{$0})
        }
        
        setLanguage()
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    @objc func handleInternetActivity() {

        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)! {

            print("Internet Ok")

        } else {

            print("No Internet Available")
            let vc: NoInternetViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.isInternet = {
                self.remove()
            }
            self.add(vc, parent: self.view)
        }
    }
    
    fileprivate func setLanguage(){
        self.titleNav.text = getLanguageData(strKey: "titleCreateExp")
//        self.title = getLaunguageData(strKey: "titleCreateExp")
        self.txtName.placeholder = getLanguageData(strKey: "txtNameOfProjSample")
        self.txtReason.placeholder = getLanguageData(strKey: "txtReason")
        self.titleBasicInfo.text = getLanguageData(strKey: "titleBasicInfo")
        self.titleRecipeType.text = getLanguageData(strKey: "titleRecipeType")
        self.titlePriority.text = getLanguageData(strKey: "titlePriority")
        self.titleChainExp.text = getLanguageData(strKey: "titleChain")
        self.btnNext.setTitle(getLanguageData(strKey: "btnTitleNavNext"), for: .normal)
        self.btnNext.setTitle(getLanguageData(strKey: "btnTitleNavNext"), for: .disabled)
    }
    
    @objc func txt(){
        if txtRecipeTypeOther.isHidden == false {
            self.btnNext.isEnabled = txtName.text?.isEmpty == false && txtReason.text?.isEmpty == false && txtRecipeTypeOther.text?.isEmpty == false
        }else {
            self.btnNext.isEnabled = txtName.text?.isEmpty == false && txtReason.text?.isEmpty == false
        }
        
    }
    func textViewDidChange(_ textView: UITextView) {
        txt()
    }
    
    @IBAction func onClickMenu(_ sender: UIButton){
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickNext(_ sender: UIButton){
        let recipeType = txtRecipeType.text == "Other" ? txtRecipeTypeOther.text! : txtRecipeType.text!
        self.startLoader()
        var dic: dictionary = [
            "user_id": userId,
            "name": txtName.text ?? "",
            "types_of_recipe": recipeType,
            "priority": segPriority.titleForSegment(at: segPriority.selectedSegmentIndex) ?? "Urgent",
            "reason for development": txtReason.text ?? "",
            "token": self.userInfo.access_token,
            "chain_experiment": pickerView2.selectedRow(inComponent: 0) == 0 ? [:] : arrChain[pickerView2.selectedRow(inComponent: 0)]
        ]
        API.shared.createExpBasic(parameters: dic) { (data) in
            self.stopLoader()
            self.experiment_id = data["id"] as? Int ?? 0
            let vc: CreateEx2ViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.experiment_id = self.experiment_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}

extension CreateEx1ViewController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerView1{
            return arrRecipe.count
        }else {
            return arrChain.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerView1{
            return arrRecipe[row]
        }else {
            return arrChain[row]["name"] as? String ?? ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerView1{
            let str = arrRecipe[row]
            self.txtRecipeType.text = str
            if txtRecipeType.text == "Other" {
                self.txtRecipeTypeOther.isHidden = false
            } else {
                self.txtRecipeTypeOther.isHidden = true
            }
        }else {
            let str = arrChain[row]["name"] as? String ?? ""
            exp_id = arrChain[row]["id"] as? Int ?? 0
            self.txtChainExp.text = str
        }
        self.view.endEditing(true)
    }
    

}

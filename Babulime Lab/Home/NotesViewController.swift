//
//  NotesViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 27/10/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class Notes{
    var id: Int
    var experiment_id: Int
    var slug: String
    var type: String
    var note: String
    var image1: String
    var image2: String
    var image3: String
    var drawing: String
    
    init(data: dictionary){
        self.id = data["id"] as? Int ?? -1
        self.experiment_id = data["experiment_id"] as? Int ?? -1
        self.slug = data["slug"] as? String ?? ""
        self.type = data["type"] as? String ?? ""
        self.note = data["note"] as? String ?? ""
        self.image1 = data["image1"] as? String ?? ""
        self.image2 = data["image2"] as? String ?? ""
        self.image3 = data["image3"] as? String ?? ""
        self.drawing = data["drawing"] as? String ?? ""
        
    }
}

class NotesViewController: BaseViewController, UIPopoverPresentationControllerDelegate {
    
    var isRemove: (()->())?
    @IBOutlet weak var btnOption: UIButton!
    @IBOutlet weak var txtNotes: UITextView!
    @IBOutlet weak var collectionImageView: UICollectionView!{
        didSet{
            self.collectionImageView.delegate = self
            self.collectionImageView.dataSource = self
        }
    }
    @IBOutlet weak var drawView: Canvas!
    @IBOutlet weak var btnAddNotes: UIButton!
    
    var arrImage: [UIImage] = []
    var maxLimit: Int = 3
    var screenName = ""
    var experiment_id = 0
    var note = Notes(data: [:])
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        drawView.setStrokeColor(color: .black)
        setUpUI()
        
    }
    
    func getData(){
        let dic: dictionary = [
            "experiment_id": experiment_id,
            "token": userInfo.access_token,
            "slug": screenName,
        ]
        
        
    }
    
    fileprivate func setUpUI(){
        if btnOption.title(for: .normal) == "Text"{
            self.txtNotes.isHidden = false
            self.collectionImageView.isHidden = true
            self.drawView.isHidden = true
        }else if btnOption.title(for: .normal) == "Image"{
            self.txtNotes.isHidden = true
            self.collectionImageView.isHidden = false
            self.drawView.isHidden = true
        }else if btnOption.title(for: .normal) == "Drawing"{
            self.txtNotes.isHidden = true
            self.collectionImageView.isHidden = true
            self.drawView.isHidden = false
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        self.isRemove?()
    }
    
    @IBAction func onClickOption(_ sender: UIButton) {
        NotesOption().pickOption(self, view: self.btnOption) { (str) in
            self.btnOption.setTitle(str, for: .normal)
            self.setUpUI()
        }
    }

    @IBAction func onClickAddNote(_ sender: UIButton){
        var postImage: [UIImage] = []
        var imageParam: [String] = []
        var dic: dictionary = [
            "experiment_id": experiment_id,
            "token": userInfo.access_token,
            "slug": screenName,
            "type": self.btnOption.title(for: .normal) ?? "",
        ]
        if self.btnOption.title(for: .normal) == "Text"{
            dic["notes"] = self.txtNotes.text ?? ""
        }
        
        if self.btnOption.title(for: .normal) == "Image" {
            postImage = arrImage
            arrImage.enumerated().forEach({imageParam.append("image\($0.offset+1)")})
            print(imageParam)
        }
        
        if self.btnOption.title(for: .normal) == "Drawing"{
            UIGraphicsBeginImageContext(self.drawView.frame.size)
            self.drawView.layer.render(in: UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            postImage = [image ?? UIImage()]
            imageParam = ["drawing"]
        }
        
        API.shared.addNotes(vc: self, param: dic, postImage: postImage, imageName: imageParam, imageParam: imageParam) { (data) in
            let message = data["msg"] as? String ?? ""
            print(message)
            self.isRemove?()
        }
    }
}


extension NotesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImage.count < maxLimit ? self.arrImage.count + 1 : self.arrImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == self.arrImage.count {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "customNotesImageCell", for: indexPath) as? customNotesImageCell else { return UICollectionViewCell() }
            cell.imgView.image = #imageLiteral(resourceName: "plus1")
            return cell
        }else{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "customNotesImageCell", for: indexPath) as? customNotesImageCell else { return UICollectionViewCell() }
            cell.imgView.image = arrImage[indexPath.row]
            
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == self.arrImage.count{
            ImagePickerManager().pickImage(self, view: collectionView.cellForItem(at: indexPath)!) { (img) in
                self.arrImage.append(img)
                collectionView.reloadData()
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width - 24)/3
        return CGSize(width: width, height: width)
    }
    
    
}

class customNotesImageCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
}

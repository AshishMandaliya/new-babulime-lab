//
//  ReminderCreateViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 10/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class ReminderCreateViewController: BaseViewController, UITextViewDelegate {

    @IBOutlet weak var backMainView: UIView!
    @IBOutlet weak var txtDate: UITextField!{
        didSet{
            self.txtDate.setLeftIcon(#imageLiteral(resourceName: "calendar"))
        }
    }
    @IBOutlet weak var txtRepeat: UITextField!{
        didSet{
            self.txtRepeat.setLeftIcon(#imageLiteral(resourceName: "repeat"))
        }
    }
    @IBOutlet weak var btnNext: UIButton!{
        didSet{
//            self.btnNext.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor?.withAlphaComponent(0.5)], for: .disabled)
//            self.btnNext.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor], for: .normal)
            self.btnNext.setTitleColor(UIColor.mainColor?.withAlphaComponent(0.5), for: .disabled)
            self.btnNext.setTitleColor(UIColor.mainColor, for: .normal)
        }
    }
    
    @IBOutlet weak var txtFrequency: UITextField!
    @IBOutlet weak var txtEvery: UITextField!{
        didSet{
            self.txtEvery.addTarget(self, action: #selector(nextValidation), for: .editingChanged)
        }
    }
    @IBOutlet weak var btnResult: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var txtResult: UITextView!{
        didSet{
            self.txtResult.delegate = self
        }
    }
    @IBOutlet weak var stcCustom: UIStackView!
    @IBOutlet weak var stcFrequency: UIStackView!
    @IBOutlet weak var stcEvery: UIStackView!

    @IBOutlet weak var titleNav: UILabel!
    @IBOutlet weak var lblReminder: UILabel!
    @IBOutlet weak var btnGetReminder: UIButton!
    @IBOutlet weak var btnCheckReminder: UIButton!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblRepeat: UILabel!
    @IBOutlet weak var btnResultOptional: UIButton!
    @IBOutlet weak var lblFrequency: UILabel!
    @IBOutlet weak var lblEvery: UILabel!
    @IBOutlet weak var stcReminder: UIStackView!
    
    var isHiddenMenu = false
    var experiment_id = 0
    var arrRepeat = ["None", "Hourly", "Every Day", "Every Week", "Every Month", "Custom"]
    var arrFrequency = ["Daily", "Weekly", "Monthly", "Yearly"]
    var selectedDate = ""
    let pickerView1 = UIPickerView()
    let pickerView2 = UIPickerView()
    let dateTimePicker = UIDatePicker()
    var isEdit = false
    var data = ViewReminder(data: [:])
    var isReminder = true
    var isResult = true

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLanguage()
        
        pickerView1.delegate = self
        pickerView1.dataSource = self
        
        pickerView2.delegate = self
        pickerView2.dataSource = self
        
        dateTimePicker.datePickerMode = .dateAndTime
        dateTimePicker.minimumDate = Date()
        dateTimePicker.addTarget(self, action: #selector(onClickDate), for: .editingDidEnd)
        
//        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(onClickDate(_:)))
//
//            txtDate.inputAccessoryView = toolBar
        
        
        if #available(iOS 13.4, *) {
            dateTimePicker.preferredDatePickerStyle = .compact
        } else {
            // Fallback on earlier versions
        }
        txtRepeat.inputView = pickerView1
        txtDate.inputView = dateTimePicker
        txtFrequency.inputView = pickerView2
        
        self.btnCheck.setImage(!self.isResult ? UIImage() : #imageLiteral(resourceName: "check"), for: .normal)
//        self.btnCheck.isHidden = !isResult
        self.txtResult.isHidden = !isResult
        
        self.stcReminder.isHidden = !isReminder
//        self.btnCheckReminder.isHidden = !isReminder
        self.btnCheckReminder.setImage(!self.isReminder ? UIImage() : #imageLiteral(resourceName: "check"), for: .normal)
        
        if isEdit{
//            self.btnNext.title = "UPDATE"
//            self.title = "Edit Reminder"
            
            isReminder = data.is_reminder_set
            isResult = data.is_reminder_result
            
            self.txtRepeat.text = data.repeatt
            self.txtDate.text = data.datetime
            if txtRepeat.text == "Custom"{
                self.stcFrequency.isHidden = false
                self.stcEvery.isHidden = false
                self.txtFrequency.text = data.custom_repeat
                self.txtEvery.text = data.number_of_days
                if txtFrequency.text?.isEmpty == true {
                    self.txtFrequency.text = self.arrFrequency[0]
                }
            }else{
                self.stcFrequency.isHidden = true
                self.stcEvery.isHidden = true
            }
            self.txtResult.text = data.expected_result_reminder
            
            self.stcReminder.isHidden = !isReminder
            self.btnCheckReminder.setImage(!self.isReminder ? UIImage() : #imageLiteral(resourceName: "check"), for: .normal)
            
            self.btnCheck.setImage(!self.isResult ? UIImage() : #imageLiteral(resourceName: "check"), for: .normal)
            self.txtResult.isHidden = !isResult
            self.nextValidation()
//            selectedDate = Date()
        }else {
            
            let dic: dictionary = [
                "token": self.userInfo.access_token,
                "experiment_id": self.experiment_id,
            ]
            
            API.shared.getReminder(parameters: dic) { (dic) in
                self.data = ViewReminder(data: dic)
                self.isReminder = self.data.is_reminder_set
                self.isResult = self.data.is_reminder_result
                
                if !self.isReminder && !self.isResult{
                    self.isReminder = true
                }
                
                self.txtRepeat.text = self.data.repeatt
                self.txtDate.text = self.data.datetime
                if self.txtRepeat.text == "Custom"{
                    self.stcFrequency.isHidden = false
                    self.stcEvery.isHidden = false
                    self.txtFrequency.text = self.data.custom_repeat
                    self.txtEvery.text = self.data.number_of_days
                    if self.txtFrequency.text?.isEmpty == true {
                        self.txtFrequency.text = self.arrFrequency[0]
                    }
                }else{
                    self.stcFrequency.isHidden = true
                    self.stcEvery.isHidden = true
                }
                self.txtResult.text = self.data.expected_result_reminder
                
                self.stcReminder.isHidden = !self.isReminder
//                self.btnCheckReminder.isHidden = !self.isReminder
                self.btnCheckReminder.setImage(!self.isReminder ? UIImage() : #imageLiteral(resourceName: "check"), for: .normal)
                
                self.btnCheck.setImage(!self.isResult ? UIImage() : #imageLiteral(resourceName: "check"), for: .normal)
                
//                self.btnCheck.isHidden = !self.isResult
                self.txtResult.isHidden = !self.isResult
                self.nextValidation()
            }
            
//            txtRepeat.text = arrRepeat[0]
//            txtFrequency.text = arrFrequency[0]
//            txtDate.text = formatDateForDisplay(date: Date(), format: "MMMM d, yyyy h:mm a")
        }
        selectedDate = formatDateForDisplay(date: Date(), format: "dd-MM-yyyy HH:mm:ss")
        
        if isReminder == false && isResult == false{
            self.isReminder.toggle()
            self.stcReminder.isHidden = !isReminder
//            self.btnCheckReminder.isHidden = !isReminder
            self.btnCheckReminder.setImage(!self.isReminder ? UIImage() : #imageLiteral(resourceName: "check"), for: .normal)
            
        }
        nextValidation()

    }
    
    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        nextValidation()
    }
    
    @objc func handleInternetActivity() {

        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)! {

            print("Internet Ok")

        } else {

            print("No Internet Available")
            let vc: NoInternetViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.isInternet = {
                self.remove()
            }
            self.add(vc, parent: self.view)
        }
    }
    
    fileprivate func setLanguage(){
//        self.btnNext.title = isEdit ? self.getLaunguageData(strKey: "btnTitleNavUpdate") : self.getLaunguageData(strKey:"btnTitleNavNext")
//        self.title = isEdit ? self.getLaunguageData(strKey: "titleEditReminder") : ""
        self.titleNav.text = isEdit ? self.getLanguageData(strKey: "titleEditReminder") : ""
        self.lblReminder.text = self.getLanguageData(strKey: "titleReminder")
//        self.lblGetReminder.text = self.getLaunguageData(strKey: "titleResultReminder")
        self.btnGetReminder.setTitle(self.getLanguageData(strKey: "titleResultReminder"), for: .normal)
        self.lblDateTime.text = self.getLanguageData(strKey: "titleDateTime")
        self.lblRepeat.text = self.getLanguageData(strKey: "titleRepeat")
        self.btnResultOptional.setTitle(self.getLanguageData(strKey: "titleResultOptional"), for: .normal)
        self.txtResult.placeholder = self.getLanguageData(strKey: "txtResultOptional")
        self.lblFrequency.text = self.getLanguageData(strKey: "titleFrequency")
        self.lblEvery.text = self.getLanguageData(strKey: "titleEvery")
        self.btnNext.setTitle(isEdit ? self.getLanguageData(strKey: "btnTitleNavUpdate") : self.getLanguageData(strKey:"btnTitleNavNext"), for: .normal)
        self.btnNext.setTitle(isEdit ? self.getLanguageData(strKey: "btnTitleNavUpdate") : self.getLanguageData(strKey:"btnTitleNavNext"), for: .disabled)
    }
    
    @objc func nextValidation(){
        self.btnNext.isEnabled = false
        if isReminder && isResult{
            if txtRepeat.text == "Custom"{
                self.btnNext.isEnabled = self.txtEvery.text?.isEmpty == false && self.txtFrequency.text?.isEmpty == false && self.txtResult.text.isEmpty == false
            }else{
                self.btnNext.isEnabled = self.txtResult.text.isEmpty == false
            }
        }else if isReminder {
            if txtRepeat.text == "Custom"{
                self.btnNext.isEnabled = self.txtEvery.text?.isEmpty == false && self.txtFrequency.text?.isEmpty == false
            }else{
                self.btnNext.isEnabled = true
            }
        }else if isResult{
            self.btnNext.isEnabled = self.txtResult.text.isEmpty == false
        }else{
            self.btnNext.isEnabled = false
        }
    }
    
    @IBAction func onClickResult(_ sender: UIButton){
        if isReminder{
            self.isResult.toggle()
//            self.btnCheck.isHidden = !isResult
            
            self.btnCheck.setImage(!self.isResult ? UIImage() : #imageLiteral(resourceName: "check"), for: .normal)
            self.txtResult.isHidden = !isResult
        }else{
            print("not working")
        }
        nextValidation()
    }
    
    @IBAction func onClickGetReminder(_ sender: UIButton){
        if isResult{
            self.isReminder.toggle()
            self.stcReminder.isHidden = !isReminder
//            self.btnCheckReminder.isHidden = !isReminder
            self.btnCheckReminder.setImage(!self.isReminder ? UIImage() : #imageLiteral(resourceName: "check"), for: .normal)
        }else{
            print("not working")
        }
        nextValidation()
    }
    
    @IBAction func onClickNotes(_ sender: UIButton){
        let vc: NotesViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.screenName = "Reminder"
        vc.isRemove = {
            self.remove()
        }
        self.add(vc, parent: self.backMainView)
    }
    
    @IBAction func onClickMenu(_ sender: UIButton){
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        if isEdit{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.alert(title: "Draft", message: "Do you want to save information as draft?", actions: ["Cancel","Save"]) { (index) in
                if index == 0{
                    let dic: dictionary = [
                        "experiment_id": self.experiment_id,
                        "token": self.userInfo.access_token,
                    ]
                    API.shared.removeReminder(parameters: dic) { (msg) in
                        print(msg)
                        self.navigationController?.popViewController(animated: true)
                    }
                }else if index == 1{
                    if self.btnNext.isEnabled == true{
                        var dic: dictionary = [
                            "experiment_id": self.experiment_id,
                            "token": self.userInfo.access_token,
                        ]
                        
                        if self.isReminder{
                            
                            dic["datetime"] = self.selectedDate
                            
                            switch self.txtRepeat.text {
                            case "Hourly":
                                dic["repeat"] = "hourly"
                            case "Every Day":
                                dic["repeat"] = "everyday"
                            case "Every Week":
                                dic["repeat"] = "every_week"
                            case "Every Month":
                                dic["repeat"] = "every_month"
                            case "Custom":
                                dic["repeat"] = "custom"
                            default:
                                dic["repeat"] = "none"
                            }
                            
                            if self.txtRepeat.text == "Custom"{
                                switch self.txtFrequency.text {
                                case "Weekly":
                                    dic["custom_repeat"] = "weekly"
                                case "Monthly":
                                    dic["custom_repeat"] = "monthly"
                                case "Yearly":
                                    dic["custom_repeat"] = "yearly"
                                default:
                                    dic["custom_repeat"] = "daily"
                                }
                                dic["number_of_days"] = self.txtEvery.text ?? "1"
                            }
                        }
                        
                        if self.isResult{
                            if !self.txtResult.text.isEmpty{
                                dic["expected_result_reminder"] = self.txtResult.text
                            }
                        }
                        
                        dic["is_reminder_set"] = self.isReminder
                        dic["is_reminder_result"] = self.isResult
                        
                        API.shared.setReminder(parameters: dic) { (msg) in
                            self.stopLoader()
                            print(msg)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }else{
                        self.dismiss(animated: true) {
                            self.alert(title: "Validation", message: "To save as draft fill up all required fields.")
                        }
                    }
                }
            }
        }
    }

    @IBAction func onClickDate(_ sender: UIDatePicker){
        self.txtDate.text = formatDateForDisplay(date: sender.date, format: "MMMM d, yyyy h:mm a")
        selectedDate = formatDateForDisplay(date: sender.date, format: "dd-MM-yyyy HH:mm:ss")
//        self.view.endEditing(true)
    }
    
    @IBAction func onClickNext(_ sender: UIButton){
//        self.startLoader()
        var dic: dictionary = [
            "experiment_id": experiment_id,
            "token": userInfo.access_token,
        ]
        
        if isReminder{
            
            dic["datetime"] = selectedDate
            
            switch txtRepeat.text {
            case "Hourly":
                dic["repeat"] = "hourly"
            case "Every Day":
                dic["repeat"] = "everyday"
            case "Every Week":
                dic["repeat"] = "every_week"
            case "Every Month":
                dic["repeat"] = "every_month"
            case "Custom":
                dic["repeat"] = "custom"
            default:
                dic["repeat"] = "none"
            }
            
            if txtRepeat.text == "Custom"{
                switch txtFrequency.text {
                case "Weekly":
                    dic["custom_repeat"] = "weekly"
                case "Monthly":
                    dic["custom_repeat"] = "monthly"
                case "Yearly":
                    dic["custom_repeat"] = "yearly"
                default:
                    dic["custom_repeat"] = "daily"
                }
                dic["number_of_days"] = self.txtEvery.text ?? "1"
            }
        }
        
        if isResult{
            if !txtResult.text.isEmpty{
                dic["expected_result_reminder"] = txtResult.text
            }
        }
        
        dic["is_reminder_set"] = isReminder
        dic["is_reminder_result"] = isResult
        
        API.shared.setReminder(parameters: dic) { (msg) in
            self.stopLoader()
            print(msg)
            if self.isEdit{
                self.navigationController?.popViewController(animated: true)
            }else {
                let vc: FinalResultViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
                vc.experiment_id = self.experiment_id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    fileprivate func formatDateForDisplay(date: Date, format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
}

extension ReminderCreateViewController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerView1{
            return arrRepeat.count
        }else{
            return arrFrequency.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerView1{
            return arrRepeat[row]
        }else{
            return arrFrequency[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerView1{
            self.txtRepeat.text = arrRepeat[row]
            if txtRepeat.text == "Custom"{
                self.stcFrequency.isHidden = false
                self.stcEvery.isHidden = false
            }else{
                self.stcFrequency.isHidden = true
                self.stcEvery.isHidden = true
            }
        }else {
            self.txtFrequency.text = arrFrequency[row]
        }
        self.view.endEditing(true)
        self.nextValidation()
    }
    

}


//extension UIToolbar {
//
//func ToolbarPiker(mySelect : Selector) -> UIToolbar {
//
//    let toolBar = UIToolbar()
//
//    toolBar.barStyle = UIBarStyle.default
//    toolBar.isTranslucent = true
//    toolBar.tintColor = UIColor.black
//    toolBar.sizeToFit()
//
//    let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: mySelect)
//    let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//
//    toolBar.setItems([ spaceButton, doneButton], animated: false)
//    toolBar.isUserInteractionEnabled = true
//
//    return toolBar
//}
//
//}

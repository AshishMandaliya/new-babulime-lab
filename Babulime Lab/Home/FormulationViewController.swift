//
//  FormulationViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 10/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class FormulationStep{
    var id: Int
    var experiment_id: String
    var step: String
    var ingredient_name_or_code: String
    var ingredient_id: Int
    var quantity: String
    var created_at: String
    var updated_at: String
    
    init(data: dictionary){
        self.id = data["id"] as? Int ?? -1
        self.experiment_id = data["experiment_id"] as? String ?? ""
        self.step = data["step"] as? String ?? ""
        self.ingredient_name_or_code = data["ingredient_name_or_code"] as? String ?? ""
        self.ingredient_id = data["ingredient_id"] as? Int ?? -1
        self.quantity = data["quantity"] as? String ?? ""
        self.created_at = data["created_at"] as? String ?? ""
        self.updated_at = data["updated_at"] as? String ?? ""
        
    }
}

class FormulationViewController: BaseViewController {
    
    @IBOutlet weak var tblSteps: UITableView!{
        didSet{
            self.tblSteps.delegate = self
            self.tblSteps.dataSource = self
        }
    }
    @IBOutlet weak var backMainView: UIView!
    
    @IBOutlet weak var txtStep: UITextField!{
        didSet{
            self.txtStep.addTarget(self, action: #selector(txt), for: .editingChanged)
        }
    }
    @IBOutlet weak var txtIngredientNameCode: UITextField!{
        didSet{
            self.txtIngredientNameCode.addTarget(self, action: #selector(txt), for: .editingChanged)
            self.txtIngredientNameCode.setRightView(image: #imageLiteral(resourceName: "rightArrow"))
            self.txtIngredientNameCode.delegate = self
        }
    }
    @IBOutlet weak var txtQuantity: UITextField!{
        didSet{
            self.txtQuantity.addTarget(self, action: #selector(txt), for: .editingChanged)
        }
    }
    
    @IBOutlet weak var btnSubmit: UIButton!{
        didSet{
            self.btnSubmit.setTitleColor(.init(white: 1, alpha: 0.5), for: .disabled)
            self.btnSubmit.setTitleColor(.white, for: .normal)
            self.btnSubmit.isEnabled = false
        }
    }
    
    @IBOutlet weak var btnNext: UIButton!{
        didSet{
//            self.btnNext.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor?.withAlphaComponent(0.5)], for: .disabled)
//            self.btnNext.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor], for: .normal)
            
            self.btnNext.setTitleColor(UIColor.mainColor?.withAlphaComponent(0.5), for: .disabled)
            self.btnNext.setTitleColor(UIColor.mainColor, for: .normal)
            self.btnNext.isEnabled = false
        }
    }
    @IBOutlet weak var btnAddStep: UIButton!{
        didSet{
//            self.btnAddStep.setTitleColor(UIColor.mainColor?.withAlphaComponent(0.5), for: .disabled)
//            self.btnAddStep.setTitleColor(UIColor.mainColor, for: .normal)
            self.btnAddStep.tintColor = UIColor.mainColor?.withAlphaComponent(0.5)
            self.btnAddStep.isEnabled = false
        }
    }
    
    @IBOutlet weak var titleNav: UILabel!
    @IBOutlet weak var lblTitleFormulation: UILabel!
    @IBOutlet weak var lblTitleStep: UILabel!
    @IBOutlet weak var lblTitleIngNameCode: UILabel!
    @IBOutlet weak var lblTitleQty: UILabel!
    
    
    let pickerView = UIPickerView()
    var isHiddenMenu = false
    var arrStep = [FormulationStep]()
    var selectedIndex = 0
    var experiment_id = 0
    var stepId = 0
    var isEdit = false
    var arrIng = [Ingredient](){
        didSet{
            self.nextButtonEnableCheck()
        }
    }
    var ingID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLanguage()
        if isEdit{
            self.btnNext.isHidden = true
//            self.title = "Edit Process of Formulation"
        }
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        txtIngredientNameCode.inputView = pickerView
        
    }
    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
        newForm()
        self.btnSubmit.setTitle(getLanguageData(strKey: "btnTitleSubmit"), for: .normal)
        getFormulationStep()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    @objc func handleInternetActivity() {

        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)! {

            print("Internet Ok")

        } else {

            print("No Internet Available")
            let vc: NoInternetViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.isInternet = {
                self.remove()
            }
            self.add(vc, parent: self.view)
        }
    }
    
    fileprivate func setLanguage(){
//        self.title = isEdit ? self.getLaunguageData(strKey: "titleNavEdit") : ""
        self.titleNav.text = isEdit ? self.getLanguageData(strKey: "titleNavEdit") : ""
        self.lblTitleFormulation.text = self.getLanguageData(strKey: "titleFormulation")
        self.lblTitleStep.text = self.getLanguageData(strKey: "titleStep")
        self.lblTitleIngNameCode.text = self.getLanguageData(strKey: "titleIngNameCode")
        self.lblTitleQty.text = self.getLanguageData(strKey: "titleQty")
//        self.btnNext.title = getLaunguageData(strKey: "btnTitleNavNext")
        self.btnNext.setTitle(getLanguageData(strKey: "btnTitleNavNext"), for: .normal)
        self.btnNext.setTitle(getLanguageData(strKey: "btnTitleNavNext"), for: .disabled)
    }
    
    private func getFormulationStep(){
        self.startLoader()
        let dic: dictionary = [
            "token": userInfo.access_token,
            "experiment_id": self.experiment_id
        ]
        
        API.shared.getProcessSteps(parameters: dic) { (arrData, arrIngs) in
            self.stopLoader()
            self.arrStep.removeAll()
            arrData.forEach { (data) in
                self.arrStep.append(FormulationStep(data: data))
                self.tblSteps.reloadData()
                if self.selectedIndex < self.arrStep.count{
                    self.fillForm(data: self.arrStep[self.selectedIndex])
                    self.btnSubmit.setTitle(self.getLanguageData(strKey: "btnTitleUpdate"), for: .normal)
                }
            }
            self.arrIng.removeAll()
            arrIngs.map{self.arrIng.append(Ingredient(data: $0))}
            print(self.arrIng)
            self.txt()
            self.nextButtonEnableCheck()
        }
    }
    
    //MARK:- Action
    @objc func txt(){
        self.btnSubmit.isEnabled = txtStep.text?.isEmpty == false && txtIngredientNameCode.text?.isEmpty == false && txtQuantity.text?.isEmpty == false
        self.nextButtonEnableCheck()
    }
    
    func nextButtonEnableCheck(){
        if !isEdit{
            let arrQty = arrStep.map({$0.quantity})
            if arrStep.count > 0 && !arrQty.contains("") && self.arrIng.isEmpty{
                self.btnNext.isEnabled = true
            }else {
                self.btnNext.isEnabled = false
            }
            self.btnAddStep.isEnabled = !self.btnNext.isEnabled
            
        }else {
            self.btnAddStep.isEnabled = !arrIng.isEmpty
        }
        self.btnAddStep.tintColor = self.btnAddStep.isEnabled ? UIColor.mainColor : UIColor.mainColor?.withAlphaComponent(0.5)
    }
    
    @IBAction func onClickMenu(_ sender: UIButton){
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickNext(_ sender: UIButton){
        let vc: ExpectedResultViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func onClickNotes(_ sender: UIButton){
        let vc: NotesViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.screenName = "Formulation"
        vc.isRemove = {
            self.remove()
        }
        self.add(vc, parent: self.backMainView)
    }
    
    @IBAction func onClickRemove(_ sender: UIButton){
        if arrStep.count > 1{
            self.startLoader()
            var dic: dictionary = [
                "token" : self.userInfo.access_token,
                "process_formulations_id": arrStep[selectedIndex].id,
                "experiment_id": self.experiment_id
            ]
            
//            let arrFormulationStep = arrStep.enumerated().filter { (dic) -> Bool in
//                if dic.offset > selectedIndex{
//                    return true
//                }else {
//                    return false
//                }
//            }
//
//            arrFormulationStep.forEach { (arr) in
//                dic["formulations[\(arr.offset)][id]"] = arr.element.id
//                dic["formulations[\(arr.offset)][step]"] = "Step \(selectedIndex + arr.offset)"
//            }
            
            for (index, element) in arrStep.enumerated(){
                if index > selectedIndex{
                    dic["formulations[\(index-selectedIndex-1)][id]"] = element.id
                    dic["formulations[\(index-selectedIndex-1)][step]"] = "Step \(index)"
                }
            }
            
            API.shared.removeProcessStep(parameters: dic) { (msg) in
                self.stopLoader()
                print(msg)
                self.selectedIndex = 0
                self.getFormulationStep()
            }
        }
    }
    
    @IBAction func onClickAddNew(_ sender: UIButton){
        self.btnSubmit.setTitle(getLanguageData(strKey: "btnTitleSubmit"), for: .normal)
        newForm()
    }
    
    @IBAction func onClickSubmit(_ sender: UIButton){
        self.startLoader()
        var dic: dictionary = [
            "experiment_id": experiment_id,
            "step": self.txtStep.text ?? "",
            "ingredient_name_or_code": self.txtIngredientNameCode.text ?? "",
            "ingredient_id": self.ingID,
            "quantity": self.txtQuantity.text ?? "",
            "token": userInfo.access_token,
        ]
        if sender.titleLabel?.text == getLanguageData(strKey: "btnTitleUpdate"){
            dic["process_formulation_id"] = arrStep[selectedIndex].id
        }
        API.shared.addProcessSteps(parameters: dic) { (addedStep) in
            self.stopLoader()
            if sender.titleLabel?.text == self.getLanguageData(strKey: "btnTitleSubmit"){
//                self.arrStep.append(addedStep)
                self.getFormulationStep()
                self.selectedIndex = self.arrStep.count > 1 ? self.arrStep.count-1 : 0
                self.btnSubmit.setTitle(self.getLanguageData(strKey: "btnTitleUpdate"), for: .normal)
            }else {
                self.getFormulationStep()
                self.arrStep[self.selectedIndex] = addedStep
            }
            self.tblSteps.reloadData()
            self.nextButtonEnableCheck()
        }
        
    }
    func newForm(){
        self.txtStep.text = "Step \(arrStep.count+1)"
        self.txtIngredientNameCode.text = ""
        self.txtQuantity.text = ""
        txt()
    }
    
    func fillForm(data: FormulationStep){
        self.stepId = data.id
        self.txtStep.text = data.step
        self.txtIngredientNameCode.text = data.ingredient_name_or_code
        self.txtQuantity.text = data.quantity
    }

}

extension FormulationViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txt()
    }
}


 // MARK:- UITableViewDelegate, UITableViewDataSource
extension FormulationViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrStep.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "IngredientCell", for: indexPath) as? IngredientCell else { return UITableViewCell() }
        if indexPath.row == selectedIndex{
            cell.viewBack.backgroundColor = UIColor.mainColor
            cell.lblName.textColor = UIColor.secondaryColor
        }else{
            cell.viewBack.backgroundColor = UIColor.clear
            cell.lblName.textColor = UIColor.black
        }
        cell.lblName.text = arrStep[indexPath.row].step
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        fillForm(data: arrStep[selectedIndex])
        self.btnSubmit.setTitle(getLanguageData(strKey: "btnTitleUpdate"), for: .normal)
        tableView.reloadData()
        self.txt()
        self.nextButtonEnableCheck()
    }
}


extension FormulationViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrIng.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrIng[row].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if !arrIng.isEmpty{
            self.txtIngredientNameCode.text = arrIng[row].name
            self.ingID = arrIng[row].id
        }
        self.view.endEditing(true)
        self.txt()
    }

}

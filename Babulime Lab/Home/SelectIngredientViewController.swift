//
//  SelectIngridientViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 08/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class Ingredient {
    var id: Int
    var name: String
    var company_name: String
    var contact_name: String
    var contact_number: String
    
    init(data: dictionary) {
        self.id = data["id"] as? Int ?? -1
        self.name = data["name"] as? String ?? ""
        self.company_name = data["company_name"] as? String ?? ""
        self.contact_name = data["contact_name"] as? String ?? ""
        self.contact_number = data["contact_number"] as? String ?? ""
        
    }
    
}

class SelectIngredientViewController: BaseViewController {
    
    
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnIngTitle: UIButton!
    @IBOutlet weak var btnAddNewIng: UIButton!
    
    var arrData = [Ingredient]()
    var isRemove: ((dictionary? , String?)->())?
    
    var filtered:[Ingredient] = []
    var searchActive : Bool = false
    var isFormulation = false
    var isEdit = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.searchBar.delegate = self
        
        self.getData(search: nil)
        setLanguage()
        self.btnAddNewIng.isHidden = isFormulation
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    @objc func handleInternetActivity() {

        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)! {

            print("Internet Ok")

        } else {

            print("No Internet Available")
            let vc: NoInternetViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.isInternet = {
                self.remove()
            }
            self.add(vc, parent: self.view)
        }
    }
    
    fileprivate func setLanguage(){
        self.btnIngTitle.setTitle(getLanguageData(strKey: "titleIngs"), for: .normal)
        self.btnAddNewIng.setTitle(getLanguageData(strKey: "btnAddNewIng"), for: .normal)
    }
    
    func getData(search: String?){
        var dic = [
            "token": userInfo.access_token
        ]
        
        if searchActive{
            dic["filter_keyword"] = search
        }
        API.shared.getIngredientList(parameters: dic) { (data) in
            self.arrData = data
            self.collectionView.reloadData()
        }
    }
    
    // MARK: - Action
    
    @IBAction func onClickAddNewIngredient(_ sender: UIButton){
        self.showInputDialog(title: self.getLanguageData(strKey: "alertTitleAddIng"), subtitle: "", actionTitle: self.getLanguageData(strKey: "btnTitleAdd"), cancelTitle: self.getLanguageData(strKey: "btnCancel"), inputPlaceholder: self.getLanguageData(strKey: "titleIngName"), inputKeyboardType: .default, cancelHandler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }) { (str) in
            self.isRemove?(nil,str)
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        self.isRemove?([:],"")
    }
    
//    @IBAction func onClickMenu(_ sender: UIBarButtonItem){
//        UIView.animate(withDuration: 0.5) { () -> Void in
//            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
//            self.isHiddenMenu.toggle()
//        }
//    }
    
}


extension SelectIngredientViewController: UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        collectionView.reloadData()
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        self.getData(search: searchBar.text)
        if searchBar.text!.isEmpty{
            searchActive = false
        }
        else{
            searchActive = true
        }
        collectionView.reloadData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        collectionView.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = true
        collectionView.reloadData()
    }


    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.getData(search: searchText)
        searchActive = !arrData.isEmpty
        self.collectionView.reloadData()
    }
}


extension SelectIngredientViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomIngredientCell", for: indexPath) as? CustomIngredientCell else { return UICollectionViewCell() }
        cell.data = arrData[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.frame.width/2 - 20
        return CGSize(width: width, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dic: dictionary = [
            "ingredient_id": arrData[indexPath.row].id,
            "token": userInfo.access_token
        ]
        self.startLoader()
        API.shared.getIngredientDetail(parameters: dic) { (ingredientDetail) in
            self.stopLoader()
            self.isRemove?(ingredientDetail, nil)
        }
    }
}

class CustomIngredientCell: UICollectionViewCell {
    
    @IBOutlet weak var lblIngredientName: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblContactName: UILabel!
    @IBOutlet weak var lblContactNumber: UILabel!
    
    
    var data: Ingredient?{
        didSet{
            self.setupUI()
        }
    }
    
    fileprivate func setupUI(){
        guard let data = data else { return }
        self.lblIngredientName.text = data.name
        self.lblCompany.text = data.company_name
        self.lblContactName.text = data.contact_name
        self.lblContactNumber.text = data.contact_number
    }
    
}

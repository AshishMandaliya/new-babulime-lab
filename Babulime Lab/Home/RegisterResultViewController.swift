//
//  RegisterResultViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 12/09/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class RegisterResult {
    let name: String
    let approval_admin_name: String
    let date: String
    let description: String
    let parameter: [dictionary]
    
    init(data: dictionary) {
        self.name = data["name"] as? String ?? ""
        self.approval_admin_name = data["approval_admin_name"] as? String ?? ""
        self.date = data["date"] as? String ?? ""
        self.description = data["description"] as? String ?? ""
        self.parameter = data["data"] as? [dictionary] ?? []
    }
}


class RegisterResultViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!

    @IBOutlet weak var lblAdminName: UILabel!
    @IBOutlet weak var lblRegisterDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblTitleRegister: UILabel!
    @IBOutlet weak var lblTitleAdminName: UILabel!
    @IBOutlet weak var lblTitleRegisterDate: UILabel!
    @IBOutlet weak var lblTitleDescription: UILabel!
    
    var isHiddenMenu = false
    var data = RegisterResult(data: [:])
    var arrParameter: [dictionary] = []
    var experiment_id = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLanguage()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    @objc func handleInternetActivity() {

        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)! {

            print("Internet Ok")

        } else {

            print("No Internet Available")
            let vc: NoInternetViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.isInternet = {
                self.remove()
            }
            self.add(vc, parent: self.view)
        }
    }
    
    func getData(){
        let dic: dictionary = [
            "experiment_id": experiment_id,
            "token": userInfo.access_token
            ]
        
        API.shared.getResultExp(parameters: dic) { (dic) in
            if !dic.isEmpty{
                self.data = RegisterResult(data: dic[0])
                self.lblAdminName.text = self.data.approval_admin_name
                self.lblRegisterDate.text = self.data.date
                self.lblDescription.text = self.data.description
                self.arrParameter = self.data.parameter
                self.collectionView.reloadData()
            }else {
                self.alert(title: self.getLanguageData(strKey: "alertNotApprovedTitle"), message: self.getLanguageData(strKey: "alertNotApprovedMsg")) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
        
    }
    
    fileprivate func setLanguage(){
        self.lblTitleRegister.text = self.getLanguageData(strKey: "titleRegisterResult")
        self.lblTitleAdminName.text = self.getLanguageData(strKey: "titleAdminName")
        self.lblTitleRegisterDate.text = self.getLanguageData(strKey: "titleRegisterDate")
        self.lblTitleDescription.text = self.getLanguageData(strKey: "titleDescription")
        
    }
    
    @IBAction func onClickMenu(_ sender: UIButton){
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }

}

extension RegisterResultViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrParameter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        self.heightCollectionView.constant = collectionView.contentSize.height
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomViewParameterCell", for: indexPath) as? CustomViewParameterCell else { return UICollectionViewCell() }
        cell.lblName.text = arrParameter[indexPath.row]["name"] as? String
        cell.lblValue.text = arrParameter[indexPath.row]["value"] as? String
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.heightCollectionView.constant = collectionView.contentSize.height
    }
}

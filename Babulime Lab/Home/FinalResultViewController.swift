//
//  FinalResultViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 11/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit


class FinalResult {
    
    let id: Int
    let title: String
    let date: String
    let is_succeed: Bool
    let as_per_expected: Bool
    let result_description: String
    let expected_result_description: String
    let custom_result: String
    let data: [dictionary]
    
    init(data: dictionary) {
        self.id = data["id"] as? Int ?? -1
        self.title = data["title"] as? String ?? ""
        self.date = data["date"] as? String ?? ""
        self.is_succeed = data["is_succeed"] as? Bool ?? true
        self.as_per_expected = data["as_per_expected"] as? Bool ?? false
        self.result_description = data["result_description"] as? String ?? ""
        self.expected_result_description = data["expected_result_description"] as? String ?? ""
        self.custom_result = data["custom_result"] as? String ?? ""
        self.data = data["data"] as? [dictionary] ?? []
        
    }
    
}

class FinalResultViewController: BaseViewController, UITextViewDelegate {
    
    @IBOutlet weak var backMainView: UIView!
    @IBOutlet weak var tblView: UIView!
    @IBOutlet weak var tblExperiment: UITableView!{
        didSet{
            self.tblExperiment.delegate = self
            self.tblExperiment.dataSource = self
        }
    }
    @IBOutlet weak var collectionView1: UICollectionView!{
        didSet{
            self.collectionView1.delegate = self
            self.collectionView1.dataSource = self
        }
    }
    @IBOutlet weak var collectionView2: UICollectionView!{
        didSet{
            self.collectionView2.delegate = self
            self.collectionView2.dataSource = self
        }
    }
    @IBOutlet weak var heightCollectionView1: NSLayoutConstraint!
    @IBOutlet weak var heightCollectionView2: NSLayoutConstraint!
    
    @IBOutlet weak var switchSuccess: UISwitch!{
        didSet{
            self.switchSuccess.addTarget(self, action: #selector(onClickSwitch), for: .valueChanged)
        }
    }
    @IBOutlet weak var switchExpected: UISwitch!{
        didSet{
            self.switchExpected.addTarget(self, action: #selector(onClickSwitch), for: .valueChanged)
        }
    }
    @IBOutlet weak var txtSuccess: UITextView!{
        didSet{
            self.txtSuccess.delegate = self
        }
    }
    @IBOutlet weak var txtExpectedResult: UITextView!{
        didSet{
            self.txtExpectedResult.delegate = self
        }
    }
    @IBOutlet weak var txtCustom: UITextView!{
        didSet{
            self.txtCustom.delegate = self
        }
    }
    @IBOutlet weak var btnSample: UIButton!{
        didSet{
            self.btnSample.setTitleColor(UIColor.secondaryColor?.withAlphaComponent(0.5), for: .disabled)
            self.btnSample.setTitleColor(UIColor.secondaryColor, for: .normal)
            self.btnSample.isEnabled = false
        }
    }
    @IBOutlet weak var btnRegister: UIButton!{
        didSet{
            self.btnRegister.setTitleColor(UIColor.secondaryColor?.withAlphaComponent(0.5), for: .disabled)
            self.btnRegister.setTitleColor(UIColor.secondaryColor, for: .normal)
            self.btnRegister.isEnabled = false
        }
    }
    
    @IBOutlet weak var titleNav: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleExps: UILabel!
    @IBOutlet weak var lblTitleSuccess: UILabel!
    @IBOutlet weak var lblTitleExpect: UILabel!
    @IBOutlet weak var lblTitleCustom: UILabel!
    
    var isHiddenMenu = false
    //    var customView = Canvas()
    var isEdit = false
    var experiment_result_id = 0
    var arrExperiment = [FinalResult]()
    var arrValue1:[String] = []{
        didSet{
            self.textValidation()
        }
    }
    var arrValue2:[String] = []{
        didSet{
            self.textValidation()
        }
    }
    //    var arrDate = [dictionary]()
    var arrParameters1 = [dictionary]()
    var arrParameters2 = [dictionary]()
    var selectedIndex = 0
    var experiment_id = 0
    var isSucceed: Bool = true
    var isExpected: Bool = false
    var pickedImageName:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setLanguage()
        if isEdit{
            self.fillForm(data: arrExperiment[0])
//            self.arrParameters1.forEach({self.arrValue1.append($0["value"] as! String)})
//            self.arrParameters2.forEach({self.arrValue2.append($0["value"] as! String)})
            
            if arrParameters1.isEmpty && arrParameters2.isEmpty{
                let dic = [
                    "token": self.userInfo.access_token
                ]
                API.shared.getParameters(parameters: dic) { (arrParameters) in
                    self.stopLoader()
                    self.arrParameters1 = arrParameters
                    self.arrParameters2 = arrParameters
                    self.arrValue1.removeAll()
                    self.arrValue2.removeAll()
                    self.initArray()
                    self.collectionView1.reloadData()
                    self.collectionView2.reloadData()
                }
            }
            self.tblView.isHidden = self.isEdit
//            self.btnSample.setTitle("Update as Sample", for: .normal)
            self.btnSample.setTitle(getLanguageData(strKey: "btnUpdateSample"), for: .normal)
            
        }else {
            self.startLoader()
            let dic = [
                "token": self.userInfo.access_token
            ]
            API.shared.getParameters(parameters: dic) { (arrParameters) in
                self.stopLoader()
                self.arrParameters1 = arrParameters
                self.arrParameters2 = arrParameters
                self.initArray()
                self.collectionView1.reloadData()
                self.collectionView2.reloadData()
                self.getExperiment()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    @objc func handleInternetActivity() {

        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)! {

            print("Internet Ok")

        } else {

            print("No Internet Available")
            let vc: NoInternetViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.isInternet = {
                self.remove()
            }
            self.add(vc, parent: self.view)
        }
    }
    
    fileprivate func setLanguage(){
//        self.title = isEdit ? getLaunguageData(strKey: "titleEditFinalResult") : ""
        self.titleNav.text = isEdit ? getLanguageData(strKey: "titleEditFinalResult") : ""
        self.lblTitleExps.text = getLanguageData(strKey: "titleExps")
        self.lblTitle.text = getLanguageData(strKey: "titleFinalResult")
        self.lblTitleSuccess.text = getLanguageData(strKey: "titleSuccessResult")
        self.lblTitleExpect.text = getLanguageData(strKey: "titleExpectedResult")
        self.lblTitleCustom.text = getLanguageData(strKey: "titleCustom")
        self.txtSuccess.placeholder = getLanguageData(strKey: "txtResultOptional")
        self.txtExpectedResult.placeholder = getLanguageData(strKey: "txtExpectPlace")
        self.txtCustom.placeholder = getLanguageData(strKey: "txtExpectPlace")
        self.btnRegister.setTitle(getLanguageData(strKey: "btnSaveRegister"), for: .normal)
        self.btnSample.setTitle(getLanguageData(strKey: "btnSaveSample"), for: .normal)
    }
    
    func getExperiment(){
        self.startLoader()
        let dic1 : dictionary = [
            "token": userInfo.access_token,
            "experiment_id": experiment_id
        ]
        API.shared.getFinalResult(parameters: dic1) { (arrData) in
            self.stopLoader()
            if arrData.isEmpty == false{
                self.arrExperiment.removeAll()
                arrData.forEach({self.arrExperiment.append(FinalResult(data: $0))})
                self.selectedIndex = self.arrExperiment.count - 1
                self.tblExperiment.reloadData()
            }
        }
    }
    
    func initArray(){
        for _ in arrParameters1{
            self.arrValue1.append("")
            self.arrValue2.append("")
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.textValidation()
    }
    
    func textValidation(){
        let isEmpty1 = arrValue1.contains("")
        let isEmpty2 = arrValue2.contains("")
        if !isEmpty1 && !isEmpty2 {
            self.btnSample.isEnabled = txtSuccess.text?.isEmpty == false && txtExpectedResult.text?.isEmpty == false
            self.btnRegister.isEnabled = txtSuccess.text?.isEmpty == false && txtExpectedResult.text?.isEmpty == false
        }else {
            self.btnSample.isEnabled = false
            self.btnRegister.isEnabled = false
        }
    }
    
    @IBAction func onClickNotes(_ sender: UIButton){
        let vc: NotesViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.screenName = "Final Result"
        vc.isRemove = {
            self.remove()
        }
        self.add(vc, parent: self.backMainView)
    }
    
    @IBAction func onClickMenu(_ sender: UIButton){
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        if isEdit{
            self.navigationController?.popViewController(animated: true)
        }else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @objc func onClickSwitch(_ sender: UISwitch){
        if sender == switchSuccess{
            isSucceed = sender.isOn
        }else if sender == switchExpected{
            isExpected = sender.isOn
        }
    }
    
    func fillForm(data: FinalResult){
        switchSuccess.isOn = data.is_succeed
        switchExpected.isOn = data.as_per_expected
        txtSuccess.text = data.result_description
        txtExpectedResult.text = data.expected_result_description
        txtCustom.text = data.custom_result
        let arrParameters = data.data
        arrParameters1 = arrParameters.filter { (dic) -> Bool in
            if dic["is_actual_result"] as? Bool == true{
                return true
            }
            return false
        }
        arrParameters2 = arrParameters.filter { (dic) -> Bool in
            if dic["is_actual_result"] as? Bool == false{
                return true
            }
            return false
        }
        self.arrValue1.removeAll()
        self.arrValue2.removeAll()
        self.arrParameters1.forEach({self.arrValue1.append($0["value"] as! String)})
        self.arrParameters2.forEach({self.arrValue2.append($0["value"] as! String)})
        collectionView1.reloadData()
        collectionView2.reloadData()
    }
}

//MARK:- Signature Registration and Add Sample
extension FinalResultViewController{
    @IBAction func onClickSample(_ sender: UIButton){
        var dic: dictionary = [
            "token": self.userInfo.access_token,
            "experiment_id": self.experiment_id,
            "is_succeed": self.isSucceed,
            "as_per_expected": self.isExpected,
            "result_description": self.txtSuccess.text ?? "",
            "expected_result_description": self.txtExpectedResult.text ?? "",
            "save_as": "sample",
            "custom_result": self.txtCustom.text ?? "",
        ]
        for (index, _) in self.arrValue1.enumerated() {
            dic["parameters[\(index)][id]"] = self.arrParameters1[index]["id"] //self.arrParameters1[index]["parameter_id"]
            dic["parameters[\(index)][is_actual_result]"] = true
            dic["parameters[\(index)][value]"] = self.arrValue1[index]
        }
        
        for (index, _) in self.arrValue2.enumerated() {
            dic["parameters[\(self.arrValue1.count + index)][id]"] = self.arrParameters2[index]["id"] //self.arrParameters2[index]["parameter_id"]
            dic["parameters[\(self.arrValue1.count + index)][is_actual_result]"] = false
            dic["parameters[\(self.arrValue1.count + index)][value]"] = self.arrValue2[index]
        }
        
        if self.btnSample.title(for: .normal) == getLanguageData(strKey: "btnUpdateSample"){
            dic["update_final_result_id"] = self.experiment_result_id
            self.startLoader()
            API.shared.storeFinalResult(parameters: dic) { (msg) in
                self.stopLoader()
                print(msg)
                self.navigationController?.popViewController(animated: true)
            }
        }else{
            showInputDialog(title: self.getLanguageData(strKey: "sampleNameTitle"), subtitle: "", actionTitle: self.getLanguageData(strKey: "btnTitleAdd"), cancelTitle: self.getLanguageData(strKey: "btnCancel"), inputPlaceholder: self.getLanguageData(strKey: "sampleTxtPlace"), inputKeyboardType: .default, cancelHandler: { (action) in
                self.dismiss(animated: true, completion: nil)
            }) { (str) in
                dic["title"] = str ?? ""
                API.shared.storeFinalResult(parameters: dic) { (msg) in
                    print(msg)
                    self.getExperiment()
                }
            }
        }
        
        
    }
    
    @IBAction func onClickRegister(_ sender: UIButton){
        
        let alert = UIAlertController(title: self.getLanguageData(strKey: "registerAlertTitle"),
                                      message: self.getLanguageData(strKey: "registerAlertMsg"),
                                      preferredStyle: .alert)
        
        
        
        let customView = Canvas(frame: CGRect(x: 10, y: 80, width: 250, height: 140))
        customView.setStrokeColor(color: .black)
        customView.backgroundColor = .white
        
        let height = NSLayoutConstraint(item: alert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 280)
        alert.view.addConstraint(height)
        
        alert.view.addSubview(customView)
        
        let saveAction = UIAlertAction(title: self.getLanguageData(strKey: "btnTitleSave"), style: .default) { (action) in
            UIGraphicsBeginImageContext(customView.frame.size)
            customView.layer.render(in: UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            var dic: dictionary = [
                "token": self.userInfo.access_token,
                "experiment_id": self.experiment_id,
                "is_succeed": self.isSucceed,
                "as_per_expected": self.isExpected,
                "result_description": self.txtSuccess.text ?? "",
                "expected_result_description": self.txtExpectedResult.text ?? "",
                "save_as": "register",
                "custom_result": self.txtCustom.text ?? "",
                //                "singature": signature.pngData()
            ]
            for (index, _) in self.arrValue1.enumerated() {
                dic["parameters[\(index)][id]"] = self.arrParameters1[index]["id"] //self.arrParameters1[index]["parameter_id"]
                dic["parameters[\(index)][is_actual_result]"] = true
                dic["parameters[\(index)][value]"] = self.arrValue1[index]
            }
            
            for (index, _) in self.arrValue2.enumerated() {
                dic["parameters[\(self.arrValue1.count + index)][id]"] = self.arrParameters2[index]["id"] //self.arrParameters2[index]["parameter_id"]
                dic["parameters[\(self.arrValue1.count + index)][is_actual_result]"] = false
                dic["parameters[\(self.arrValue1.count + index)][value]"] = self.arrValue2[index]
            }
            
            API.shared.signUp(vc: self, param: dic, postImage: image!, imageName: self.pickedImageName, failer: { (message) in
                print(message)
                self.alert(title: self.getLanguageData(strKey: "alertError"), message: self.getLanguageData(strKey: "alertWentWrong"))
            }) { (dic) in
                let message = ResponseKey.fatchDataAsString(res: dic, valueOf: .message)
                print(message)
                self.alertRegisterSuccess()
            }
        }
        
        let cancelAction = UIAlertAction(title: self.getLanguageData(strKey: "btnCancel"), style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func alertRegisterSuccess(){
        let alert = UIAlertController(title: self.getLanguageData(strKey: "alertRegisterSuccessTitle"), message: nil, preferredStyle: .alert)
        
        let viewRecipeAction = UIAlertAction(title: self.getLanguageData(strKey: "alertViewRecipe"), style: .default) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
            
        }
        
        let image = UIImageView(frame: CGRect(x: 10, y: 80, width: 100, height: 100))
        image.contentMode = .scaleAspectFit
        image.image = #imageLiteral(resourceName: "checkmark")
        image.translatesAutoresizingMaskIntoConstraints = false
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .centerX, relatedBy: .equal, toItem: alert.view, attribute: .centerX, multiplier: 1, constant: 0))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .top, relatedBy: .equal, toItem: alert.view, attribute: .top, multiplier: 1, constant: 80))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100.0))
        
        let label = UILabel(frame: CGRect(x: 10, y: 200, width: 250, height: 80))
        label.text = self.getLanguageData(strKey: "alertRegisterSuccessMsg")
        label.numberOfLines = 0
        label.font = UIFont(name: "SFProText-Regular", size: 13)
        label.textAlignment = .center
        
        alert.view.addSubview(image)
        alert.view.addSubview(label)
        
        let height = NSLayoutConstraint(item: alert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 350)
        alert.view.addConstraint(height)
        
        alert.addAction(viewRecipeAction)
        
        self.present(alert, animated: true, completion: nil)
    }
}


// MARK:- UITableViewDelegate, UITableViewDataSource
extension FinalResultViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrExperiment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ExperimentCell", for: indexPath) as? ExperimentCell else { return UITableViewCell() }
        if indexPath.row == selectedIndex{
            cell.viewBack.backgroundColor = UIColor.mainColor
            cell.lblName.textColor = UIColor.secondaryColor
            cell.lblDate.textColor = UIColor.secondaryColor
        }else{
            cell.viewBack.backgroundColor = UIColor.clear
            cell.lblName.textColor = UIColor.black
            cell.lblDate.textColor = UIColor.black
        }
        cell.lblName.text = arrExperiment[indexPath.row].title
        cell.lblDate.text = arrExperiment[indexPath.row].date
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        fillForm(data: arrExperiment[selectedIndex])
        tableView.reloadData()
    }
}

extension FinalResultViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionView1{
            return arrParameters1.count
        }else if collectionView == collectionView2{
            return arrParameters2.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomResultParameterCell", for: indexPath) as? CustomResultParameterCell else { return UICollectionViewCell() }
        if collectionView == collectionView1{
            if arrExperiment.isEmpty == false{
                cell.lblName.text = arrParameters1[indexPath.row]["name"] as? String
                cell.txtValue.text = arrParameters1[indexPath.row]["value"] as? String
            }else{
                cell.lblName.text = arrParameters1[indexPath.row]["name"] as? String
            }
            cell.string = { str in
                self.arrValue1[indexPath.row] = str
            }
            self.heightCollectionView1.constant = collectionView.contentSize.height
        }else if collectionView == collectionView2{
            if arrExperiment.isEmpty == false{
                cell.lblName.text = arrParameters2[indexPath.row]["name"] as? String
                cell.txtValue.text = arrParameters2[indexPath.row]["value"] as? String
            }else{
                cell.lblName.text = arrParameters2[indexPath.row]["name"] as? String
            }
            cell.string = { str in
                self.arrValue2[indexPath.row] = str
            }
            self.heightCollectionView2.constant = collectionView.contentSize.height
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.heightCollectionView1.constant = collectionView.contentSize.height
        self.heightCollectionView2.constant = collectionView.contentSize.height
    }
}

class ExperimentCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewBack: UIView!
}


//
//  CreateEx2ViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 07/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit
import VisionKit

class AddedIngredient {
    var id: Int
    var name: String
    var property_or_purity: String
    var purpose: String
    var product_detail_sheet: String
    var certificate: String
    var contact_name: String
    var company_name: String
    var contact_number: String
    var created_at: String
    var updated_at: String
    
    init(data: dictionary) {
        self.id = data["id"] as? Int ?? -1
        self.name = data["name"] as? String ?? ""
        self.property_or_purity = data["property_or_purity"] as? String ?? ""
        self.purpose = data["purpose"] as? String ?? ""
        self.product_detail_sheet = data["product_detail_sheet"] as? String ?? ""
        self.certificate = data["certificate"] as? String ?? ""
        self.contact_name = data["contact_name"] as? String ?? ""
        self.company_name = data["company_name"] as? String ?? ""
        self.contact_number = data["contact_number"] as? String ?? ""
        self.created_at = data["created_at"] as? String ?? ""
        self.updated_at = data["updated_at"] as? String ?? ""
    }
}

class CreateEx2ViewController: BaseViewController, UITextViewDelegate {
    
    @IBOutlet weak var tblIngredient: UITableView!{
        didSet{
            self.tblIngredient.delegate = self
            self.tblIngredient.dataSource = self
        }
    }
    @IBOutlet weak var backMainView: UIView!
    
    @IBOutlet weak var txtSelectIngredient: UITextField!{
        didSet{
            self.txtSelectIngredient.delegate = self
            self.txtSelectIngredient.setRightView(image: #imageLiteral(resourceName: "rightArrow"))
            self.txtSelectIngredient.addTarget(self, action: #selector(txt), for: .editingChanged)
        }
    }
    @IBOutlet weak var txtPropertyPurity: UITextField!{
        didSet{
            self.txtPropertyPurity.addTarget(self, action: #selector(txt), for: .editingChanged)
        }
    }
    @IBOutlet weak var txtPurpose: UITextView!{
        didSet{
            self.txtPurpose.delegate = self
        }
    }
    @IBOutlet weak var btnNext: UIButton!{
        didSet{
//            self.btnNext.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor?.withAlphaComponent(0.5)], for: .disabled)
//            self.btnNext.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor], for: .normal)
            
            self.btnNext.setTitleColor(UIColor.mainColor?.withAlphaComponent(0.5), for: .disabled)
            self.btnNext.setTitleColor(UIColor.mainColor, for: .normal)
            self.btnNext.isEnabled = false
        }
    }
    @IBOutlet weak var txtContactName: UITextField!{
        didSet{
            self.txtContactName.addTarget(self, action: #selector(txt), for: .editingChanged)
        }
    }
    @IBOutlet weak var txtContactNumber: UITextField!{
        didSet{
            self.txtContactNumber.addTarget(self, action: #selector(txt), for: .editingChanged)
        }
    }
    @IBOutlet weak var txtCompanyName: UITextField!{
        didSet{
            self.txtCompanyName.addTarget(self, action: #selector(txt), for: .editingChanged)
        }
    }
    @IBOutlet weak var btnProductDetailSheet: UIButton!
    @IBOutlet weak var btnCertificate: UIButton!
    @IBOutlet weak var imgProductDetailSheet: UIImageView!
    @IBOutlet weak var imgCertificate: UIImageView!
    @IBOutlet weak var btnAdd: UIButton!{
        didSet{
            self.btnAdd.setTitleColor(.init(white: 1, alpha: 0.5), for: .disabled)
            self.btnAdd.setTitleColor(.white, for: .normal)
            self.btnAdd.isEnabled = false
        }
    }
    
    @IBOutlet weak var titleNav: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblIngName: UILabel!
    @IBOutlet weak var lblIngPurity: UILabel!
    @IBOutlet weak var lblIngPurpose: UILabel!
    @IBOutlet weak var lblIngProduct: UILabel!
    @IBOutlet weak var lblIngCerti: UILabel!
    @IBOutlet weak var lblIngContact: UILabel!
    @IBOutlet weak var lblIngContactName: UILabel!
    @IBOutlet weak var lblIngContactNumber: UILabel!
    @IBOutlet weak var lblIngCompanyName: UILabel!
    
    var isHiddenMenu = false
    var isEdit = false
    var selectedIndex = 0
    var experiment_id = 0
    var IngredientList = [AddedIngredient]()
    var isNewIngredient = false
    var addedIngredientId = 0
    var companyName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        let back = UIBarButtonItem()
//        back.title = getLaunguageData(strKey: "titleIngredient")
//        self.navigationItem.backBarButtonItem = back
        
//        getIngredientList()
        setLanguage()
        if isEdit{
            self.btnNext.isHidden = true
//            self.title = "Edit Process of Formulation"
        }
    }
    
    
    
    @objc func tapped() {
      view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
        txt()
        self.btnAdd.setTitle(getLanguageData(strKey: "btnTitleAdd"), for: .normal)
        getIngredientList()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    @objc func handleInternetActivity() {

        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)! {

            print("Internet Ok")

        } else {

            print("No Internet Available")
            let vc: NoInternetViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.isInternet = {
                self.remove()
            }
            self.add(vc, parent: self.view)
        }
    }
    
    fileprivate func setLanguage(){
        self.lblTitle.text = isEdit ? getLanguageData(strKey: "titleUpdateIng") : getLanguageData(strKey: "titleIngredient")
//        self.title = isEdit ? getLaunguageData(strKey: "titleEditIng") : ""
        self.titleNav.text = isEdit ? getLanguageData(strKey: "titleEditIng") : ""
        self.lblIngName.text = getLanguageData(strKey: "titleIngName")
        self.lblIngPurity.text = getLanguageData(strKey: "titleIngPurity")
        self.lblIngPurpose.text = getLanguageData(strKey: "titleIngPurpose")
        self.lblIngProduct.text = getLanguageData(strKey: "titleIngProduct")
        self.lblIngCerti.text = getLanguageData(strKey: "titleIngCerti")
        self.lblIngContact.text = getLanguageData(strKey: "titleIngContact")
        self.lblIngContactName.text = getLanguageData(strKey: "titleIngContactName")
        self.lblIngContactNumber.text = getLanguageData(strKey: "titleIngContactNumber")
        self.lblIngCompanyName.text = getLanguageData(strKey: "titleIngCompanyName")
        self.btnNext.setTitle(getLanguageData(strKey: "btnTitleNavNext"), for: .normal)
        self.btnNext.setTitle(getLanguageData(strKey: "btnTitleNavNext"), for: .disabled)
    }
    
    func getIngredientList(){
        self.startLoader()
        let dic:dictionary = [
            "token": userInfo.access_token,
            "experiment_id": self.experiment_id
        ]

        API.shared.getIngredientInExp(parameters: dic) { (arrData) in
            self.stopLoader()
            self.IngredientList.removeAll()
            arrData.forEach { (data) in
                self.startLoader()
                let dic: dictionary = [
                    "token": self.userInfo.access_token,
                    "ingredient_id": data["id"] as? Int ?? -1
                ]
                API.shared.getIngredientDetail(parameters: dic) { (data) in
                    self.stopLoader()
                    self.IngredientList.append(AddedIngredient(data: data))
                    self.tblIngredient.reloadData()
                    if self.selectedIndex < self.IngredientList.count{
                        self.fillForm(data: self.IngredientList[self.selectedIndex])
//                        self.btnAdd.setTitle("Update", for: .normal)
                        self.btnAdd.setTitle(self.getLanguageData(strKey: "btnTitleUpdate"), for: .normal)
                    }
                    self.txt()
                    self.nextButtonEnableCheck()
                }
            }
            
        }
    }
    
    //MARK:- Action
    
    @objc func txt(){
        self.btnAdd.isEnabled = txtSelectIngredient.text?.isEmpty == false && txtPurpose.text?.isEmpty == false && txtPropertyPurity.text?.isEmpty == false && imgProductDetailSheet.image != nil && self.isValidateMobile(value: self.txtContactNumber.text ?? "")
        
    }
//    func textViewDidChange(_ textView: UITextView) {
//        txt()
//    }
    func textViewDidEndEditing(_ textView: UITextView) {
        txt()
    }
    
    func nextButtonEnableCheck(){
        if !isEdit{
            if IngredientList.count > 0{
                self.btnNext.isEnabled = true
            }else {
                self.btnNext.isEnabled = false
            }
        }
    }
    
    @IBAction func onClickMenu(_ sender: UIButton){
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickNotes(_ sender: UIButton){
        let vc: NotesViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.screenName = "Ingredient"
        vc.isRemove = {
            self.remove()
        }
        self.add(vc, parent: self.backMainView)
    }
    
    @IBAction func onClickRemove(_ sender: UIButton){
        if IngredientList.count > 1{
            self.startLoader()
            let dic: dictionary = [
                "token" : self.userInfo.access_token,
                "ingredient_id": IngredientList[selectedIndex].id,
                "experiment_id": self.experiment_id
            ]
            
            API.shared.removeIngredientInExp(parameters: dic) { (msg) in
                self.stopLoader()
                print(msg)
                self.selectedIndex = 0
                self.getIngredientList()
            }
        }
    }
    
    @IBAction func onClickNext(_ sender: UIButton){
        let vc: FormulationViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func onClickAdd(_ sender: UIButton){
        self.startLoader()
        var dic: dictionary = [
            "experiment_id": experiment_id,
            "name": self.txtSelectIngredient.text ?? "",
            "property_or_purity": self.txtPropertyPurity.text ?? "",
            "purpose": self.txtPurpose.text ?? "",
            "contact_name": self.txtContactName.text ?? "",
            "company_name": self.txtCompanyName.text ?? "",
            "contact_number": self.txtContactNumber.text ?? "",
//            "product_detail_sheet": imgProductDetailSheet.image?.pngData() ?? Data(),
//            "certificate": imgCertificate.image?.pngData() ?? Data(),
            "token": userInfo.access_token
        ]
        if !isNewIngredient && sender.titleLabel?.text == getLanguageData(strKey: "btnTitleAdd"){
            dic["ingredient_id"] = addedIngredientId
        }
        if sender.titleLabel?.text == getLanguageData(strKey: "btnTitleUpdate"){
            dic["ingredient_update_id"] = addedIngredientId
        }
        
//        API.shared.addIngredientInExp(parameters: dic) { (addedIngredient) in
//            self.stopLoader()
//            if sender.titleLabel?.text == "Add"{
//                self.getIngredientList()
//                self.selectedIndex = self.IngredientList.count > 1 ? self.IngredientList.count-1 : 0
//                self.btnAdd.setTitle("Update", for: .normal)
//            }else {
//                self.getIngredientList()
//                self.IngredientList[self.selectedIndex] = addedIngredient
//            }
//            self.tblIngredient.reloadData()
//            self.nextButtonEnableCheck()
//        }
        
        API.shared.addIngredient(vc:self, param: dic, postImage: [imgProductDetailSheet.image ?? UIImage(color: .clear), imgCertificate.image ?? UIImage(color: .clear)] as! [UIImage], imageName: ["product_detail_sheet", "certificate"], imageParam: ["product_detail_sheet", "certificate"]) { (dic) in
            let addedIngredient = AddedIngredient(data: dic)
            self.stopLoader()
            if sender.titleLabel?.text == self.getLanguageData(strKey: "btnTitleAdd"){
                self.getIngredientList()
                self.selectedIndex = self.IngredientList.count > 1 ? self.IngredientList.count-1 : 0
                self.btnAdd.setTitle(self.getLanguageData(strKey: "btnTitleUpdate"), for: .normal)
            }else {
                self.getIngredientList()
                self.IngredientList[self.selectedIndex] = addedIngredient
            }
            self.tblIngredient.reloadData()
            self.nextButtonEnableCheck()
        }
        
//        API.shared.sendImage(vc: self, param: dic, postImage: nil, imageName: "", imageArray: [self.imgProductDetailSheet.image!,self.imgCertificate.image ?? UIImage()], nameArray: ["", ""]) { (dic) in
//            let addedIngredient = AddedIngredient(data: dic)
//            self.stopLoader()
//            if sender.titleLabel?.text == "Add"{
//                self.getIngredientList()
//                self.selectedIndex = self.IngredientList.count > 1 ? self.IngredientList.count-1 : 0
//                self.btnAdd.setTitle("Update", for: .normal)
//            }else {
//                self.getIngredientList()
//                self.IngredientList[self.selectedIndex] = addedIngredient
//            }
//            self.tblIngredient.reloadData()
//            self.nextButtonEnableCheck()
//        }
    }
    
    @IBAction func onClickAddNew(_ sender: UIButton){
        self.btnAdd.setTitle(getLanguageData(strKey: "btnTitleAdd"), for: .normal)
        newForm()
    }
    
    @IBAction func onClickProductDetailSheet(_ sender: UIButton){
        ImagePickerManager().pickImage(self, view: imgProductDetailSheet) { (image) in
            self.imgProductDetailSheet.image = image
            self.txt()
        }
        
    }
    
    @IBAction func onClickCertificate(_ sender: UIButton){
        ImagePickerManager().pickImage(self, view: imgCertificate) { (image) in
            self.imgCertificate.image = image
            self.txt()
        }
    }
    
//    private func configureDocumentView(){
//        guard VNDocumentCameraViewController.isSupported else{ return }
//        let scanningDocumentVC = VNDocumentCameraViewController()
//        scanningDocumentVC.delegate = self
//        self.present(scanningDocumentVC, animated: true, completion: nil)
//    }
    
    func fillForm(data: AddedIngredient){
        self.addedIngredientId = data.id
        self.companyName = data.company_name
        self.txtSelectIngredient.text = data.name
        self.txtPropertyPurity.text = data.property_or_purity
        self.txtPurpose.text = data.purpose
        self.txtContactName.text = data.contact_name
        self.txtContactNumber.text = data.contact_number
        self.txtCompanyName.text = data.company_name
        let sheet = URL(string: data.product_detail_sheet)
        let certificate = URL(string: data.certificate)
        self.startLoader()
        DispatchQueue.global().async {
            let imgSheet = try? Data(contentsOf: sheet!)
            let imgCertificate = try? Data(contentsOf: certificate!)//make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            DispatchQueue.main.async {
                self.imgProductDetailSheet.image = UIImage(data: imgSheet!)
                self.imgCertificate.image = UIImage(data: imgCertificate!)
                self.txt()
                self.stopLoader()
            }
        }
        
//        if let data = try? Data(contentsOf: URL(string: data.product_detail_sheet) ?? URL(fileURLWithPath: "")){
//            self.imgProductDetailSheet.image = UIImage(data: data)
//        }
//        if let data = try? Data(contentsOf: URL(string: data.certificate) ?? URL(fileURLWithPath: "")){
//            self.imgCertificate.image = UIImage(data: data)
//        }
        
//        self.imgProductDetailSheet.downloadImg(url: data.product_detail_sheet)
//        self.imgCertificate.downloadImg(url: data.certificate)
        
    }
    
    func newForm(){
        self.txtSelectIngredient.text = ""
        self.txtPropertyPurity.text = ""
        self.txtPurpose.text = ""
        self.txtContactName.text = ""
        self.txtContactNumber.text = ""
        self.txtCompanyName.text = ""
        self.imgProductDetailSheet.image = nil
        self.imgCertificate.image = nil
        txt()
    }
}


////MARK:- VNDocumentCameraViewControllerDelegate
//extension CreateEx2ViewController: VNDocumentCameraViewControllerDelegate{
//    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
//        for i in 0..<scan.pageCount{
//            let image = scan.imageOfPage(at: i)
//        }
//    }
//}



 // MARK:- UITableViewDelegate, UITableViewDataSource
extension CreateEx2ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return IngredientList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "IngredientCell", for: indexPath) as? IngredientCell else { return UITableViewCell() }
        if indexPath.row == selectedIndex{
            cell.viewBack.backgroundColor = UIColor.mainColor
            cell.lblName.textColor = UIColor.secondaryColor
        }else{
            cell.viewBack.backgroundColor = UIColor.clear
            cell.lblName.textColor = UIColor.black
        }
        cell.lblName.text = IngredientList[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        fillForm(data: IngredientList[selectedIndex])
//        self.btnAdd.setTitle("Update", for: .normal)
        self.btnAdd.setTitle(getLanguageData(strKey: "btnTitleUpdate"), for: .normal)
        tableView.reloadData()
        self.nextButtonEnableCheck()
        self.txt()
    }
    
    
}

extension CreateEx2ViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtSelectIngredient{
            self.dismissKeyboard()
            let child: SelectIngredientViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            child.isEdit = self.isEdit
            child.isRemove = { (selectedIngredient, newIngredient) in
                child.view.removeFromSuperview()
                if selectedIngredient == nil{
                    self.isNewIngredient = true
                    self.newForm()
                    self.txtSelectIngredient.text = newIngredient
                }else if newIngredient == nil{
                    self.isNewIngredient = false
                    self.fillForm(data: AddedIngredient(data: selectedIngredient!))
                }
                self.txt()
            }
//            addChildVC(child,frame: self.backMainView.bounds)
            add(child, parent: self.backMainView)
        }
    }
}


class IngredientCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewBack: UIView!
}

//
//  NoteOptionsViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 27/10/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class NotesOption: NSObject, UINavigationControllerDelegate {

//    var picker = UIImagePickerController();
    var alert = UIAlertController(title: "Choose Option", message: nil, preferredStyle: .actionSheet)
    var viewController: UIViewController?
    var pickOptionCallBack : ((String) -> ())?;

    override init(){
        super.init()
    }

    func pickOption(_ viewController: UIViewController, view: UIView, _ callback: @escaping ((String) -> ())) {
        pickOptionCallBack = callback;
        self.viewController = viewController;

        let textAction = UIAlertAction(title: "Text", style: .default){ [self]
            UIAlertAction in
//            self.openCamera()
            self.pickOptionCallBack?("Text")
        }
        let imageAction = UIAlertAction(title: "Image", style: .default){
            UIAlertAction in
//            self.openGallery()
            self.pickOptionCallBack?("Image")
        }
        let drawAction = UIAlertAction(title: "Drawing", style: .default){
            UIAlertAction in
//            self.openGallery()
            self.pickOptionCallBack?("Drawing")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            UIAlertAction in
            viewController.dismiss(animated: true, completion: nil)
        }

        // Add the actions
//        picker.delegate = self
        alert.addAction(textAction)
        alert.addAction(imageAction)
        alert.addAction(drawAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = view
        viewController.present(alert, animated: true, completion: nil)
    }
    
//    func openCamera(){
//        alert.dismiss(animated: true, completion: nil)
//        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
//            picker.sourceType = .camera
//            self.viewController!.present(picker, animated: true, completion: nil)
//        } else {
//            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
//            alertWarning.show()
//        }
//    }
//    func openGallery(){
//        alert.dismiss(animated: true, completion: nil)
//        picker.sourceType = .photoLibrary
//        self.viewController!.present(picker, animated: true, completion: nil)
//    }
//
//
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        picker.dismiss(animated: true, completion: nil)
//    }
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        picker.dismiss(animated: true, completion: nil)
//        guard let image = info[.originalImage] as? UIImage else {
//            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
//        }
//        pickImageCallback?(image)
//    }
//
//
//
//    @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?) {
//    }

}


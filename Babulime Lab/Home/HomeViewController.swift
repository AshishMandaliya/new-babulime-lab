//
//  HomeViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 06/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class ExperimentListing {
    let id: Int
    let user_id: Int
    let created_date: String
    let name: String
    let reason_for_development: String
    let types_of_recipe: String
    let status: String
    let priority: String
    let experiment_id: Int
    let experiment_results: dictionary
    let permissions: dictionaryforrequest
    
    init(data: dictionary){
        self.id = data["id"] as? Int ?? -1
        self.user_id = data["user_id"] as? Int ?? -1
        self.created_date = data["created_date"] as? String ?? ""
        self.name = data["name"] as? String ?? ""
        self.reason_for_development = data["reason_for_development"] as? String ?? ""
        self.types_of_recipe = data["types_of_recipe"] as? String ?? ""
        self.status = data["status"] as? String ?? ""
        self.priority = data["priority"] as? String ?? ""
        self.experiment_id = data["experiment_id"] as? Int ?? -1
        self.experiment_results = data["experiment_results"] as? dictionary ?? [:]
        self.permissions = data["permissions"] as? dictionaryforrequest ?? [:]
    }
}

class HomeViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tblMyExperiment: UITableView!{
        didSet{
            self.tblMyExperiment.delegate = self
            self.tblMyExperiment.dataSource = self
        }
    }
    @IBOutlet weak var segment: UISegmentedControl!{
        didSet{
            self.segment.addTarget(self, action: #selector(changeSegment), for: .valueChanged)
        }
    }
    @IBOutlet weak var btnNotification: UIBarButtonItem!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var vwNotification: UIView!
    @IBOutlet weak var tblViewNotification: UITableView!{
        didSet{
            self.tblViewNotification.delegate = self
            self.tblViewNotification.dataSource = self
        }
    }
    @IBOutlet weak var tblViewDetailFilter: UITableView!{
        didSet{
            self.tblViewDetailFilter.delegate = self
            self.tblViewDetailFilter.dataSource = self
        }
    }
    @IBOutlet weak var tblViewFliterCategory: UITableView!{
        didSet{
            self.tblViewFliterCategory.delegate = self
            self.tblViewFliterCategory.dataSource = self
        }
    }
    @IBOutlet weak var vwFliter: UIView!
    
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleTotal: UILabel!
    @IBOutlet weak var lblTitleNotification: UILabel!
    @IBOutlet weak var btnCreate: UIButton!
    @IBOutlet weak var btnApply: UIButton!{
        didSet{
            self.btnApply.setTitleColor(UIColor(hex: "#0091ff80"), for: .disabled)
            self.btnApply.setTitleColor(UIColor(hex: "#0091ffff"), for: .normal)
            self.btnApply.isEnabled = true
        }
    }
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var searchBarFilter: UISearchBar!{
        didSet{
            self.searchBarFilter.delegate = self
        }
    }
    @IBOutlet weak var txtFilter: UITextField!{
        didSet{
            self.txtFilter.addTarget(self, action: #selector(txt), for: .editingChanged)
            self.txtFilter.delegate = self
        }
    }
    @IBOutlet weak var titleFilter: UILabel!
    @IBOutlet weak var redDotFilter: UIView!
    @IBOutlet weak var titleNoDataFilter: UILabel!
    @IBOutlet weak var subTryAgainFilter: UILabel!
    @IBOutlet weak var noDataFilterView: UIView!
    
    
    @IBOutlet weak var firstCreateExpView: UIView!
    @IBOutlet weak var titleMsgFirstCreate: UILabel!
    @IBOutlet weak var subTitleMsgFirstCreate: UILabel!
    @IBOutlet weak var btnStartCreate: UIButton!
    
    @IBOutlet weak var noDraftView: UIView!
    @IBOutlet weak var titleMsgDraft: UILabel!
    @IBOutlet weak var subTitleMsgDraft: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var searchBarHome: UISearchBar!
    
    var searchActive : Bool = false{
        didSet{
            self.setLanguage()
        }
    }
    
    var isHiddenMenu = false
    var dataArray = [ExperimentListing]()
    var isNotificationOpen = false
    var FilterArray = [dictionary]()
    var arrNotification = [dictionary]()
    var filterSelectedIndex = 0{
        didSet{
            self.searchBarFilter.text = ""
        }
    }
//    var filterDetailSelectedIndex = 0
    var detail = [String]()
    let dateTimePicker = UIDatePicker()
    
    var isFilterApply = false
    var arrIngFilter: [String] = []{
        didSet{
            checkApplyButtonValidation()
        }
    }
    var arrContactPerson: [String] = []{
        didSet{
            checkApplyButtonValidation()
        }
    }
    var arrCompanyName: [String] = []{
        didSet{
            checkApplyButtonValidation()
        }
    }
    var arrImportance: [String] = []{
        didSet{
            checkApplyButtonValidation()
        }
    }
    var arrResult: [String] = []{
        didSet{
            checkApplyButtonValidation()
        }
    }
    var filterPropertyPurity = ""{
        didSet{
            checkApplyButtonValidation()
        }
    }
    var filterDateTime = ""{
        didSet{
            checkApplyButtonValidation()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        splitViewController?.viewWillAppear(true)
        self.isNotificationOpen = false
        self.onClickNotification(self.btnNotification as UIBarButtonItem)
//        self.vwFliter.slideOut(to: .right, x: 0, y: 0, duration: 0.5, delay: 0, completion: nil)
        
        self.vwFliter.slideOutCustom()
        
//        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Nav
        let back = UIBarButtonItem()
        back.title = getLanguageData(strKey: "titleMyExp")
        self.navigationItem.backBarButtonItem = back
        
//        self.navigationController?.isNavigationBarHidden = true
        
        self.setLanguage()
        searchBarHome.delegate = self
        self.btnNotification.addBadge(number: 9, withOffset: CGPoint(x: 10, y: 0), andColor: UIColor.mainColor!, andFilled: true)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
        
        getData()
        self.redDotFilter.isHidden = !isFilterApply
        
        
    }
    
    
    
//    @objc func willEnterForeground(){
//        let tView = UIView(frame: self.view.frame)
//        tView.backgroundColor = .clear
//        tView.isUserInteractionEnabled = false
//        self.view.addSubview(tView)
//        
//        if AuthenticationManager.sharedInstance.needsAuthentication{
//            let vc: SecureViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
//            self.add(vc, parent: tView)
//            vc.dismissVC = {
//                self.remove()
//                
//                AuthenticationManager.sharedInstance.needsAuthentication = false
//                self.viewWillAppear(true)
//            }
//        }
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    @objc func handleInternetActivity() {
        
        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)! {
            
            print("Internet Ok")
            
        } else {
            
            print("No Internet Available")
            let vc: NoInternetViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.isInternet = {
                self.remove()
            }
            self.add(vc, parent: self.view)
        }
    }
    
    func setLanguage(){
        self.lblNavTitle.text = getLanguageData(strKey: "homeTitle")
        self.lblTitle.text = getLanguageData(strKey: "titleMyExp")
        self.lblTitleTotal.text = getLanguageData(strKey: "titleTotal")
        self.lblTitleNotification.text = getLanguageData(strKey: "titleNotification")
        self.btnCreate.setTitle(getLanguageData(strKey: "titleCreate"), for: .normal)
        
        //filter
        self.titleFilter.text = getLanguageData(strKey: "titleFilter")
        self.btnCancel.setTitle(getLanguageData(strKey: "btnClearFilters"), for: .normal)
        self.btnApply.setTitle(getLanguageData(strKey: "btnApply"), for: .normal)
        self.titleNoDataFilter.text = self.searchActive ? getLanguageData(strKey: "searchTitleMsg") : getLanguageData(strKey: "filterTitleMsg")
        self.subTryAgainFilter.text = self.searchActive ? getLanguageData(strKey: "searchSubTitleMsg") : getLanguageData(strKey: "filterSubTitleMsg")
        
        //First Create
        self.titleMsgFirstCreate.text = getLanguageData(strKey: "titleMsgCreatFirst")
        self.subTitleMsgFirstCreate.text = getLanguageData(strKey: "subTitleMsgCreatFirst")
        self.btnStartCreate.setTitle(getLanguageData(strKey: "btnTitleStartCreate"), for: .normal)
        
        
        //No Draft
        self.titleMsgDraft.text = self.getLanguageData(strKey: "titleMsgDraft")
        self.subTitleMsgDraft.text = self.getLanguageData(strKey: "subTitleMsgDraft")
    }
    
    func getData(){
        self.startLoader()
        var dic: dictionary = [
            "token": self.userInfo.access_token,
            "listing": segment.titleForSegment(at: segment.selectedSegmentIndex)!.lowercased(),
            "user_id": self.userInfo.user_id,
            "ingredients": arrIngFilter.joined(separator: ","),
            "contact_persons": arrContactPerson.joined(separator: ","),
            "company_names": arrCompanyName.joined(separator: ","),
            "importances": arrImportance.joined(separator: ","),
            "results": arrResult.joined(separator: ","),
            "property_purities": filterPropertyPurity,
            "filter_date_time": filterDateTime,
        ]
        if searchActive{
            dic["keyword_search"] = self.searchBarHome.text
        }
        
        API.shared.getExperimentListing(vc: self, parameters: dic) { (listing, notificationCount) in
            self.dataArray.removeAll()
            listing.forEach { (data) in
                self.dataArray.append(ExperimentListing(data: data))
            }
            notificationCount == 0 ? self.btnNotification.removeBadge() : self.btnNotification.addBadge(number: notificationCount, withOffset: CGPoint(x: 10, y: 0), andColor: UIColor.mainColor!, andFilled: true)
            
            self.lblTotal.text = "\(self.dataArray.count)"
            
            if self.dataArray.isEmpty {
                if self.searchActive{
                    self.tblMyExperiment.isHidden = true
                    self.noDataFilterView.isHidden = false
                }else{
                    if !self.isFilterApply{
                        self.mainView.isHidden = true
                        if self.segment.titleForSegment(at: self.segment.selectedSegmentIndex) == "Draft"{
                            self.noDraftView.isHidden = false
                            self.firstCreateExpView.isHidden = true
                        }else{
                            self.noDraftView.isHidden = true
                            self.firstCreateExpView.isHidden = false
                            self.btnCreate.isHidden = true
                        }
                    }else{
                        self.mainView.isHidden = false
                        self.btnCreate.isHidden = false
                    }
                    
                    if self.isFilterApply{
                        self.tblMyExperiment.isHidden = true
                        self.noDataFilterView.isHidden = false
                    }else{
                        self.tblMyExperiment.isHidden = false
                        self.noDataFilterView.isHidden = true
                    }
                }
            }else{
                self.mainView.isHidden = false
                self.btnCreate.isHidden = false
                self.tblMyExperiment.isHidden = false
            }
            
            
            
            self.tblMyExperiment.reloadData()
            self.stopLoader()
        }
    }
    
    func getFilter(){
        let dic: dictionary = [
            "user_id" : self.userId,
            "token": self.userInfo.access_token
        ]
        
        API.shared.getFilter(parameters: dic) { (arr) in
            self.FilterArray.removeAll()
            self.FilterArray = arr
            self.detail = self.FilterArray[self.filterSelectedIndex]["detail"] as? [String] ?? []
            self.tblViewFliterCategory.reloadData()
            self.tblViewDetailFilter.reloadData()
            
        }
        
    }
    
    func getNotification(){
        let dic: dictionary = [
            "user_id" : self.userId,
            "token": self.userInfo.access_token
        ]
        
        API.shared.getNotification(parameters: dic) { (arr) in
            self.arrNotification.removeAll()
            self.arrNotification = arr
            self.tblViewNotification.reloadData()
        }
    }
    
    
    // MARK:- Action
    @IBAction func onClickMenu(_ sender: UIBarButtonItem){
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @IBAction func onClickLogout(_ sender: UIButton){
        UserData.shared.removeUser()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.rootToLogin()
    }
    
    @IBAction func onClickFilterApply(_ sender: UIButton){
//        let filterCategoryName = self.FilterArray[filterSelectedIndex]["name"] as? String ?? ""
//        let filterDetailName = detail.isEmpty ? txtFilter.text ?? "" : detail[filterDetailSelectedIndex]
//
//        switch filterCategoryName {
//        case "Ingredients":
//            filterKey = "filter_ingredient_name"
//        case "Contact Person":
//            filterKey = "filter_contact_person"
//        case "Company Name":
//            filterKey = "filter_company_name"
//        case "Importance":
//            filterKey = "filter_importance"
//        case "Result":
//            filterKey = "filter_result"
//        case "Status":
//            filterKey = "filter_status"
//        default:
//            filterKey = ""
//        }
//
//        filterValue = filterKey.isEmpty ? "" : filterDetailName
//
//        self.getData(withfilterKey: filterKey, value: filterValue)
//        self.btnCloseFliter()
        self.isFilterApply = true
        self.redDotFilter.isHidden = false
        print(isFilterApply)
        getData()
        self.btnCloseFliter()
    }
    
    @IBAction func onClickCancelFilter(_ sender: UIButton){
        self.isFilterApply = false
        self.arrIngFilter = []
        self.arrContactPerson = []
        self.arrCompanyName = []
        self.arrImportance = []
        self.arrResult = []
        self.filterDateTime = ""
        self.filterPropertyPurity = ""
        self.redDotFilter.isHidden = true
        self.btnCloseFliter()
    }
    
    func checkApplyButtonValidation(){
        self.btnApply.isEnabled = self.arrIngFilter.isEmpty == false || self.arrContactPerson.isEmpty == false || self.self.arrCompanyName.isEmpty == false || self.arrImportance.isEmpty == false || self.arrResult.isEmpty == false || self.filterDateTime.isEmpty == false || self.filterPropertyPurity.isEmpty == false
    }
    
    @objc func changeSegment(_ sender: UISegmentedControl){
        getData()
    }
    
    @IBAction func OpenFliter()
    {
//        self.vwFliter.slideIn(from: .right, x: 0, y: 0, duration: 0.5, delay: 0, completion: nil)
        self.vwFliter.slideInCustom()
        getFilter()
        checkApplyButtonValidation()
    }
    
    @IBAction func btnCloseFliter()
    {
//        self.vwFliter.slideOut(to: .right, x: 0, y: 0, duration: 0.5, delay: 0, completion: nil)
        self.dismissKeyboard()
        self.vwFliter.slideOutCustom { (isComplete) in
            self.checkApplyButtonValidation()
            if !self.btnApply.isEnabled{
                self.isFilterApply = false
                self.redDotFilter.isHidden = true
                self.getData()
            }
        }
        
        
    }
    
    @IBAction func onClickCreate(_ sender: UIButton){
        let vc: CreateEx1ViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickNotification(_ sender: Any){
        if isNotificationOpen{
//            self.vwNotification.slideIn(from: .right, x: 0, y: 0, duration: 0.5, delay: 0) { (true) in
//                self.getNotification()
//            }
            self.vwNotification.slideInCustom { (isComplete) in
                self.getNotification()
            }
        }
        else{
//            self.vwNotification.slideOut(to: .right, x: 0, y: 0, duration: 0.5, delay: 0, completion: nil)
            self.vwNotification.slideOutCustom()
        }
        self.isNotificationOpen.toggle()
    }
    
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        if searchBar == self.searchBarFilter{
//
//            detail = searchText.isEmpty ? self.FilterArray[filterSelectedIndex]["detail"] as? [String] ?? [] : detail.filter({(dataString: String) -> Bool in
//                // If dataItem matches the searchText, return true to include it
//                return dataString.range(of: searchText, options: .caseInsensitive) != nil
//            })
//
//            self.tblViewDetailFilter.reloadData()
//        }
//    }
//
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        if searchBar == self.searchBarFilter{
//            detail = self.FilterArray[filterSelectedIndex]["detail"] as? [String] ?? []
//            self.tblViewDetailFilter.reloadData()
//        }
//    }
    
    @objc func onClickDate(_ sender: UIDatePicker){
        self.filterDateTime = formatDateForDisplay(date: sender.date, format: "yyyy-MM-dd hh:mm:ss")
        self.txtFilter.text = self.filterDateTime
        self.view.endEditing(true)
    }
    
    @objc func txt(){
        let filterCategoryName = self.FilterArray[filterSelectedIndex]["name"] as? String ?? ""
        if filterCategoryName == "Property/Purity"{
            self.filterPropertyPurity = self.txtFilter.text ?? ""
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField == self.txtFilter{
            filterDateTime = ""
        }
        return true
    }
}

extension HomeViewController: UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true
//        collectionView.reloadData()
        
        if searchBar == searchBarHome{
            searchActive = true
            tblMyExperiment.reloadData()
        }
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        self.getData(search: searchBar.text)
//        if searchBar.text!.isEmpty{
//            searchActive = false
//        }
//        else{
//            searchActive = true
//        }
//        collectionView.reloadData()
        if searchBar == searchBarHome{
            self.getData()
            searchActive = !searchBar.text!.isEmpty
            self.tblMyExperiment.reloadData()
        }
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        searchActive = false
//        collectionView.reloadData()
        
        if searchBar == self.searchBarFilter{
            detail = self.FilterArray[filterSelectedIndex]["detail"] as? [String] ?? []
            self.tblViewDetailFilter.reloadData()
        }else if searchBar == self.searchBarHome{
            searchActive = false
            self.tblMyExperiment.reloadData()
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        searchActive = false
//        collectionView.reloadData()
        if searchBar == searchBarHome{
            self.dismissKeyboard()
            self.searchActive = true
            tblMyExperiment.reloadData()
        }
    }


    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        self.getData(search: searchText)
//        searchActive = !arrData.isEmpty
//        self.collectionView.reloadData()
        
        if searchBar == self.searchBarFilter{
            
            detail = searchText.isEmpty ? self.FilterArray[filterSelectedIndex]["detail"] as? [String] ?? [] : detail.filter({(dataString: String) -> Bool in
                // If dataItem matches the searchText, return true to include it
                return dataString.range(of: searchText, options: .caseInsensitive) != nil
            })
            
            self.tblViewDetailFilter.reloadData()
        }else if searchBar == searchBarHome{
            searchActive = !searchBar.text!.isEmpty
            self.getData()
            self.tblMyExperiment.reloadData()
        }
    }
}



// MARK:- UITableViewDelegate, UITableViewDataSource
extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblViewNotification {
            let detail = self.arrNotification[section]["detail"] as? [dictionary] ?? []
            return detail.count
        }else if tableView == tblViewFliterCategory{
            return FilterArray.count
        }else if tableView == tblViewDetailFilter{
            let detail = FilterArray.count > 0 ? self.detail : []//FilterArray[filterSelectedIndex]["detail"] as? [String] : []
            return detail.count
        }else {
            return dataArray.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblViewNotification {
            return self.arrNotification.count
        }else if tableView == tblViewFliterCategory{
            return 1
        }else if tableView == tblViewDetailFilter{
            return 1
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblViewNotification{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
            
            let detail = self.arrNotification[indexPath.section]["detail"] as? [dictionary] ?? []
            cell.lblExperimentName.text = detail[indexPath.row]["experiment_name"] as? String ?? ""
            cell.lblTime.text = detail[indexPath.row]["date"] as? String ?? ""
            
            cell.editReminder = { [weak self] in
                
                let detail = self?.arrNotification[indexPath.section]["detail"] as? [dictionary] ?? []
                let reminder = detail[indexPath.row]["reminder"] as? dictionary
                let vc: ReminderCreateViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
                vc.isEdit = true
                vc.experiment_id = detail[indexPath.row]["experiment_id"] as? Int ?? -1
                vc.data = ViewReminder(data: reminder ?? [:])
                self?.navigationController?.pushViewController(vc, animated: true)
            }
            
            cell.addResult = { [weak self] in
                let detail = self?.arrNotification[indexPath.section]["detail"] as? [dictionary] ?? []
                let vc: FinalResultViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
                vc.experiment_id = detail[indexPath.row]["experiment_id"] as? Int ?? -1
                self?.navigationController?.pushViewController(vc, animated: true)
            }
            
            return cell
        }else if tableView == tblViewFliterCategory {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FliterCategoryCell", for: indexPath) as! FliterCategoryCell
            
            cell.lblFilterCategory.text = self.FilterArray[indexPath.row]["name"] as? String ?? ""
            cell.lblFilterCategory.textColor = UIColor(hex: "#222222ff")
            cell.layer.backgroundColor = UIColor.clear.cgColor
            
            if indexPath.row == filterSelectedIndex{
                cell.lblFilterCategory.textColor = UIColor.mainColor
                cell.layer.backgroundColor = UIColor(hex: "#ebeaecff")?.cgColor
            }
            
            return cell
        }else if tableView == tblViewDetailFilter{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailFilterCell", for: indexPath) as! DetailFilterCell
            
            cell.lblIngredient.text = detail[indexPath.row]
            
            if isFilterApply{
                
            }
            
            let filterCategoryName = self.FilterArray[filterSelectedIndex]["name"] as? String ?? ""
            switch filterCategoryName {
            case "Ingredients":
                if arrIngFilter.contains(self.detail[indexPath.row]){
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
            case "Contact Person":
                if arrContactPerson.contains(self.detail[indexPath.row]){
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
            case "Company Name":
                if arrCompanyName.contains(self.detail[indexPath.row]){
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
            case "Importance":
                if arrImportance.contains(self.detail[indexPath.row]){
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
            case "Result":
                if arrResult.contains(self.detail[indexPath.row]){
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
            default:
                print("Filter Not Found")
                cell.accessoryType = .none
            }
            
            
            
            return cell
        }else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyExperimentCell", for: indexPath) as? MyExperimentCell else { return UITableViewCell() }
            
            cell.lblName.text = dataArray[indexPath.row].name
            cell.lblTime.text = dataArray[indexPath.row].created_date
            cell.lblDetail.text = dataArray[indexPath.row].reason_for_development
            cell.btnState.setTitle(dataArray[indexPath.row].types_of_recipe, for: .normal)
            if dataArray[indexPath.row].priority == "urgent"{
                cell.lblPriority.text = dataArray[indexPath.row].priority
                cell.lblPriority.textColor = UIColor.urgentColor
                cell.viewDotPriority.backgroundColor = UIColor.urgentColor
            }else if dataArray[indexPath.row].priority == "normal"{
                cell.lblPriority.text = dataArray[indexPath.row].priority
                cell.lblPriority.textColor = UIColor.normalColor
                cell.viewDotPriority.backgroundColor = UIColor.normalColor
            }
            if segment.titleForSegment(at: segment.selectedSegmentIndex)!.lowercased() == "register" && dataArray[indexPath.row].status != "registered"{
                cell.stcPending.isHidden = false
            }else {
                cell.stcPending.isHidden = true
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblMyExperiment{
            if segment.titleForSegment(at: segment.selectedSegmentIndex) == "Register" {
                if dataArray[indexPath.row].status == "registered"{
                    let vc: RegisterResultViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
                    vc.experiment_id = dataArray[indexPath.row].id
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }else if segment.titleForSegment(at: segment.selectedSegmentIndex) == "Draft" {
                let vc: EditExpViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
                vc.experiment_id = dataArray[indexPath.row].id
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                let vc: ListExpViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
                vc.experiment_id = dataArray[indexPath.row].experiment_id
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }else if tableView == tblViewFliterCategory {
            
            self.filterSelectedIndex = indexPath.row
            self.detail = self.FilterArray[self.filterSelectedIndex]["detail"] as? [String] ?? []
            if detail.isEmpty{
                self.tblViewDetailFilter.isHidden = true
                self.txtFilter.isHidden = false
                
                let filterCategoryName = self.FilterArray[filterSelectedIndex]["name"] as? String ?? ""
                if filterCategoryName == "Date & Time"{
                    self.txtFilter.inputView = dateTimePicker
                    dateTimePicker.datePickerMode = .dateAndTime
                    dateTimePicker.maximumDate = Date()
                    dateTimePicker.addTarget(self, action: #selector(onClickDate), for: .valueChanged)
                    self.txtFilter.text = filterDateTime
                    self.txtFilter.clearButtonMode = .always
                }else if filterCategoryName == "Property/Purity"{
                    self.txtFilter.keyboardType = .decimalPad
                    self.txtFilter.text = filterPropertyPurity
                    self.txtFilter.clearButtonMode = .never
                }
            }else{
                self.txtFilter.isHidden = true
                self.tblViewDetailFilter.isHidden = false
                self.tblViewDetailFilter.reloadData()
            }
            tableView.reloadData()
            
            
        }else if tableView == tblViewDetailFilter{
            
            let filterCategoryName = self.FilterArray[filterSelectedIndex]["name"] as? String ?? ""
            self.detail = self.FilterArray[self.filterSelectedIndex]["detail"] as? [String] ?? []
//            filterDetailSelectedIndex = indexPath.row
//            tblViewDetailFilter.reloadData()
            let cell = tableView.cellForRow(at: indexPath)!
            if cell.accessoryType == .checkmark {
                
                
                cell.accessoryType = .none
                switch filterCategoryName {
                case "Ingredients":
                    self.arrIngFilter = self.arrIngFilter.filter {$0 != self.detail[indexPath.row]}
                    print(arrIngFilter)
                case "Contact Person":
                    self.arrContactPerson = self.arrContactPerson.filter {$0 != self.detail[indexPath.row]}
                    print(arrContactPerson)
                case "Company Name":
                    self.arrCompanyName = self.arrCompanyName.filter {$0 != self.detail[indexPath.row]}
                    print(arrCompanyName)
                case "Importance":
                    self.arrImportance = self.arrImportance.filter {$0 != self.detail[indexPath.row]}
                    print(arrImportance)
                case "Result":
                    self.arrResult = self.arrResult.filter {$0 != self.detail[indexPath.row]}
                    print(arrResult)
                default:
                    print("Filter Not Found")
                }
                
            } else {
                
                cell.accessoryType = .checkmark
                switch filterCategoryName {
                case "Ingredients":
                    arrIngFilter.append(self.detail[indexPath.row])
                    print(arrIngFilter)
                case "Contact Person":
                    arrContactPerson.append(self.detail[indexPath.row])
                    print(arrContactPerson)
                case "Company Name":
                    arrCompanyName.append(self.detail[indexPath.row])
                    print(arrCompanyName)
                case "Importance":
                    arrImportance.append(self.detail[indexPath.row])
                    print(arrImportance)
                case "Result":
                    arrResult.append(self.detail[indexPath.row])
                    print(arrResult)
                default:
                    print("Filter Not Found")
                }
                
            }
            
        }else if tableView == tblViewNotification{
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == tblViewNotification
        {
            return 35
        }
        
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if tableView == tblViewNotification
        {
            return 20
        }
        
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 20, y: 2, width: 320, height: 30)
        myLabel.font = UIFont.init(name: "SFProText-Medium", size: 24)
        myLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        myLabel.textColor = #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1333333333, alpha: 1)
        
        let headerView = UIView()
        headerView.addSubview(myLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if tableView == tblViewNotification
        {
            return self.arrNotification[section]["lable"] as? String ?? ""
        }
        
        else
        {
            return ""
        }
    }
    
    func alertRegisterSuccess(){
        let alert = UIAlertController(title: "Request for modify recipe has been sent successfully", message: nil, preferredStyle: .alert)
        
        let viewRecipeAction = UIAlertAction(title: "Got it!", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
            
        }
        
        let image = UIImageView(frame: CGRect(x: 10, y: 80, width: 100, height: 100))
        image.contentMode = .scaleAspectFit
        image.image = #imageLiteral(resourceName: "checkmark")
        image.translatesAutoresizingMaskIntoConstraints = false
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .centerX, relatedBy: .equal, toItem: alert.view, attribute: .centerX, multiplier: 1, constant: 0))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .top, relatedBy: .equal, toItem: alert.view, attribute: .top, multiplier: 1, constant: 80))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100.0))
        
        let label = UILabel(frame: CGRect(x: 10, y: 200, width: 250, height: 80))
        label.text = "You will be able to modify recipe once all admin confirm your request."
        label.numberOfLines = 0
        label.font = UIFont(name: "SFProText-Regular", size: 13)
        label.textAlignment = .center
        
        alert.view.addSubview(image)
        alert.view.addSubview(label)
        
        let height = NSLayoutConstraint(item: alert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 350)
        alert.view.addConstraint(height)
        
        alert.addAction(viewRecipeAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func formatDateForDisplay(date: Date, format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
}


class MyExperimentCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var btnState: UIButton!{
        didSet{
            self.btnState.roundCorners(corners: [.topRight, .bottomLeft], radius: 10)
        }
    }
    @IBOutlet weak var btnChain: UIButton!{
        didSet{
            self.btnChain.roundCorners(corners: [.topLeft, .bottomRight], radius: 10)
        }
    }
    @IBOutlet weak var viewDotPriority: UIView!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var stcPending: UIStackView!
}

class NotificationCell : UITableViewCell {
    
    @IBOutlet weak var lblResult: UILabel!
    
    @IBOutlet weak var lblExperimentName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnResult: UIButton!{
        didSet{
            self.btnResult.addTarget(self, action: #selector(onClickAddResult), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnEditReminder: UIButton!{
        didSet{
            self.btnEditReminder.addTarget(self, action: #selector(onClickEditReminder), for: .touchUpInside)
        }
    }
    @IBOutlet weak var btnNotNow: UIButton!
    
    var editReminder: (()->()) = {}
    var addResult: (()->()) = {}
    
    @objc func onClickEditReminder(_ sender: UIButton){
        self.editReminder()
    }
    
    @objc func onClickAddResult(_ sender: UIButton){
        self.addResult()
    }
}

class DetailFilterCell : UITableViewCell{
    
    @IBOutlet weak var lblIngredient: UILabel!
}

class FliterCategoryCell : UITableViewCell{
    
    @IBOutlet weak var lblFilterCategory: UILabel!
}



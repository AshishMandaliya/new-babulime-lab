//
//  ExpectedResultViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 10/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class ExpectedResultViewController: BaseViewController, UITextViewDelegate {
    
    @IBOutlet weak var backMainView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    var arrData = [dictionary]()
    var arrValue: [String] = []{
        didSet{
            textValidation()
        }
    }
    @IBOutlet weak var btnNext: UIButton!{
        didSet{
//            self.btnNext.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor?.withAlphaComponent(0.5)], for: .disabled)
//            self.btnNext.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor], for: .normal)
            
            self.btnNext.setTitleColor(UIColor.mainColor?.withAlphaComponent(0.5), for: .disabled)
            self.btnNext.setTitleColor(UIColor.mainColor, for: .normal)
            self.btnNext.isEnabled = !isEdit
        }
    }
    @IBOutlet weak var txtMethod: UITextView!{
        didSet{
            self.txtMethod.delegate = self
        }
    }
    @IBOutlet weak var txtAnswer: UITextView!{
        didSet{
            self.txtAnswer.delegate = self
        }
    }
    
    @IBOutlet weak var titleNav: UILabel!
    @IBOutlet weak var lblMustfill: UILabel!
    @IBOutlet weak var lblMethod: UILabel!
    @IBOutlet weak var lblExpected: UILabel!
    
    var isHiddenMenu = false
    var experiment_id = 0
    var isEdit = false
    var data = MustFill(data: [:])
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLanguage()
        if isEdit{
            self.arrData = data.expected_parameters
            self.txtMethod.text = data.method_of_checking
            self.txtAnswer.text = data.expected_result
            self.arrValue.removeAll()
            data.expected_parameters.forEach({self.arrValue.append($0["value"] as? String ?? "")})
            if arrValue.isEmpty{
                let dic = [
                    "token": userInfo.access_token,
                ]
                API.shared.getParameters(parameters: dic) { (arrParameters) in
                    self.arrData = arrParameters
                    self.initArray()
                    self.collectionView.reloadData()
                }
            }
            self.collectionView.reloadData()
        }else {
//            let dic = [
//                "token": userInfo.access_token,
//            ]
//            API.shared.getParameters(parameters: dic) { (arrParameters) in
//                self.arrData = arrParameters
//                self.initArray()
//                self.collectionView.reloadData()
//            }
            
            let dic: dictionary = [
                "token": self.userInfo.access_token,
                "experiment_id": self.experiment_id,
            ]
            API.shared.getMethodResult(parameters: dic) { (dic) in
                self.data = MustFill(data: dic)
                self.arrData = self.data.expected_parameters
                self.txtMethod.text = self.data.method_of_checking
                self.txtAnswer.text = self.data.expected_result
                self.arrValue.removeAll()
                self.data.expected_parameters.forEach({self.arrValue.append($0["value"] as? String ?? "")})
                if self.arrValue.isEmpty{
                    let dic = [
                        "token": self.userInfo.access_token,
                    ]
                    API.shared.getParameters(parameters: dic) { (arrParameters) in
                        self.arrData = arrParameters
                        self.initArray()
                        self.collectionView.reloadData()
                    }
                }
                self.collectionView.reloadData()
            }
        }
        //else if isdraft
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
        
        
        // api -> isdraft : true/false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    @objc func handleInternetActivity() {

        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)! {

            print("Internet Ok")

        } else {

            print("No Internet Available")
            let vc: NoInternetViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.isInternet = {
                self.remove()
            }
            self.add(vc, parent: self.view)
        }
    }
    
    fileprivate func setLanguage(){
//        self.btnNext.title = isEdit ? self.getLaunguageData(strKey: "btnTitleNavUpdate") : self.getLaunguageData(strKey:"btnTitleNavNext")
//        self.title = isEdit ? self.getLaunguageData(strKey: "titleMustFillEdit") : ""
        self.titleNav.text = isEdit ? self.getLanguageData(strKey: "titleMustFillEdit") : ""
        self.lblMustfill.text = self.getLanguageData(strKey: "titleMustFill")
        self.lblMethod.text = self.getLanguageData(strKey: "titleMethodCheck")
        self.lblExpected.text = self.getLanguageData(strKey: "titleExpected")
        self.txtMethod.placeholder = self.getLanguageData(strKey: "txtAnswerPlace")
        self.txtAnswer.placeholder = self.getLanguageData(strKey: "txtAnswerPlace")
        self.btnNext.setTitle(isEdit ? self.getLanguageData(strKey: "btnTitleNavUpdate") : self.getLanguageData(strKey:"btnTitleNavNext"), for: .normal)
        self.btnNext.setTitle(isEdit ? self.getLanguageData(strKey: "btnTitleNavUpdate") : self.getLanguageData(strKey:"btnTitleNavNext"), for: .disabled)
    }
    
    
    func initArray(){
        for _ in arrData{
            self.arrValue.append("")
        }
    }
    func textValidation(){
//        let isEmpty = arrValue.contains("")
//            if !isEmpty{
                self.btnNext.isEnabled = txtMethod.text?.isEmpty == false && txtAnswer.text?.isEmpty == false
//            }else {
//                self.btnNext.isEnabled = false
//            }
    }
    func textViewDidChange(_ textView: UITextView) {
        self.textValidation()
    }
    
    @IBAction func onClickMenu(_ sender: UIButton){
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @IBAction func onClickNotes(_ sender: UIButton){
        let vc: NotesViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.screenName = "Expected Result"
        vc.isRemove = {
            self.remove()
        }
        self.add(vc, parent: self.backMainView)
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        
        if isEdit{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.alert(title: "Draft", message: "Do you want to save information as draft?", actions: ["Cancel","Save"]) { (index) in
                if index == 0{
                    let dic: dictionary = [
                        "experiment_id": self.experiment_id,
                        "token": self.userInfo.access_token,
                    ]
                    API.shared.removeMethodResult(parameters: dic) { (msg) in
                        print(msg)
                        self.navigationController?.popViewController(animated: true)
                    }
                }else if index == 1{
                    if self.btnNext.isEnabled == true{
                        self.startLoader()
                        var dic: dictionary = [
                            "experiment_id": self.experiment_id,
                            "token": self.userInfo.access_token,
                            "method_of_checking": self.txtMethod.text ?? "",
                            "answer": self.txtAnswer.text ?? "",
                        ]
                        for (index, _) in self.arrData.enumerated() {
                            dic["parameters[\(index)][id]"] = self.arrData[index]["id"] //isEdit ? self.arrData[index]["id"] : self.arrData[index]["parameter_id"]
                            dic["parameters[\(index)][value]"] = self.arrValue[index] // isEdit ? self.arrData[index]["value"] : self.arrValue[index]
                        }
                        
                        API.shared.storeMethodAndResult(parameters: dic) { (msg) in
                            self.stopLoader()
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }else{
                        self.dismiss(animated: true) {
                            self.alert(title: "Validation", message: "To save as draft fill up all required fields.")
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func onClickNext(_ sender: UIButton){
        self.startLoader()
        var dic: dictionary = [
            "experiment_id": experiment_id,
            "token": userInfo.access_token,
            "method_of_checking": txtMethod.text ?? "",
            "answer": txtAnswer.text ?? "",
        ]
        for (index, _) in self.arrData.enumerated() {
            dic["parameters[\(index)][id]"] = self.arrData[index]["id"] //isEdit ? self.arrData[index]["id"] : self.arrData[index]["parameter_id"]
            dic["parameters[\(index)][value]"] = self.arrValue[index] // isEdit ? self.arrData[index]["value"] : self.arrValue[index]
        }
        
        API.shared.storeMethodAndResult(parameters: dic) { (msg) in
            self.stopLoader()
            print(msg)
            if self.isEdit{
                self.navigationController?.popViewController(animated: true)
            } else{
                let vc: ReminderCreateViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
                vc.experiment_id = self.experiment_id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension ExpectedResultViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        self.heightCollectionView.constant = collectionView.contentSize.height
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomResultParameterCell", for: indexPath) as? CustomResultParameterCell else { return UICollectionViewCell() }
        cell.lblName.text = arrData[indexPath.row]["name"] as? String
        cell.txtValue.text = self.arrValue[indexPath.row] //isEdit ? arrData[indexPath.row]["value"] as? String : arrValue[indexPath.row]
        
        cell.string = { str in
//            if self.isEdit{
//                self.arrData[indexPath.row]["value"] = str
//            }else {
//                self.arrValue[indexPath.row] = str
//            }
            self.arrValue[indexPath.row] = str
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.heightCollectionView.constant = collectionView.contentSize.height
    }
}


class CustomResultParameterCell: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!{
        didSet{
            self.lblName.font = UIFont.regular16
        }
    }
    @IBOutlet weak var txtValue: UITextField!{
        didSet{
            self.txtValue.addTarget(self, action: #selector(text), for: .editingChanged)
        }
    }
    
    var string: ((String)->())?
    
    @objc func text(_ sender: UITextField){
        self.string?(sender.text ?? "")
    }
}

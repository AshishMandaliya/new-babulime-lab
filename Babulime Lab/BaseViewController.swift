//
//  BaseViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 06/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit
import Foundation

class BaseViewController: UIViewController {
    
    var userId:Int{
        return UserData.shared.getUser().userInfo?.user_id ?? -1
    }
    
    var userInfo:User{
        return UserData.shared.getUser().userInfo ?? User(data: [:])
    }
    
    var viewLoader: UIView?

    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
    }
    
    func startLoader() {
        self.stopLoader()
        let loaderview = UIView(frame: self.view.frame)
        loaderview.alpha = 0.8
        loaderview.backgroundColor = .black
        
        
        let loader = UIActivityIndicatorView(style: .whiteLarge)
        loader.center = self.view.center
        loader.hidesWhenStopped = true
        loader.startAnimating()
        loaderview.addSubview(loader)

        loaderview.addSubview(loader)
        self.view.addSubview(loaderview)
        viewLoader = loaderview
    }
    
    func stopLoader() {
        viewLoader?.removeFromSuperview()
        viewLoader = nil
    }
    
    func getLanguageData(strKey : String) -> String
    {
        var lang = UserData.shared.language
//      if lang == "en" {
//        lang = "Base"
//      }
      let path = Bundle.main.path(forResource: lang, ofType: "lproj")
      let bundle = Bundle.init(path: path!)!
      return bundle.localizedString(forKey: strKey, value: nil, table: nil)
    }

}

extension UITextField{
    
    func setRightView(image: UIImage) {
        let height: CGFloat = 20.0
        let y = (self.frame.height - height)/2
        let iconView = UIImageView(frame: CGRect(x: 0, y: y, width: height, height: height)) // set your Own size
        iconView.image = image
        iconView.contentMode = .scaleAspectFit
        let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: self.frame.height))
//        iconContainerView.backgroundColor = .red
        iconContainerView.addSubview(iconView)
        rightView = iconContainerView
        rightViewMode = .always
//      self.tintColor = .lightGray
    }
    
    func setLeftIcon(_ image: UIImage) {
       let iconView = UIImageView(frame:
                      CGRect(x: 10, y: 5, width: 20, height: 20))
       iconView.image = image
       let iconContainerView: UIView = UIView(frame:
                      CGRect(x: 20, y: 0, width: 30, height: 30))
       iconContainerView.addSubview(iconView)
       leftView = iconContainerView
       leftViewMode = .always
    }
}

extension UIImageView{
    func downloadImg(url:String, placeHolder:UIImage? = #imageLiteral(resourceName: "scan")){
        if let url = URL(string: url+"png"){
            self.af_setImage(withURL: url)
        }else{
            self.image = placeHolder
        }
    }
}


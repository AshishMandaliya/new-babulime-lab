//
//  EditExpViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 15/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class EditExpViewController: BaseViewController {

    @IBOutlet weak var segment: UISegmentedControl!{
        didSet{
            self.segment.addTarget(self, action: #selector(valueChangeSegment), for: .valueChanged)
        }
    }
    
    @IBOutlet weak var verifyIngredient: UIView!
    @IBOutlet weak var verifyFormulation: UIView!
    @IBOutlet weak var verifyMustFill: UIView!
    @IBOutlet weak var verifyReminder: UIView!
    @IBOutlet weak var verifyFinalResult: UIView!
    
    @IBOutlet weak var parentView: UIView!
    let vc1: BasicInfoViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
    let vc2: EditIngredientViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
    let vc3: EditFormulationViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
    let vc4: EditMustFillViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
    let vc5: EditReminderViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
    let vc6: EditFinalResultViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
    
    var isHiddenMenu = false
    var data: dictionary = [:]
    var experiment_id = 0
    var experiment_result_id = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        getData()
        
        setLanguage()
        
        //Nav
//        let back = UIBarButtonItem()
//        back.title = getLaunguageData(strKey: "btnBack")
//        self.navigationItem.backBarButtonItem = back
        //API.shared.serverDown()
    }
    
    fileprivate func setLanguage(){
        self.segment.setTitle(self.getLanguageData(strKey: "segBasic"), forSegmentAt: 0)
        self.segment.setTitle(self.getLanguageData(strKey: "segIng"), forSegmentAt: 1)
        self.segment.setTitle(self.getLanguageData(strKey: "segFormulation"), forSegmentAt: 2)
        self.segment.setTitle(self.getLanguageData(strKey: "segMustFill"), forSegmentAt: 3)
        self.segment.setTitle(self.getLanguageData(strKey: "segReminder"), forSegmentAt: 4)
        self.segment.setTitle(self.getLanguageData(strKey: "segFinalResult"), forSegmentAt: 5)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
        getData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    @objc func handleInternetActivity() {

        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)! {

            print("Internet Ok")

        } else {

            print("No Internet Available")
            let vc: NoInternetViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.isInternet = {
                self.remove()
            }
            self.add(vc, parent: self.view)
        }
    }
    
    private func getData(){
        self.startLoader()
        var dic: dictionary = [
            "token": self.userInfo.access_token,
            "experiment_id": self.experiment_id,
            
        ]
        if self.experiment_result_id != 0{
            dic["experiment_result_id"] = self.experiment_result_id
        }
        
        API.shared.viewExperimentDetail(parameters: dic) { (dataDetail) in
            self.stopLoader()
            self.data = dataDetail
//            self.vc1.data = Basic(data: self.data["basic"] as? dictionary ?? [:])
//            self.add(self.vc1, parent: self.parentView)
            self.segmentSelected(index: self.segment.selectedSegmentIndex)
            
            let verifyTabs = self.data["verify_tabs"] as? dictionary ?? [:]
            self.verifyIngredient.isHidden = verifyTabs["ingredient_verify"] as? Bool ?? false
            self.verifyFormulation.isHidden = verifyTabs["formulation_verify"] as? Bool ?? false
            self.verifyMustFill.isHidden = verifyTabs["must_fill_verify"] as? Bool ?? false
            self.verifyReminder.isHidden = verifyTabs["reminder_verify"] as? Bool ?? false
            self.verifyFinalResult.isHidden = verifyTabs["final_result_verify"] as? Bool ?? false
        }
    }
    
//    private func addChildSubview(){
//
//        self.parentView.addSubview(vc1.view)
//        self.parentView.addSubview(vc2.view)
//        self.parentView.addSubview(vc3.view)
//        self.parentView.addSubview(vc4.view)
//        self.parentView.addSubview(vc5.view)
//        self.parentView.addSubview(vc6.view)
//
//        vc1.view.frame = parentView.bounds
//        vc2.view.frame = parentView.bounds
//        vc3.view.frame = parentView.bounds
//        vc4.view.frame = parentView.bounds
//        vc5.view.frame = parentView.bounds
//        vc6.view.frame = parentView.bounds
//
//        //set first view
//        self.parentView.bringSubviewToFront(vc1.view)
//
//    }
    
    @IBAction func onClickMenu(_ sender: UIButton){
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }

    @objc func valueChangeSegment(_ sender: UISegmentedControl){
        
        self.remove()
        segmentSelected(index: sender.selectedSegmentIndex)
    }
    
    
    
    private func segmentSelected(index: Int){
        switch index{
        case 0 :
            vc1.data = Basic(data: data["basic"] as? dictionary ?? [:])
            self.add(vc1, parent: parentView)
        case 1 :
            let ingredientArray = data["ingredient"] as? [dictionary] ?? []
            vc2.IngredientList.removeAll()
            ingredientArray.forEach {vc2.IngredientList.append(AddedIngredient(data: $0))}
            vc2.experiment_id = self.experiment_id
            self.add(vc2, parent: parentView)
            vc2.viewWillAppear(true)
            
        case 2 :
            let stepArray = data["formulation"] as? [dictionary] ?? []
            vc3.arrStep.removeAll()
            stepArray.forEach({vc3.arrStep.append(FormulationStep(data: $0))})
            vc3.experiment_id = self.experiment_id
            self.add(vc3, parent: parentView)
            vc3.viewWillAppear(true)
        case 3 :
            vc4.data = MustFill(data: data["must_fill"] as? dictionary ?? [:])
            vc4.experiment_id = self.experiment_id
            self.add(vc4, parent: parentView)
            vc4.viewWillAppear(true)
        case 4 :
            vc5.data = ViewReminder(data: data["reminder"] as? dictionary ?? [:])
            vc5.experiment_id = self.experiment_id
            self.add(vc5, parent: parentView)
            vc5.viewWillAppear(true)
        case 5 :
            let dic = data["final_result"] as? [dictionary] ?? []
            vc6.finalResult = FinalResult(data: dic.first ?? [:])
            vc6.experiment_id = self.experiment_id
            vc6.experiment_result_id = self.experiment_result_id
            self.add(vc6, parent: parentView)
            vc6.viewWillAppear(true)
         default:
            return
        }
    }
}


public extension UIViewController {

    /// Adds child view controller to the parent.
    ///
    /// - Parameter child: Child view controller.
    func add(_ child: UIViewController, parent: UIView) {
        addChild(child)
        parent.addSubview(child.view)
        child.view.frame = parent.bounds
        child.didMove(toParent: self)
    }

    /// It removes the child view controller from the parent.
    func remove() {
        self.children.forEach {
            $0.willMove(toParent: nil)
            $0.view.removeFromSuperview()
            $0.removeFromParent()
        }
    }
}

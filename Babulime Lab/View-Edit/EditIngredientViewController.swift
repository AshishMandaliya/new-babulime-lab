//
//  EditIngredientViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 15/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class EditIngredientViewController: BaseViewController {
    
    @IBOutlet weak var backMainView: UIView!
    @IBOutlet weak var tblIngredient: UITableView!{
        didSet{
            self.tblIngredient.delegate = self
            self.tblIngredient.dataSource = self
        }
    }
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPurity: UILabel!
    @IBOutlet weak var lblPurpose: UILabel!
    @IBOutlet weak var lblContactName: UILabel!
    @IBOutlet weak var lblContactNumber: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var imgProductDetailSheet: UIImageView!
    @IBOutlet weak var imgCertificate: UIImageView!
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblIngName: UILabel!
    @IBOutlet weak var lblIngPurity: UILabel!
    @IBOutlet weak var lblIngPurpose: UILabel!
    @IBOutlet weak var lblIngProduct: UILabel!
    @IBOutlet weak var lblIngCerti: UILabel!
    @IBOutlet weak var lblIngContactName: UILabel!
    @IBOutlet weak var lblIngContactNumber: UILabel!
    @IBOutlet weak var lblIngCompanyName: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    var selectedIndex = 0
    var IngredientList = [AddedIngredient]()
    var experiment_id = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setLanguage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblIngredient.reloadData()
        if !IngredientList.isEmpty{
            viewData(data: IngredientList[selectedIndex])
        }
    }
    
    fileprivate func setLanguage(){
        self.lblTitle.text = getLanguageData(strKey: "titleIngredient")
        self.lblIngName.text = getLanguageData(strKey: "titleIngName")
        self.lblIngPurity.text = getLanguageData(strKey: "titleIngPurity")
        self.lblIngPurpose.text = getLanguageData(strKey: "titleIngPurpose")
        self.lblIngProduct.text = getLanguageData(strKey: "titleIngProduct")
        self.lblIngCerti.text = getLanguageData(strKey: "titleIngCerti")
        self.lblIngContactName.text = getLanguageData(strKey: "titleIngContactName")
        self.lblIngContactNumber.text = getLanguageData(strKey: "titleIngContactNumber")
        self.lblIngCompanyName.text = getLanguageData(strKey: "titleIngCompanyName")
        self.btnEdit.setTitle(self.getLanguageData(strKey: "btnEdit"), for: .normal)
    }
    
    fileprivate func viewData(data: AddedIngredient){
        self.lblName.text = data.name
        self.lblPurity.text = data.property_or_purity
        self.lblPurpose.text = data.purpose
        self.lblContactName.text = data.contact_name
        self.lblContactNumber.text = data.contact_number
        self.lblCompanyName.text = data.company_name
        let sheet = URL(string: data.product_detail_sheet)
        let certificate = URL(string: data.certificate)
        let parentVC = self.parent as! EditExpViewController
        parentVC.startLoader()
        DispatchQueue.global().async {
            let imgSheet = try? Data(contentsOf: sheet!)
            let imgCertificate = try? Data(contentsOf: certificate!)//make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            DispatchQueue.main.async {
                self.imgProductDetailSheet.image = UIImage(data: imgSheet!)
                self.imgCertificate.image = UIImage(data: imgCertificate!)
                parentVC.stopLoader()
            }
            
        }
        
//        if let data = try? Data(contentsOf: URL(string: data.product_detail_sheet) ?? URL(fileURLWithPath: "")){
//            self.imgProductDetailSheet.image = UIImage(data: data)
//        }
//        if let data = try? Data(contentsOf: URL(string: data.certificate) ?? URL(fileURLWithPath: "")){
//            self.imgCertificate.image = UIImage(data: data)
//        }
//        self.imgProductDetailSheet.downloadImg(url: data.product_detail_sheet)
//        self.imgCertificate.downloadImg(url: data.certificate)
    }
    
    @IBAction func onClickEdit(_ sender: UIButton){
        let vc: CreateEx2ViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.isEdit = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickNotes(_ sender: UIButton){
        let vc: NotesViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.screenName = "Ingredient"
        vc.isRemove = {
            self.remove()
        }
        self.add(vc, parent: self.backMainView)
    }
}


 // MARK:- UITableViewDelegate, UITableViewDataSource
extension EditIngredientViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return IngredientList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "IngredientCell", for: indexPath) as? IngredientCell else { return UITableViewCell() }
        if indexPath.row == selectedIndex{
            cell.viewBack.backgroundColor = UIColor.mainColor
            cell.lblName.textColor = UIColor.secondaryColor
        }else{
            cell.viewBack.backgroundColor = UIColor.clear
            cell.lblName.textColor = UIColor.black
        }
        cell.lblName.text = IngredientList[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        viewData(data: IngredientList[selectedIndex])
        tableView.reloadData()
    }
    
    
}

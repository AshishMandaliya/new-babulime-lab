//
//  EditFinalResultViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 17/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class EditFinalResultViewController: BaseViewController {
    
    @IBOutlet weak var collectionView1: UICollectionView!{
        didSet{
            self.collectionView1.delegate = self
            self.collectionView1.dataSource = self
        }
    }
    @IBOutlet weak var collectionView2: UICollectionView!{
        didSet{
            self.collectionView2.delegate = self
            self.collectionView2.dataSource = self
        }
    }
    @IBOutlet weak var heightCollectionView1: NSLayoutConstraint!
    @IBOutlet weak var heightCollectionView2: NSLayoutConstraint!
    
    @IBOutlet weak var lblIsSucceed: UILabel!
    @IBOutlet weak var lblIsExpected: UILabel!
    @IBOutlet weak var lblSucceedText: UILabel!
    @IBOutlet weak var lblExpecteText: UILabel!
    @IBOutlet weak var lblCustomResult: UILabel!
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleSuccess: UILabel!
    @IBOutlet weak var lblTitleExpect: UILabel!
    @IBOutlet weak var lblTitleCustom: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    var arrParameters1 = [dictionary]()
    var arrParameters2 = [dictionary]()
    var finalResult = FinalResult(data: [:])
    var experiment_result_id = 0
    var experiment_id = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setLanguage()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewData()
    }
    
    fileprivate func setLanguage(){
        self.lblTitle.text = getLanguageData(strKey: "titleFinalResult")
        self.lblTitleSuccess.text = getLanguageData(strKey: "titleSuccessResult")
        self.lblTitleExpect.text = getLanguageData(strKey: "titleExpectedResult")
        self.lblTitleCustom.text = getLanguageData(strKey: "titleCustom")
        self.btnEdit.setTitle(self.getLanguageData(strKey: "btnEdit"), for: .normal)
    }
    
    @IBAction func onClickEdit(_ sender: UIButton){
        let vc: FinalResultViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.experiment_result_id = self.experiment_result_id
        vc.arrExperiment.append(finalResult)
        vc.arrParameters1 = self.arrParameters1
        vc.arrParameters2 = self.arrParameters2
        vc.isEdit = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickNotes(_ sender: UIButton){
        let vc: NotesViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.screenName = "Final Result"
        vc.isRemove = {
            self.remove()
        }
        self.add(vc, parent: self.view)
    }
    
    
    func viewData(){
        if finalResult.is_succeed{
            self.lblIsSucceed.text = "YES"
            self.lblIsSucceed.textColor = UIColor.green
        }else {
            self.lblIsSucceed.text = "NO"
            self.lblIsSucceed.textColor = UIColor.red
        }
        if finalResult.as_per_expected{
            self.lblIsExpected.text = "YES"
            self.lblIsExpected.textColor = UIColor.green
        }else {
            self.lblIsExpected.text = "NO"
            self.lblIsExpected.textColor = UIColor.red
        }
        self.lblSucceedText.text = finalResult.result_description
        self.lblExpecteText.text = finalResult.expected_result_description
        self.lblCustomResult.text = finalResult.custom_result
        let arrParameters = finalResult.data
        arrParameters1 = arrParameters.filter { (dic) -> Bool in
            if dic["is_actual_result"] as? Bool == true{
                return true
            }
            return false
        }
        arrParameters2 = arrParameters.filter { (dic) -> Bool in
            if dic["is_actual_result"] as? Bool == false{
                return true
            }
            return false
        }
        collectionView1.reloadData()
        collectionView2.reloadData()
    }

}

extension EditFinalResultViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionView1{
            return arrParameters1.count
        }else if collectionView == collectionView2{
            return arrParameters2.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomViewParameterCell", for: indexPath) as? CustomViewParameterCell else { return UICollectionViewCell() }
        if collectionView == collectionView1{
            cell.lblName.text = arrParameters1[indexPath.row]["name"] as? String
            cell.lblValue.text = arrParameters1[indexPath.row]["value"] as? String
            
            self.heightCollectionView1.constant = collectionView.contentSize.height
        }else if collectionView == collectionView2{
            cell.lblName.text = arrParameters2[indexPath.row]["name"] as? String
            cell.lblValue.text = arrParameters2[indexPath.row]["value"] as? String
            
            self.heightCollectionView2.constant = collectionView.contentSize.height
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.heightCollectionView1.constant = collectionView.contentSize.height
        self.heightCollectionView2.constant = collectionView.contentSize.height
    }
}

 

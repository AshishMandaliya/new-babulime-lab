//
//  BasicInfoViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 15/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class Basic {
    var name: String
    var reason_for_development: String
    var priority: String
    var types_of_recipe: String
    var chain_experiment: dictionary
    
    init(data: dictionary) {
        self.name = data["name"] as? String ?? ""
        self.reason_for_development = data["reason_for_development"] as? String ?? ""
        self.priority = data["priority"] as? String ?? ""
        self.types_of_recipe = data["types_of_recipe"] as? String ?? ""
        self.chain_experiment = data["chain_experiment"] as? dictionary ?? [:]
    }
}

class BasicInfoViewController: BaseViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblReason: UILabel!
    @IBOutlet weak var lblTypeRecipe: UILabel!
    @IBOutlet weak var lblChainExp: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var viewPriority: UIView!
    
    
    @IBOutlet weak var titleBasic: UILabel!
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var titleReason: UILabel!
    @IBOutlet weak var titlePriority: UILabel!
    @IBOutlet weak var titleTypeOfRecipe: UILabel!
    @IBOutlet weak var titleCainExp: UILabel!
    
    var data = Basic(data: [:])
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let basicData = Basic(data: data)
        
        setLanguage()
        getData()
        
    }
    
    fileprivate func getData(){
        lblName.text = data.name
        lblReason.text = data.reason_for_development
        lblTypeRecipe.text = data.types_of_recipe
        lblPriority.text = data.priority
        if lblPriority.text == "Urgent" || lblPriority.text == "urgent" {
            self.viewPriority.backgroundColor = UIColor.urgentColor
            self.lblPriority.textColor = UIColor.urgentColor
        }else {
            self.viewPriority.backgroundColor = UIColor.normalColor
            self.lblPriority.textColor = UIColor.normalColor
        }
        lblChainExp.text = data.chain_experiment["name"] as? String ?? "None"
    }
    
    fileprivate func setLanguage(){
        self.titleBasic.text = self.getLanguageData(strKey: "titleBasicInfo")
        self.titleName.text = self.getLanguageData(strKey: "name")
        self.titleReason.text = self.getLanguageData(strKey: "reason")
        self.titlePriority.text = self.getLanguageData(strKey: "priority")
        self.titleTypeOfRecipe.text = self.getLanguageData(strKey: "titleRecipeType")
        self.titleCainExp.text = self.getLanguageData(strKey: "chainExp")
        
    }
    
//    @IBAction func onClickMenu(_ sender: UIBarButtonItem){
//        UIView.animate(withDuration: 0.5) { () -> Void in
//            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
//            self.isHiddenMenu.toggle()
//        }
//    }

}

//
//  EditReminderViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 17/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class ViewReminder{
    var datetime: String
    var repeatt: String
    var expected_result_reminder: String
    var custom_repeat: String
    var number_of_days: String
    var is_reminder_set: Bool
    var is_reminder_result: Bool
    
    init(data: dictionary){
        self.datetime = data["datetime"] as? String ?? ""
        let repeated = data["repeat"] as? String ?? ""
        switch repeated {
        case "hourly":
            self.repeatt = "Hourly"
        case "everyday":
            self.repeatt = "Every Day"
        case "every_week":
            self.repeatt = "Every Week"
        case "every_month":
            self.repeatt = "Every Month"
        case "custom":
            self.repeatt = "Custom"
        default:
            self.repeatt = "None"
        }
        self.expected_result_reminder = data["expected_result_reminder"] as? String ?? ""
        let customRepeat = data["custom_repeat"] as? String ?? ""
        switch customRepeat {
        case "weekly":
            self.custom_repeat = "Weekly"
        case "monthly":
            self.custom_repeat = "Monthly"
        case "yearly":
            self.custom_repeat = "Yearly"
        default:
            self.custom_repeat = "Daily"
        }
        self.number_of_days = data["number_of_days"] as? String ?? ""
        
        self.is_reminder_set = data["is_reminder_set"] as? Bool ?? false
        self.is_reminder_result = data["is_reminder_result"] as? Bool ?? false
    }
}

class EditReminderViewController: BaseViewController {
    
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblRepeat: UILabel!
    @IBOutlet weak var lblExpectedResultReminder: UILabel!
    @IBOutlet weak var lblFrequency: UILabel!
    @IBOutlet weak var lblEvery: UILabel!
    @IBOutlet weak var stcCustom: UIStackView!
    
    @IBOutlet weak var lblReminder: UILabel!
    @IBOutlet weak var lblGetReminder: UILabel!
    @IBOutlet weak var lblTitleRepeat: UILabel!
    @IBOutlet weak var lblResultOptional: UILabel!
    @IBOutlet weak var lblTitleFrequency: UILabel!
    @IBOutlet weak var lblTitleEvery: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var stcReminder: UIStackView!
    @IBOutlet weak var stcResult: UIStackView!

    var data = ViewReminder(data: [:])
    var experiment_id = 0
    var isReminder = false
    var isResult = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setLanguage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewData()
    }
    
    fileprivate func setLanguage(){
        self.lblReminder.text = self.getLanguageData(strKey: "titleReminder")
        self.lblGetReminder.text = self.getLanguageData(strKey: "titleResultReminder")
        self.lblTitleRepeat.text = self.getLanguageData(strKey: "titleRepeat")
        self.lblResultOptional.text = self.getLanguageData(strKey: "titleYourResult")
        self.lblTitleFrequency.text = self.getLanguageData(strKey: "titleFrequency")
        self.lblTitleEvery.text = self.getLanguageData(strKey: "titleEvery")
        self.btnEdit.setTitle(self.getLanguageData(strKey: "btnEdit"), for: .normal)
    }
    
    func viewData(){
        
        self.isReminder = data.is_reminder_set
        self.isResult = data.is_reminder_result
        
        self.stcReminder.isHidden = !isReminder
        self.stcResult.isHidden = !isResult
        
        if isReminder{
            self.lblDateTime.text = data.datetime
            self.lblRepeat.text = data.repeatt
            if lblRepeat.text == "Custom"{
                self.stcCustom.isHidden = false
                lblFrequency.text = data.custom_repeat
                lblEvery.text = data.number_of_days
            }else {
                self.stcCustom.isHidden = true
            }
        }
        if isResult{
            self.lblExpectedResultReminder.text = data.expected_result_reminder
        }
        
        
    }
    
    @IBAction func onClickEdit(_ sender: UIButton){
        let vc: ReminderCreateViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.data = self.data
        vc.isEdit = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickNotes(_ sender: UIButton){
        let vc: NotesViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.screenName = "Reminder"
        vc.isRemove = {
            self.remove()
        }
        self.add(vc, parent: self.view)
    }
    
}

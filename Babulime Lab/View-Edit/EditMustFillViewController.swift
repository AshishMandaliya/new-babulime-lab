//
//  EditMustFillViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 17/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class MustFill {
    let method_of_checking: String
    let expected_parameters: [dictionary]
    let expected_result: String

    init(data: dictionary){
        self.method_of_checking = data["method_of_checking"] as? String ?? ""
        self.expected_parameters = data["expected_parameters"] as? [dictionary] ?? []
        self.expected_result = data["expected_result"] as? String ?? ""
    }
}

class EditMustFillViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var lblMethodChecking: UILabel!
    @IBOutlet weak var lblExpectedResult: UILabel!
    
    @IBOutlet weak var lblMustfill: UILabel!
    @IBOutlet weak var lblMethod: UILabel!
    @IBOutlet weak var lblExpected: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    var data = MustFill(data: [:])
    var arrParameter: [dictionary] = []
    var experiment_id = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLanguage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewData()
    }
    
    fileprivate func setLanguage(){
        self.lblMustfill.text = self.getLanguageData(strKey: "titleMustFill")
        self.lblMethod.text = self.getLanguageData(strKey: "titleMethodCheck")
        self.lblExpected.text = self.getLanguageData(strKey: "titleExpected")
        self.btnEdit.setTitle(self.getLanguageData(strKey: "btnEdit"), for: .normal)
    }
    
    func viewData(){
        self.arrParameter = data.expected_parameters
        lblExpectedResult.text = data.expected_result
        lblMethodChecking.text = data.method_of_checking
        collectionView.reloadData()
    }

    @IBAction func onClickEdit(_ sender: UIButton){
        let vc: ExpectedResultViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.data = self.data
        vc.isEdit = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickNotes(_ sender: UIButton){
        let vc: NotesViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.screenName = "Expected Result"
        vc.isRemove = {
            self.remove()
        }
        self.add(vc, parent: self.view)
    }
}

extension EditMustFillViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrParameter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        self.heightCollectionView.constant = collectionView.contentSize.height
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomViewParameterCell", for: indexPath) as? CustomViewParameterCell else { return UICollectionViewCell() }
        cell.lblName.text = arrParameter[indexPath.row]["name"] as? String
        cell.lblValue.text = arrParameter[indexPath.row]["value"] as? String
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.heightCollectionView.constant = collectionView.contentSize.height
    }
}

class CustomViewParameterCell: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblValue: UILabel!
}

//
//  EditFormulationViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 16/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class EditFormulationViewController: BaseViewController {
    
    @IBOutlet weak var tblFormulation: UITableView!{
        didSet{
            self.tblFormulation.delegate = self
            self.tblFormulation.dataSource = self
        }
    }
    
    
    @IBOutlet weak var lblTitleFormulation: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    var arrStep = [FormulationStep]()
    var selectedIndex = 0
    var experiment_id = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLanguage()
    }
    
    fileprivate func setLanguage(){
        self.lblTitleFormulation.text = self.getLanguageData(strKey: "titleFormulation")
        self.btnEdit.setTitle(self.getLanguageData(strKey: "btnEdit"), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblFormulation.reloadData()
    }
    
    @IBAction func onClickEdit(_ sender: UIButton){
        let vc: FormulationViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.isEdit = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickNotes(_ sender: UIButton){
        let vc: NotesViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        vc.experiment_id = self.experiment_id
        vc.screenName = "Formulation"
        vc.isRemove = {
            self.remove()
        }
        self.add(vc, parent: self.view)
    }

}

extension EditFormulationViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrStep.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EditFormulationCell", for: indexPath) as? EditFormulationCell else { return UITableViewCell() }
        
        cell.lblTitleStep.text = self.getLanguageData(strKey: "titleStep")
        cell.lblTitleIngNameCode.text = self.getLanguageData(strKey: "titleIngNameCode")
        cell.lblTitleQty.text = self.getLanguageData(strKey: "titleQty")
        
        cell.lblStepNum.text = arrStep[indexPath.row].step
        cell.lblIngredientName.text = arrStep[indexPath.row].ingredient_name_or_code
        cell.lblQuantity.text = arrStep[indexPath.row].quantity
        
        return cell
    }
}

class EditFormulationCell: UITableViewCell {
    @IBOutlet weak var lblStepNum: UILabel!
    @IBOutlet weak var lblIngredientName: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    
    @IBOutlet weak var lblTitleStep: UILabel!
    @IBOutlet weak var lblTitleIngNameCode: UILabel!
    @IBOutlet weak var lblTitleQty: UILabel!
}

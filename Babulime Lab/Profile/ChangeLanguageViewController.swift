//
//  ChangeLanguageViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 05/10/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class ChangeLanguageViewController: BaseViewController {
    
    @IBOutlet weak var tblLanguage: UITableView!{
        didSet{
            self.tblLanguage.delegate = self
            self.tblLanguage.dataSource = self
        }
    }
    @IBOutlet weak var lblTitleChangeLanguage: UILabel!
    
    var selectedLanguageIndex = 0
    var arrLanguage: [String] = []{
        didSet{
            self.tblLanguage.reloadData()
        }
    }

    var isHiddenMenu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        selectedLanguageIndex = UserData.shared.language == "en" ? 0 : 1
        self.lblTitleChangeLanguage.text = getLanguageData(strKey: "titleChangeLanguage")
        arrLanguage = [getLanguageData(strKey: "eng"), getLanguageData(strKey: "guj")]
        tblLanguage.tableFooterView = UIView()
    }
    
    
    //MARK: - Action
    @IBAction func onClickMenu(_ sender: UIButton){
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }

}

extension ChangeLanguageViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLanguage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageCell") as? LanguageCell else { return UITableViewCell() }
        cell.lblLanguage.text = arrLanguage[indexPath.row]
        cell.accessoryType = .none
        if selectedLanguageIndex == indexPath.row {
            cell.accessoryType = .checkmark
        }
        
        cell.tintColor = UIColor.mainColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedLanguageIndex = indexPath.row
        UserData.shared.setLanguage(language: self.selectedLanguageIndex == 0 ? "en" : "gu-IN")
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

class LanguageCell: UITableViewCell{
    
    @IBOutlet weak var lblLanguage: UILabel!
}

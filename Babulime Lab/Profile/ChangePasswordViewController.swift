//
//  ChangePasswordViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 22/09/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class ChangePasswordViewController: BaseViewController {
    
    @IBOutlet weak var txtCurrentPassword: UITextField!{
        didSet{
            self.txtCurrentPassword.addTarget(self, action: #selector(txt), for: .editingChanged)
            self.txtCurrentPassword.rightViewWithButton()
        }
    }
    
    @IBOutlet weak var txtNewPassword: UITextField!{
        didSet{
            self.txtNewPassword.addTarget(self, action: #selector(txt), for: .editingChanged)
            self.txtNewPassword.rightViewWithButton()
        }
    }
    @IBOutlet weak var txtRepeatPassword: UITextField!{
        didSet{
            self.txtRepeatPassword.addTarget(self, action: #selector(txt), for: .editingChanged)
            self.txtRepeatPassword.rightViewWithButton()
        }
    }
    
    @IBOutlet weak var btnChange: UIButton!{
        didSet{
//            self.btnChange.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor?.withAlphaComponent(0.5)], for: .disabled)
//            self.btnChange.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor], for: .normal)
            
            self.btnChange.setTitleColor(UIColor.mainColor?.withAlphaComponent(0.5), for: .disabled)
            self.btnChange.setTitleColor(UIColor.mainColor, for: .normal)
            self.btnChange.isEnabled = false
        }
    }
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleNewPassword: UILabel!
    @IBOutlet weak var lblTitleRepeatPassword: UILabel!
    @IBOutlet weak var lblTitleCurrentPassword: UILabel!
    
    var isHiddenMenu = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLanguage()
    }
    
    fileprivate func setLanguage(){
        self.lblTitle.text = self.getLanguageData(strKey: "titleChangePassword")
        self.btnChange.setTitle(self.getLanguageData(strKey: "btnTitleNavChange"), for: .disabled)
        self.btnChange.setTitle(self.getLanguageData(strKey: "btnTitleNavChange"), for: .normal)
        self.lblTitleNewPassword.text = self.getLanguageData(strKey: "titleNewPassword")
        self.lblTitleRepeatPassword.text = self.getLanguageData(strKey: "titleRepeatPassword")
        self.lblTitleCurrentPassword.text = self.getLanguageData(strKey: "titleCurrentPassword")
    }
    
    //MARK:- Action
    @objc func txt(sender:UITextField){
        self.btnChange.isEnabled = txtCurrentPassword.text?.isEmpty == false && txtNewPassword.text?.isEmpty == false && txtRepeatPassword.text?.isEmpty == false && txtNewPassword.text == txtRepeatPassword.text
    }
    
    @IBAction func onClickChange(_ sender: UIButton){
        let dic: dictionary = [
            "user_id" : self.userId,
            "token": self.userInfo.access_token,
            "new_password": self.txtNewPassword.text ?? "",
            "repeate_password": self.txtRepeatPassword.text ?? "",
            "current_password": self.txtCurrentPassword.text ?? ""
        ]
        self.startLoader()
        API.shared.changePassword(parameters: dic) { (status) in
            self.stopLoader()
            if status{
                self.navigationController?.popViewController(animated: true)
            }else{
                self.alert(title: self.getLanguageData(strKey: "alertTitleWrongPassword"), message: self.getLanguageData(strKey: "alertMsgWrongPassword"))
            }
        }
    }
    
    @IBAction func onClickMenu(_ sender: UIButton){
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }

}

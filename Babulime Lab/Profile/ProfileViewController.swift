//
//  ProfileViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 19/09/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {

    @IBOutlet weak var tblView: UITableView!{
        didSet{
            self.tblView.delegate = self
            self.tblView.dataSource = self
        }
    }
    @IBOutlet weak var txtName: UITextField!{
        didSet{
            self.txtName.addTarget(self, action: #selector(onClickTxtName), for: .editingChanged)
        }
    }
    @IBOutlet weak var imgProfile: UIImageView!{
        didSet{
            self.imgProfile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClickProfileImage)))
        }
    }
    @IBOutlet weak var btnUpdate: UIButton!{
        didSet{
//            self.btnUpdate.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor?.withAlphaComponent(0.5)], for: .disabled)
//            self.btnUpdate.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.mainColor], for: .normal)
            
            self.btnUpdate.setTitleColor(UIColor.mainColor?.withAlphaComponent(0.5), for: .disabled)
            self.btnUpdate.setTitleColor(UIColor.mainColor, for: .normal)
            self.btnUpdate.isEnabled = false
        }
    }
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleName: UILabel!
    
    var arrSection = ["UNLOCK WITH TOUCH ID", "GENERAL"]
    var arrUnlock = ["Require Touch ID"]
    var arrUnlockIcon = ["touchId"]
    var arrMenu = ["Change Language","Change Password", "Log Out"]
    var arrMenuIcon = ["changeLanguage","changePassword", "logout"]
    
    var isHiddenMenu = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.tblView.tableFooterView = UIView()
        self.txtName.text = userInfo.name
        
//        let profileImage = URL(string: self.userInfo.profile_image)
//        self.startLoader()
//        DispatchQueue.global().async {
//            let imgProfile = try? Data(contentsOf: profileImage!)
//            DispatchQueue.main.async {
//                self.imgProfile.image = UIImage(data: imgProfile!)
//                self.stopLoader()
//                self.txt()
//            }
//        }
        
        if let data = try? Data(contentsOf: URL(string: userInfo.profile_image) ?? URL(fileURLWithPath: "")){
            imgProfile.image = UIImage(data: data)
        }
        self.txt()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setLanguage()
    }
    
    fileprivate func setLanguage(){
//        self.title = self.getLaunguageData(strKey: "titleProfile")
        self.lblTitle.text = self.getLanguageData(strKey: "titleProfile")
        self.lblTitleName.text = self.getLanguageData(strKey: "titleProfileName")
//        self.btnUpdate.title = self.getLaunguageData(strKey: "btnTitleNavUpdate")
        self.btnUpdate.setTitle(self.getLanguageData(strKey: "btnTitleNavUpdate"), for: .normal)
        self.btnUpdate.setTitle(self.getLanguageData(strKey: "btnTitleNavUpdate"), for: .disabled)
        self.tblView.reloadData()
    }
    
    @IBAction func onClickMenu(_ sender: UIButton){
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @objc func onClickProfileImage(_ gesture: UITapGestureRecognizer){
        ImagePickerManager().pickImage(self, view: imgProfile) { (image) in
            self.imgProfile.image = image
            self.txt()
        }
        
    }
    
    @objc func onClickTxtName(_ sender: UITextField){
        txt()
    }
    
    func txt(){
        self.btnUpdate.isEnabled = self.txtName.text?.isEmpty == false
    }
    
    @IBAction func onClickUpdate(_ sender: UIButton){
        let dic: dictionary = [
            "token": userInfo.access_token,
            "user_id": userId,
            "name": txtName.text ?? "",
        ]
        
        API.shared.profileUpdate(vc: self, param: dic, postImage: imgProfile.image, imageName: "profileImage") { (message) in
            print(message)
            self.alert(title: self.getLanguageData(strKey: "alertError"), message: self.getLanguageData(strKey: "alertWentWrong"))
        } success: { (dic) in
            print(dic)
            let data = dic["data"] as? dictionary
            UserData.shared.setUser(userinfo: data ?? [:])
            self.txtName.text = self.userInfo.name
            
//            let profileImage = URL(string: self.userInfo.profile_image)
//            self.startLoader()
//            DispatchQueue.global().async {
//                let imgProfile = try? Data(contentsOf: profileImage!)
//                DispatchQueue.main.async {
//                    self.imgProfile.image = UIImage(data: imgProfile!)
//                    self.stopLoader()
//                }
//
//            }
            
            
            if let data = try? Data(contentsOf: URL(string: self.userInfo.profile_image) ?? URL(fileURLWithPath: "")){
                self.imgProfile.image = UIImage(data: data)
            }
        }
    }

}


extension ProfileViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return self.arrUnlock.count
        }else if section == 1{
            return self.arrMenu.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as? ProfileCell else { return UITableViewCell() }
            cell.lblMenu.text = getLanguageData(strKey: "titleProfileS\(indexPath.section)R\(indexPath.row)")
        if indexPath.section == 0{
            cell.imgMenuIcon.image = UIImage(named: arrUnlockIcon[indexPath.row])
            
            let switchView = UISwitch(frame: .zero)
            switchView.setOn(UserData.shared.getTouchId(), animated: true)
            switchView.tag = indexPath.row // for detect which row switch Changed
            switchView.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
            cell.accessoryView = switchView
            
            
        }else if indexPath.section == 1{
            cell.imgMenuIcon.image = UIImage(named: arrMenuIcon[indexPath.row])
            cell.accessoryType = .disclosureIndicator
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0{
            
        }else if indexPath.section == 1{
            if indexPath.row == 0{
                let vc: ChangeLanguageViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 1{
                let vc: ChangePasswordViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 2{
                UserData.shared.removeUser()
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.rootToLogin()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text =  getLanguageData(strKey: "titleProfileSection\(section)") //arrSection[section]
        label.font = UIFont.medium17
        label.textColor = UIColor(hex: "#3c3c4399")
        
        headerView.backgroundColor = UIColor.white
        headerView.addSubview(label)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0{
            let footerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
            
            let view = UIView(frame: CGRect.init(x: 0, y: 5, width: footerView.frame.width, height: 0.5))
            view.backgroundColor = UIColor(hex: "#3c3c435c")
            
            let label = UILabel(frame: CGRect.init(x: 0, y: 15, width: footerView.frame.width, height: footerView.frame.height-10))
            label.numberOfLines = 0;
            label.font = UIFont.regular16
            label.text  = self.getLanguageData(strKey: "titleProfileS\(section)Footer")//"When enabled, you’ll need to use Touch ID to unlock."
            label.textColor = UIColor(hex: "#3c3c4399")
            
            footerView.backgroundColor = UIColor.white
            footerView.addSubview(view)
            footerView.addSubview(label)
            
            return footerView
        }
        return UIView()
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0{
            return 50
        }else{
            return 0
        }
    }
    
    @objc func switchChanged(_ sender: UISwitch){
        UserData.shared.setTouchId(enable: sender.isOn)
    }
}


class ProfileCell: UITableViewCell{
    
    @IBOutlet weak var imgMenuIcon: UIImageView!
    @IBOutlet weak var lblMenu: UILabel!
}


extension UIImage {

    convenience init?(color: UIColor) {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()

        context?.setFillColor(color.cgColor)
        context?.fill(rect)

        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}




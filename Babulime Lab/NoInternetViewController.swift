//
//  NoInternetViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 15/09/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class NoInternetViewController: BaseViewController {
    
    @IBOutlet weak var titleMsgNoInternet: UILabel!
    @IBOutlet weak var subTitleMsgNoInternet: UILabel!
    @IBOutlet weak var btnTryAgain: UIButton!
    
    var isInternet: (()->())?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setLanguage()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    fileprivate func setLanguage(){
        self.titleMsgNoInternet.text = getLanguageData(strKey: "titleMsgNoInternet")
        self.subTitleMsgNoInternet.text = getLanguageData(strKey: "subTitleMsgNoInternet")
        self.btnTryAgain.setTitle(self.getLanguageData(strKey: "btnTryAgain"), for: .normal)
    }
    
    @IBAction func onClickTryAgain(_ sender: UIButton){
//        if Reachability.isConnectedToNetwork(){
//            self.isInternet?()
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReachabilityManagerUpdate"), object: nil)
    }
    
    @objc func handleInternetActivity() {

        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)! {
            
            print("Internet Ok")
            self.isInternet?()
        } else {

            print("No Internet Available")
        }
    }

}

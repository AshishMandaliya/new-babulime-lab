//
//  API.swift
//  APISwipeDemo
//
//  Created by Mac on 04/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

typealias failureBlock = (String) -> Void
typealias successBlock = ([String:Any]) -> Void
class API {
    
    static let shared = API()
    
    func login(parameters: [String:Any],completion: @escaping ((dictionary? , String?) ->())){
        WebRequest.shared.request(endPoint: EndPoints.login,parameters: parameters) { (data,arr,err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                completion(data,err)
            }
        }
    }
    
    func forgotPassword(parameters: [String:Any],completion: @escaping ((Int?, String?) ->())){
        WebRequest.shared.request(endPoint: EndPoints.forgotPassword,parameters: parameters) { (data,arr,err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let otp = data["otp"] as? Int else { return }
                    completion(otp, nil)
                }else{
                    guard let msg = data["message"] as? String else { return }
                    completion(nil, msg)
                }
            }
        }
    }
    
    func varifyOTP(parameters: [String:Any],completion: @escaping ((dictionary?, String?) ->())){
        WebRequest.shared.request(endPoint: EndPoints.verifyOTP,parameters: parameters) { (data,arr,err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let data = data["data"] as? dictionary else { return }
                    completion(data, nil)
                }else{
                    guard let msg = data["message"] as? String else { return }
                    completion(nil, msg)
                }
            }
        }
    }
    
    func setNewPassword(parameters: [String:Any],completion: @escaping ((String) ->())){
        WebRequest.shared.request(endPoint: EndPoints.setNewPassword,parameters: parameters) { (data,arr,err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let msg = data["message"] as? String else { return }
                    completion(msg)
                }
            }
        }
    }
    
    func getIngredientList(parameters: [String: Any], completion: @escaping (([Ingredient]) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.getIngredientList, headers: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let ingredients = data["ingredients"] as? [dictionary] else { return }
                    let arrData = ingredients.map{Ingredient(data: $0)}
                    completion(arrData)
                }
            }
        }
    }
    
    func getChainExperiments(parameters: [String: Any], completion: @escaping (([dictionary]) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.getChainExp, headers: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let experiments = data["experiment"] as? [dictionary] else { return }
                    completion(experiments)
                }
            }
        }
    }
    
    func createExpBasic(parameters: [String: Any], completion: @escaping ((dictionary) ->())){
        WebRequest.shared.request(endPoint: EndPoints.createBasic, parameters: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let experiment = data["experiment"] as? dictionary else { return }
                    completion(experiment)
                }
            }
        }
    }
    
    func getIngredientDetail(parameters: [String: Any], completion: @escaping ((dictionary) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.getIngredientDetail, headers: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let ingredientDetail = data["ingredient"] as? dictionary else { return }
                    completion(ingredientDetail)
                }
            }
        }
    }
    
    func addIngredientInExp(parameters: [String: Any], completion: @escaping ((AddedIngredient) ->())){
        WebRequest.shared.request(endPoint: EndPoints.addIngredientInExp, parameters: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let ingredient = data["ingredient"] as? dictionary else { return }
                    completion(AddedIngredient(data: ingredient))
                }
            }
        }
    }
    
    
    func addIngredient(vc:UIViewController,param: dictionary, postImage: [UIImage]?, imageName: [String]?, imageParam: [String]?, failer:failureBlock? = nil, success:@escaping successBlock ){
        WebRequest.shared.requestsWithImages(url: "\(baseUrl)\(EndPoints.addIngredientInExp)", parameter: param, withPostImage: nil, withPostImageName: nil, withPostImageAry: postImage!, withPostImageNameAry: imageName!, withParamName: imageParam!) { (result) in
            let responce = self.checkResponce(vc: vc, result: result)
            if responce.isSuccess {
                success(responce.dic!)
            }
            else {
                failer?(responce.message!)
                appDelegate.showServerDown()
            }
        }
    }
    
    func sendImage(vc:UIViewController, param: dictionary, postImage: UIImage?, imageName:String?,imageArray:[UIImage],nameArray:[String], failer:failureBlock? = nil, success:@escaping successBlock ){
        WebRequest.shared.requestsSendImage(url: "\(baseUrl)\(EndPoints.addIngredientInExp)", parameter: param, withPostImage: nil, withPostImageName: nil, withPostImageAry: imageArray, withPostImageNameAry: nameArray, withParamName: "",  withParamNameAry:["certificate","product_detail_sheet"]) { (result) in
            let responce = self.checkResponce(vc: vc, result: result)
            if responce.isSuccess {
                success(responce.dic!)
            }
            else {
                failer?(responce.message!)
                appDelegate.showServerDown()
            }
        }
    }
    
    func getIngredientInExp(parameters: [String: Any], completion: @escaping (([dictionary]) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.getIngredientInExp, headers: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let ingredients = data["ingredients"] as? [dictionary] else { return }
                    completion(ingredients)
                }
            }
        }
    }
    
    func removeIngredientInExp(parameters: [String: Any], completion: @escaping ((String) ->())){
        WebRequest.shared.request(endPoint: EndPoints.removeIngredientInExp, parameters: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let message = data["message"] as? String else { return }
                    completion(message)
                }
            }
        }
    }
    
    func addProcessSteps(parameters: [String: Any], completion: @escaping ((FormulationStep) ->())){
        WebRequest.shared.request(endPoint: EndPoints.addProcessSteps, parameters: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let steps = data["process_of_formulation"] as? dictionary else { return }
                    completion(FormulationStep(data: steps))
                }
            }
        }
    }
    
    func getProcessSteps(parameters: [String: Any], completion: @escaping (([dictionary], [dictionary]) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.getProcessSteps, headers: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let steps = data["process_of_formulations"] as? [dictionary] else { return }
                    guard let ings = data["ingredients"] as? [dictionary] else { return }
                    completion(steps,ings)
                }
            }
        }
    }
    
    func removeProcessStep(parameters: [String: Any], completion: @escaping ((String) ->())){
        WebRequest.shared.request(endPoint: EndPoints.removeProcessSteps, parameters: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let message = data["message"] as? String else { return }
                    completion(message)
                }
            }
        }
    }
    
    func getParameters(parameters: [String: Any], completion: @escaping (([dictionary]) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.getParameters, headers: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let parameters = data["parameters"] as? [dictionary] else { return }
                    completion(parameters)
                }
            }
        }
    }
    
    func storeMethodAndResult(parameters: [String: Any], completion: @escaping ((String) ->())){
        WebRequest.shared.request(endPoint: EndPoints.storeMethodAndResult, parameters: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let message = data["message"] as? String else { return }
                    completion(message)
                }
            }
        }
    }
    
    func getMethodResult(parameters: [String: Any], completion: @escaping ((dictionary) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.getMethodResult, headers: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let exp = data["experiment"] as? dictionary else { return }
                    guard let mustFill = exp["must_fill"] as? dictionary else { return }
                    completion(mustFill)
                }
            }
        }
    }
    
    func removeMethodResult(parameters: [String: Any], completion: @escaping ((String) ->())){
        WebRequest.shared.request(endPoint: EndPoints.removeMethodResult, parameters: parameters) { (data, arr, err) in
            guard let data = data else { return }
            guard let status = data["success"] as? Bool else { return }
            if status == true {
                guard let message = data["message"] as? String else { return }
                completion(message)
            }
        }
    }
    
    func setReminder(parameters: [String: Any], completion: @escaping ((String) ->())){
        WebRequest.shared.request(endPoint: EndPoints.setReminder, parameters: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let message = data["message"] as? String else { return }
                    completion(message)
                }
            }
        }
    }
    
    func getReminder(parameters: [String: Any], completion: @escaping ((dictionary) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.getReminder, headers: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let exp = data["experiment"] as? dictionary else { return }
                    guard let reminder = exp["reminder"] as? dictionary else { return }
                    completion(reminder)
                }
            }
        }
    }
    
    func removeReminder(parameters: [String: Any], completion: @escaping ((String) ->())){
        WebRequest.shared.request(endPoint: EndPoints.removeReminder, parameters: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let message = data["message"] as? String else { return }
                    completion(message)
                }
            }
        }
    }
    
    func storeFinalResult(parameters: [String: Any], completion: @escaping ((String) ->())){
        WebRequest.shared.request(endPoint: EndPoints.storeFinalResult, parameters: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let message = data["message"] as? String else { return }
                    completion(message)
                }
            }
        }
    }
    
    func getFinalResult(parameters: [String: Any], completion: @escaping (([dictionary]) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.getFinalResult, headers: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let result = data["results"] as? [dictionary] else { return }
                    completion(result)
                }
            }
        }
    }
    
    func addNotes(vc:UIViewController,param: dictionary, postImage: [UIImage]?, imageName: [String]?, imageParam: [String]?, failer:failureBlock? = nil, success:@escaping successBlock ){
        WebRequest.shared.requestsWithImages(url: "\(baseUrl)\(EndPoints.addNotes)", parameter: param, withPostImage: nil, withPostImageName: nil, withPostImageAry: postImage!, withPostImageNameAry: imageName!, withParamName: imageParam!) { (result) in
            let responce = self.checkResponce(vc: vc, result: result)
            if responce.isSuccess {
                success(responce.dic!)
            }
            else {
                failer?(responce.message!)
                appDelegate.showServerDown()
            }
        }
    }
    
    
    func serverDown(){
        WebRequest.shared.requestGet(endPoint: EndPoints.serverDown) { (data, arr, err) in
            guard let err = err else {return}
            appDelegate.showServerDown()
        }
        
    }
    
    func serverCheckAPI(completion: @escaping ((String?, String?) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.serverCheck) { (data, arr, err) in
            if let err = err {
                completion(nil, err)
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let message = data["message"] as? String else { return }
                    completion(message, nil)
                }
            }
        }
    }
    
    
    func getFinalRegisterExperimentResult(parameters: [String: Any], completion: @escaping (([dictionary]) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.RegisterExperimentResult, headers: parameters) { (data, arr, err) in
            guard let data = data else { return }
            self.CheckForServerDown(data: data)
            guard let status = data["success"] as? Bool else { return }
            if status == true {
                guard let result = data["results"] as? [dictionary] else { return }
                completion(result)
            }
        }
    }
    
    
    func RequestForAction(parameters: [String: Any], completion: @escaping ((Bool) ->())){
        WebRequest.shared.request(endPoint: EndPoints.requestForAction, parameters: parameters) { (data, arr, err) in
            guard let data = data else { return }
            self.CheckForServerDown(data: data)
            guard let status = data["success"] as? Bool else { return }
            completion(status)
        }
    }
    
    func VerifyPassword(parameters: [String: Any], completion: @escaping ((Bool) ->())){
        WebRequest.shared.request(endPoint: EndPoints.verifyPassword, parameters: parameters) { (data, arr, err) in
            guard let data = data else { return }
            self.CheckForServerDown(data: data)
            guard let status = data["status"] as? Bool else { return }
            completion(status)
        }
    }
    
    
    func getExperimentListingAdmin(parameters: [String: Any], completion: @escaping (([dictionary], Int) ->())){
        WebRequest.shared.request(endPoint: EndPoints.getExperimentListing, parameters: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                guard let notificationCount = data["notification_count"] as? Int else { return }
                if status == true {
                    guard let experimentListing = data["experiments"] as? [dictionary] else { return }
                    completion(experimentListing, notificationCount)
                }
            }
        }
    }
    
    
    
    func CheckForServerDown(data: dictionary)
    {
        if data.isEmpty
        {
            UserDefaults.standard.set(true, forKey: "ServerDownAlert")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    
    
    
    func getExperimentListing(vc: UIViewController, parameters: [String: Any], completion: @escaping (([dictionary], Int) ->())){
        //        if Reachability.isConnectedToNetwork(){
        //            WebRequest.shared.requestGet(endPoint: EndPoints.getExperimentListing, headers: parameters) { (data, arr, err) in
        //                guard let data = data else { return }
        //                guard let status = data["success"] as? Bool else { return }
        //                if status == true {
        //                    guard let experimentListing = data["experiments"] as? [dictionary] else { return }
        //                    guard let notificationCount = data["notification_count"] as? Int else { return }
        //                    completion(experimentListing, notificationCount)
        //                }
        //            }
        //        }else {
        //            let vc1: NoInternetViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        //            vc1.isInternet = {
        //                vc.remove()
        //                vc.viewDidLoad()
        //            }
        //            vc.add(vc1, parent: vc.view)
        //        }
        
        WebRequest.shared.request(endPoint: EndPoints.getExperimentListing, parameters: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let experimentListing = data["experiments"] as? [dictionary] else { return }
                    guard let notificationCount = data["notification_count"] as? Int else { return }
                    completion(experimentListing, notificationCount)
                }
            }
        }
    }
    
    func signUp(vc:UIViewController,param: dictionary, postImage: UIImage?, imageName:String?, failer:failureBlock? = nil, success:@escaping successBlock ){
        WebRequest.shared.requestsWithImage(url: "\(baseUrl)\(EndPoints.storeFinalResult)", parameter: param, withPostImage: postImage, withPostImageName: imageName, withParamName: "singature") { (result) in
            let responce = self.checkResponce(vc: vc, result: result)
            if responce.isSuccess {
                success(responce.dic!)
            }
            else {
                failer?(responce.message!)
                appDelegate.showServerDown()
            }
        }
    }
    
    func getExperimentSample(parameters: [String: Any], completion: @escaping (([dictionary]) ->())){
        WebRequest.shared.request(endPoint: EndPoints.getExperimentSample, parameters: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let sampleExperiment = data["experiment_results"] as? [dictionary] else { return }
                    completion(sampleExperiment)
                }
            }
        }
    }
    func viewExperimentDetail(parameters: [String: Any], completion: @escaping ((dictionary) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.viewExperimentDetail, headers: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let experiment = data["experiment"] as? dictionary else { return }
                    completion(experiment)
                }
            }
        }
    }
    
    func getResultExp(parameters: [String: Any], completion: @escaping (([dictionary]) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.getResultExp, headers: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let experiments = data["final_results"] as? [dictionary] else { return }
                    completion(experiments)
                }
            }
        }
    }
    
    func getNotification(parameters: [String: Any], completion: @escaping (([dictionary]) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.getNotification, headers: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let experiments = data["notifications"] as? [dictionary] else { return }
                    completion(experiments)
                }
            }
        }
    }
    
    func getFilter(parameters: [String: Any], completion: @escaping (([dictionary]) ->())){
        WebRequest.shared.requestGet(endPoint: EndPoints.getFilter, headers: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                if status == true {
                    guard let experiments = data["filter"] as? [dictionary] else { return }
                    completion(experiments)
                }
            }
        }
    }
    
    func changePassword(parameters: [String: Any], completion: @escaping ((Bool) ->())){
        WebRequest.shared.request(endPoint: EndPoints.changePassword, parameters: parameters) { (data, arr, err) in
            if let err = err {
                print(err)
                appDelegate.showServerDown()
            }else{
                guard let data = data else { return }
                guard let status = data["success"] as? Bool else { return }
                //            if status == true {
                //                guard let message = data["message"] as? String else { return }
                //                completion(message)
                //            }
                completion(status)
            }
        }
    }
    
    func profileUpdate(vc:UIViewController,param: dictionary, postImage: UIImage?, imageName:String?, failer:failureBlock? = nil, success:@escaping successBlock ){
        WebRequest.shared.requestsWithImage(url: "\(baseUrl)\(EndPoints.profileUpdate)", parameter: param, withPostImage: postImage, withPostImageName: imageName, withParamName: "profile_image") { (result) in
            let responce = self.checkResponce(vc: vc, result: result)
            if responce.isSuccess {
                success(responce.dic!)
            }
            else {
                failer?(responce.message!)
                appDelegate.showServerDown()
            }
        }
    }
}


extension API{
    
    private func checkResponce(vc: UIViewController?, result: Result<Any>) -> (isSuccess:Bool, dic:dictionary? ,message:String?) {
        //        Modal.sharedAppdelegate.stoapLoader()
        switch result {
        case .success(let val):
            print("Response val: \(val)")
            if (val as! dictionary)[keys.status.rawValue] as? Bool ?? false{
                return (isSuccess: true, dic: val as? dictionary, message: nil)
            }else{
                guard let message = (val as! dictionary)[keys.message.rawValue] as? String else { //server side respose false
                    print("Status is false but can't get error message")
                    return (isSuccess: false, dic: nil, message: nil)
                }
                if let vc = vc{
                    vc.alert(title: "", message: message)
                }
                return (isSuccess: false, dic: nil, message: message)
            }
        case .failure(let error):
            let strErr = error.localizedDescription
            if strErr == "The Internet connection appears to be offline." {//strErr == "Could not connect to the server." ||
                if let vc = vc{
                    vc.alert(title: "", message: strErr, actions: ["Cancel","Settings"], completion: { (flag) in
                        if flag == 1{ //Settings
                            vc.open(scheme:UIApplication.openSettingsURLString)
                        }
                        else{ //== 0 Cancel
                        }
                    })
                }
            }
            return (isSuccess: false, dic: nil, message: strErr)
        }
    }
    
}
extension UIViewController {
    
    //MARK: - Aleart Class
    
    //https://useyourloaf.com/blog/openurl-deprecated-in-ios10/
    
    func open(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            print("Open \(scheme): \(success)")
                                          })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(scheme): \(success)")
            }
        }
    }
    func open(url: URL) {
        if UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                let options = [UIApplication.OpenExternalURLOptionsKey.universalLinksOnly : true]
                UIApplication.shared.open(url, options: options, completionHandler: nil)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(url)
            }
        }
        else{
            print("Can't open given URL")
        }
    }
}

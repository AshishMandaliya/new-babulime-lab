//
//  Constant.swift
//  US Citizenship Quiz 2020
//
//  Created by Gopi Donga on 21/07/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

typealias dictionary = [String : Any]
typealias dictionaryforrequest = [String : Int]

let appDelegate = UIApplication.shared.delegate as! AppDelegate

var baseUrl = "https://babulimelab.9brainz.store/"
var SelectedLanguage = UserData.shared.language
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> origin/master

class AuthenticationManager {
  static let sharedInstance = AuthenticationManager()
  var needsAuthentication = false
}
<<<<<<< HEAD
=======
=======
>>>>>>> 2c50902ba94781086bc63e41aa40a822616dc54c
>>>>>>> origin/master

struct EndPoints {
    static let login = "api/v1/login"
    static let createBasic = "api/v1/create-experiment"
    static let getChainExp = "api/v1/get-experiments"
    static let getIngredientList = "api/v1/get-ingredients"
    static let getIngredientDetail = "api/v1/get-ingredient-details"
    static let addIngredientInExp = "api/v1/create-ingredient"
    static let getIngredientInExp = "api/v1/get-experiment-ingredients"
    static let removeIngredientInExp = "api/v1/remove-ingredient"
    static let addProcessSteps = "api/v1/create-process-of-formulation"
    static let getProcessSteps = "api/v1/get-process-of-formulation"
    static let removeProcessSteps = "api/v1/remove-process-of-formulation"
    static let getParameters = "api/v1/get-expected-results-parameters"
    static let storeMethodAndResult = "api/v1/store-method-and-result"
    static let getMethodResult = "api/v1/get-method-result-details"
    static let removeMethodResult = "api/v1/remove-must-fill"
    static let setReminder = "api/v1/set-reminder"
    static let getReminder = "api/v1/get-reminder-view-details"
    static let removeReminder = "api/v1/remove-reminder"
    static let storeFinalResult = "api/v1/store-final-result"
    static let getFinalResult = "api/v1/get-final-results"
    static let getExperimentListing = "api/v1/experiment-listing"
    static let getExperimentSample = "api/v1/experiment-samples"
    static let getResultExp = "api/v1/registered-experiment-results"
    static let getNotification = "api/v1/notifications"
    static let getFilter = "api/v1/filter-list"
    static let verifyPassword = "api/v1/verify-password-for-view"
    static let RegisterExperimentResult = "api/v1/registered-experiment-results"
    static let requestForAction = "api/v1/request-for-action"
    static let serverDown = "api/v1/server-down"
    static let serverCheck = "api/v1/server-ping"
    static let changePassword = "api/v1/change-password"
    static let profileUpdate = "api/v1/profile-update"
    static let forgotPassword = "api/v1/forgot-password"
    static let verifyOTP = "api/v1/otp-verify"
    static let setNewPassword = "api/v1/set-new-password"
    static let addNotes = "api/v1/add-notes"
    static let getNotes = "api/v1/get-notes"
    
    //to view detail of an experiment
    static let viewExperimentDetail = "api/v1/experiment-view-details"
    
}

extension UIColor{
    static let mainColor = UIColor(hex: "#633f97ff")
    static let secondaryColor = UIColor(hex: "#ffffffff")
    static let otherColor = UIColor(hex: "#353739ff")
    static let copyrightColor = UIColor(hex: "#5d5d5dff")
    static let urgentColor = UIColor(hex: "#e02020ff")
    static let normalColor = UIColor(hex: "#ef811bff")
}

extension UIFont{
    static let titleFont = UIFont(name: "SFProDisplay-Bold", size: 32)
    static let medium22 = UIFont(name: "SFProDisplay-Medium", size: 22)
    static let medium18 = UIFont(name: "SFProDisplay-Medium", size: 18)
    static let medium17 = UIFont(name: "SFProDisplay-Medium", size: 17)
    static let semi20 = UIFont(name: "SFProDisplay-Semibold", size: 20)
    static let regular14 = UIFont(name: "SFProText-Regular", size: 14)
    static let regular16 = UIFont(name: "SFProText-Regular", size: 16)
    static let regular20 = UIFont(name: "SFProText-Regular", size: 20)
    static let medium15 = UIFont(name: "SFProText-Medium", size: 15)

}


public enum Result<T> {
    case success(T)
    case failure(Error)
}

//MARK: - Api Structure Key

enum keys:String  {
    case status = "success"
    case message = "message"
    case data = "tabs_verify"
}
func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
    
    let options = prettyPrinted ?
        JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
    // JSONSerialization.WritingOptionsJSONSerialization.WritingOptions.PrettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
    
    
    if JSONSerialization.isValidJSONObject(value) {
        
        do{
            let data = try JSONSerialization.data(withJSONObject: value, options: options)
            if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                return string as String
            }
        }catch {
            
            print("error")
            //Access error here
        }
        
    }
    return ""
    
}
func json(from object:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}
func createThumbnailOfVideoFromRemoteUrl(url: String) -> String? {
    let videoid = getYoutubeId(youtubeUrl: url)
    return "https://img.youtube.com/vi/" + videoid! + "/default.jpg"
//    let asset = AVAsset(url: URL(string: videoUrl)!)
//    let assetImgGenerate = AVAssetImageGenerator(asset: asset)
//    assetImgGenerate.appliesPreferredTrackTransform = true
    //Can set this to improve performance if target size is known before hand
    //assetImgGenerate.maximumSize = CGSize(width,height)
//    let time = CMTimeMakeWithSeconds(1.0, 600)
//    do {
//        let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
//        let thumbnail = UIImage(cgImage: img)
//        return thumbnail
//    } catch {
//        print(error.localizedDescription)
//        return nil
//    }
}
func getYoutubeId(youtubeUrl: String) -> String?
{
    if URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value == nil
    {
        return ""
    }
    else
    {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
    }
}
struct ResponseKey{
    
    static func fatchDataAsDictionary(res: dictionary, valueOf key: keys) -> [String:Any] {
        if res[key.rawValue] as? [String:Any] != nil{
            print("Dictionary as a response")
            return res[key.rawValue] as! [String:Any]
        }
        else{
            if res[key.rawValue] as? [Any] != nil{
                print("Response is different: Array as a response")
                return [:]
            }
            else if res[key.rawValue] as? String != nil{
                print("Response is different: String as a response")
                return [:]
            }else{
                print("Response is different: Response type not a Dictionary nore Array or String")
                return [:]
            }
        }
    }
    
    static func fatchDataAsArray(res: dictionary, valueOf key: keys) -> [Any] {
        if res[key.rawValue] as? [Any] != nil{
            print("Array as a response")
            return res[key.rawValue] as! [Any]
        }
        else{
            if res[key.rawValue] as? [String:Any] != nil{
                print("Response is different: Dictionary as a response")
                return []
            }
            else if res[key.rawValue] as? String != nil{
                print("Response is different: String as a response")
                return []
            }else{
                print("Response is different: Response type not a Dictionary nore Array or String")
                return []
            }
        }
    }
    
    static func fatchDataAsString(res: dictionary, valueOf key: keys) -> String {
        if res[key.rawValue] as? String != nil{
            print("String as a response")
            return res[key.rawValue] as! String
        }
        else{
            if res[key.rawValue] as? [Any] != nil{
                print("Response is different: Array as a response")
                return ""
            }
            else if res[key.rawValue] as? [String:Any] != nil{
                print("Response is different: Dictionary as a response")
                return ""
            }else{
                print("Response is different: Response type not a Dictionary nore Array or String")
                return ""
            }
        }
    }
    
    static func fatchData(res: dictionary, valueOf key: keys) -> (dic: [String:Any], ary: [Any], str: String){
        if res[key.rawValue] as? [String:Any] != nil{
            print("Dictionary as a response")
            return (dic: res[key.rawValue] as! [String:Any], ary: [], str: "")
        }
        else if res[key.rawValue] as? [Any] != nil{
            print("Array as a response")
            return (dic: [:], ary: res[key.rawValue] as! [Any], str: "")
        }
        else if res[key.rawValue] as? String != nil{
            print("String as a response")
            return (dic: [:], ary: [], str: res[key.rawValue] as! String)
        }else{
            print("Response type not a Dictionary nore Array or String")
            return (dic: [:], ary:[], str: "")
        }
    }
}

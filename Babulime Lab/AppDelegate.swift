//
//  AppDelegate.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 05/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
//        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared.enable = true
        
        
        Utility.startInternetMonitoring()

        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if launchedBefore  {
            print("Not first launch.")
            UserDefaults.standard.set("", forKey: "TokenValue")
            let k = UserDefaults.standard.value(forKey: "ServerDownAlert")
            UserDefaults.standard.set(k, forKey: "ServerDownAlert")
            UserDefaults.standard.synchronize()
        } else {
            print("First launch, setting UserDefault.")
            Utility.setUserDefaults(withObject: true as AnyObject, forKey: "isInternetConnected")
            let TV = UserDefaults.standard.value(forKey: "TokenValue")
            UserDefaults.standard.set(TV, forKey: "TokenValue")
            UserDefaults.standard.set(false, forKey: "ServerDownAlert")
            Utility.setUserDefaults(withObject: true as AnyObject, forKey: "isInternetConnectionAvailable")
            UserDefaults.standard.set(true, forKey: "launchedBefore")
            UserData.shared.setLanguage(language: "en")
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> origin/master
            UserData.shared.setTouchId(enable: false)
        }
        
        
        if UserData.shared.getUser().isLogin{
<<<<<<< HEAD
=======
=======
            UserDefaults.standard.synchronize()
        }
        
        if UserData.shared.getUser().isLogin
        {
>>>>>>> 2c50902ba94781086bc63e41aa40a822616dc54c
>>>>>>> origin/master
            if UserData.shared.getUser().userInfo?.is_admin == 1
            {
                rootToAdminHome()
            }
            else
            {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> origin/master
                if UserData.shared.getTouchId(){
                    touchIdClick()
                    
                }else{
                    rootToHome()
                }
                rootToHome()
                
            }
        }else{
            rootToLogin()
        }
        
        
<<<<<<< HEAD
=======
=======
                rootToHome()
            }
            
        }
        else
        {
            rootToLogin()
        }

>>>>>>> 2c50902ba94781086bc63e41aa40a822616dc54c
>>>>>>> origin/master
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        Utility.stopInternetMonitoring()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        if UserData.shared.getTouchId(){
            self.touchIdClick(completion: nil)
        }
        Utility.startInternetMonitoring()
    }
    
    func showServerDown(){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc: ServerDownViewController = storyboard.instantiateVC()!
        self.window?.addSubview(vc.view)
    }

    func rootToAdminHome(){
        let storyboard = UIStoryboard(name: "Admin", bundle: nil)
        let vc: AdminSplitsViewController = storyboard.instantiateVC()!
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
    }

    func rootToLogin(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: ViewController = storyboard.instantiateVC()!
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isHidden = true
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
    }
    func rootToHome(){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc: SplitsViewController = storyboard.instantiateVC()!
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
    }
    
    func touchIdClick(completion: (() -> Void)? = nil){
         AuthenticationManager.sharedInstance.needsAuthentication = true
    }
    
   
}

extension UIApplication {
var statusBarUIView: UIView? {
    if #available(iOS 13.0, *) {
        let tag = 38482
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if let statusBar = keyWindow?.viewWithTag(tag) {
            return statusBar
        } else {
            guard let statusBarFrame = keyWindow?.windowScene?.statusBarManager?.statusBarFrame else { return nil }
            let statusBarView = UIView(frame: statusBarFrame)
            statusBarView.tag = tag
            keyWindow?.addSubview(statusBarView)
            return statusBarView
        }
    } else if responds(to: Selector(("statusBar"))) {
        return value(forKey: "statusBar") as? UIView
    } else {
        return nil
    }
  }
}



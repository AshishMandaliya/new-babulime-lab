//
//  AdminMenuTableViewController.swift
//  Babulime Lab
//
//  Created by Ashish Mandaliya on 02/09/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class AdminMenuTableViewController: UITableViewController {
    
    var menuName = ["Home"]
    var menuIcons = ["home"]
    var selectedIndex = 0
    var userInfo:User{
        return UserData.shared.getUser().userInfo ?? User(data: [:])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.rowHeight = 70
        
        self.addViewUnderTheTableView()
        tableView.tableFooterView = UIView()
    }
    
    func addViewUnderTheTableView() {
            let vw: UIView = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height - 100.0, width: tableView.frame.size.width, height: 100.0))
            vw.backgroundColor = .clear
            
            let iv: UIImageView = UIImageView(frame: CGRect(x: 20.0, y: 20.0, width: 60.0, height: 60.0))
            iv.contentMode = .scaleAspectFit
            iv.layer.cornerRadius = 30.0
        
            if let data = try? Data(contentsOf: URL(string: userInfo.profile_image) ?? URL(fileURLWithPath: "")){
                iv.image = UIImage(data: data)
            }
            iv.clipsToBounds = true
            vw.addSubview(iv)
            
            let lbl: UILabel = UILabel(frame: CGRect(x: 90.0, y: 20.0, width: (tableView.frame.size.width - 120.0), height: 60.0))
            lbl.numberOfLines = 2
            lbl.lineBreakMode = .byWordWrapping
            lbl.textColor = UIColor.black
            lbl.font = UIFont.medium22
            
            lbl.text = userInfo.name
            vw.addSubview(lbl)
            
            vw.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClickProfile)))
            
            navigationController?.view.addSubview(vw)
        }
    
    @objc func onClickProfile(_ gesture: UITapGestureRecognizer){
            let vc: ProfileViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        
            self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: vc), sender: self)
        }
    
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return menuName.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return menuName.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MenuTableViewCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        
        if selectedIndex == indexPath.row {
            cell.backView.backgroundColor = UIColor.mainColor
            cell.imgViewMenu.tintColor = UIColor.secondaryColor
            cell.lblMenu.textColor = UIColor.secondaryColor
        }else {
            cell.backView.backgroundColor = UIColor.clear
            cell.imgViewMenu.tintColor = UIColor.black
            cell.lblMenu.textColor = UIColor.black
        }
        
        // Configure the cell...
        cell.imgViewMenu.image = UIImage(named: menuIcons[indexPath.row])
        cell.lblMenu.text = menuName[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            selectedIndex = indexPath.row
            switch selectedIndex {
            case 0:
                    
                appdelegate.rootToAdminHome()
            
            default:
             
                appdelegate.rootToAdminHome()
                
            }
            
            tableView.reloadData()
        }
  
}

//
//  AdminHomeViewController.swift
//  Babulime Lab
//
//  Created by Ashish Mandaliya on 02/09/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class RegisterExperimentResult
{
    let id: Int
    let experiment_id: Int
    let name: String
    let approval_admin_name: String
    let date: String
    let description: String
    let Parameter: [dictionary]
    
    init(data: dictionary)
    {
        self.id = data["id"] as? Int ?? -1
        self.experiment_id = data["experiment_id"] as? Int ?? -1
        self.name = data["name"] as? String ?? ""
        self.approval_admin_name = data["approval_admin_name"] as? String ?? ""
        self.date = data["date"] as? String ?? ""
        self.description = data["description"] as? String ?? ""
        self.Parameter = data["data"] as? [dictionary] ?? []
    }
}

class AdminHomeViewController: BaseViewController,UISearchBarDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var vwInternetLose: UIView!
    @IBOutlet weak var cvParameter: UICollectionView!
    @IBOutlet weak var btnTryAginNoInternet: UIButton!
    @IBOutlet weak var lblTotalCount: UILabel!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var btnCreateExperiment: UIButton!
    @IBOutlet weak var btnNotification: UIBarButtonItem!
    @IBOutlet weak var redDotFilter: UIView!
    @IBOutlet weak var lblMainHeadingNotification: UILabel!
    @IBOutlet weak var lblMainHeadingFliter: UILabel!
    @IBOutlet weak var lblFinalResultDescription: UILabel!
    @IBOutlet weak var lblFinalResultDate: UILabel!
    @IBOutlet weak var btnFlask: UIBarButtonItem!
    @IBOutlet weak var vwNotification: UIView!
    @IBOutlet weak var btnApplyForFilter: UIButton!{
        didSet{
            self.btnApplyForFilter.setTitleColor(UIColor(hex: "#0091ff80"), for: .disabled)
            self.btnApplyForFilter.setTitleColor(UIColor(hex: "#0091ffff"), for: .normal)
            self.btnApplyForFilter.isEnabled = true
        }
    }
    @IBOutlet weak var btnCancelForFilter: UIButton!
    @IBOutlet weak var tblViewNotification: UITableView!
    @IBOutlet weak var tblViewExperince: UITableView!
    @IBOutlet weak var txtFilter: UITextField!{
        didSet{
            self.txtFilter.addTarget(self, action: #selector(txt), for: .editingChanged)
            self.txtFilter.delegate = self
        }
    }
    @IBOutlet weak var tblViewFliterCategory: UITableView!
    @IBOutlet weak var tblViewDetailFilter: UITableView!
    @IBOutlet weak var btnClearFilter: UIButton!
    @IBOutlet weak var vwFliter: UIView!
    @IBOutlet weak var lblUsers: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblScreenHeading: UILabel!
    @IBOutlet weak var stVWScreen12: UIStackView!
    @IBOutlet weak var txtRecipeName: UITextField!
    @IBOutlet weak var tblViewScreen12: UITableView!
    @IBOutlet weak var vwResultSegment: UIView!
    @IBOutlet weak var lblResultRegisterDate: UILabel!
    @IBOutlet weak var searchBarFilter: UISearchBar!{
        didSet{
            self.searchBarFilter.delegate = self
        }
    }
    @IBOutlet weak var lblResultRecipeName: UILabel!
    @IBOutlet weak var lblResultParameter: UILabel!
    @IBOutlet weak var btnToggleScreen: UIButton!
    @IBOutlet weak var lblResultDescription: UILabel!
    @IBOutlet weak var vwRecipeNamePickerView: UIView!
    @IBOutlet weak var segmentControler: UISegmentedControl!{
        didSet{
            self.segmentControler.addTarget(self, action: #selector(valueChangeSegment), for: .valueChanged)
        }
    }
    @IBOutlet weak var vwServerDown: UIView!
    @IBOutlet weak var vwCreateExperiment: UIView!
    @IBOutlet weak var vwNoResultFilter: UIView!
    
    var filterSelectedIndex = 0{
        didSet{
            self.searchBarFilter.text = ""
        }
    }
    var arrIngFilter: [String] = []{
        didSet{
            checkApplyButtonValidation()
        }
    }
    var arrContactPerson: [String] = []{
        didSet{
            checkApplyButtonValidation()
        }
    }
    var arrCompanyName: [String] = []{
        didSet{
            checkApplyButtonValidation()
        }
    }
    var arrImportance: [String] = []{
        didSet{
            checkApplyButtonValidation()
        }
    }
    var arrResult: [String] = []{
        didSet{
            checkApplyButtonValidation()
        }
    }
    var filterPropertyPurity = ""{
        didSet{
            checkApplyButtonValidation()
        }
    }
    var filterDateTime = ""{
        didSet{
            checkApplyButtonValidation()
        }
    }
    var isHiddenMenu = false
    var dataArray = [ExperimentListing]()
    var arrHeadingPickerView = ["The Limewater carbon dioxide test","Limewater Breath Experiment","Combustion experiment with limewater"]
    let pickarViewForRecipeName = UIPickerView()
    var isModify : Bool = false
    var FinalResultArray = [RegisterExperimentResult]()
    var FilterArray = [dictionary]()
    var detail = [String]()
    var arrNotification = [dictionary]()
    var isNotificationOpen = false
    var isFilterApply = false
    var TotalNotificationCount = 0
    let dateTimePicker = UIDatePicker()
    var PickerViewIndex = 0
    var OTPTimer = Timer()
    var FinalTimeForOTPEnter = 20
    var OTPTimeCounter = 10
    var TotalAdminEnterOTP = 3
    var RemainingAdminEnterOTP = 1
    let lblTimeRemaining = UILabel(frame: CGRect(x: 20, y: 203, width: 240, height: 18))
    let lblOTP = UILabel(frame: CGRect(x: 20, y: 174, width: 120, height: 18))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name(rawValue: "ReachabilityManagerUpdate"), object: nil)
        
        pickarViewForRecipeName.dataSource = self
        pickarViewForRecipeName.delegate = self
        
        txtRecipeName.font = UIFont(name: "SFProText-Regular", size: 17)
        
        vwInternetLose.alpha = 0
        vwServerDown.alpha = 0
        vwCreateExperiment.alpha = 0
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        RecipeName()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneClick))
        
        toolBar.setItems([doneButton], animated: false)
        
        txtRecipeName.inputAccessoryView = toolBar
        
        let tapGestureForRecipeName = UITapGestureRecognizer(target: self, action: #selector(OpenPickerView))
        
        vwRecipeNamePickerView.addGestureRecognizer(tapGestureForRecipeName)
        
        
        self.vwNotification.alpha = 0
        self.vwFliter.alpha = 0
        
        stVWScreen12.isHidden = false
        tblViewScreen12.isHidden = false
        vwResultSegment.isHidden = true
        
        tblViewFliterCategory.contentInset = UIEdgeInsets(top: -25, left: 0, bottom: 0, right: 0)
        
        self.isNotificationOpen = false
        self.btnNotification(self.btnNotification as UIBarButtonItem)
        self.vwFliter.slideOutCustom()
        self.vwNoResultFilter.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        lblScreenHeading.text = "Experiments".localizableString(loc: SelectedLanguage)
        btnCreateExperiment.setTitle("CREATE".localizableString(loc: SelectedLanguage), for: .normal)
        
        getData()
        languageChanged(strLan: SelectedLanguage)
        self.txtRecipeName.text = ""
        
        if isFilterApply
        {
            self.redDotFilter.isHidden = false
        }
        
        else
        {
            self.redDotFilter.isHidden = true
        }
        
        self.title = "Home".localizableString(loc: SelectedLanguage)
        
        let cellWidth : CGFloat = 100
        let cellheight : CGFloat = 55
        let cellSize = CGSize(width: cellWidth , height:cellheight)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.minimumLineSpacing = 10.0
        layout.minimumInteritemSpacing = 10.0
        self.cvParameter.setCollectionViewLayout(layout, animated: true)
        self.cvParameter.reloadData()
        
        OTPTimeCounter = FinalTimeForOTPEnter
//        alertFinalForOTP(WhichTitle: "Modify Recipe")
//        StartOTPTimer()
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        } 
        
    }
    
    @objc func changeTime()
    {
        OTPTimeCounter -= 1
        print("\(OTPTimeCounter)")
        
        if OTPTimeCounter != 0
        {
            lblTimeRemaining.text = "Time Remaining: \(OTPTimeCounter)seconds"
        }
        
        else
        {
            StopOTPTimer()
            dismiss(animated: true, completion: nil)
            alert(title: "Session Timeout", message: " Your session times out after you’ve been inactive for a while. Kindly resubmit request.")
        }
        
    }
    
    func StartOTPTimer()
    {
        OTPTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(changeTime), userInfo: nil, repeats: true)
    }
    
    func StopOTPTimer()
    {
        OTPTimer.invalidate()
    }
    
    @objc func onClickDate(_ sender: UIDatePicker){
        self.filterDateTime = formatDateForDisplay(date: sender.date, format: "yyyy-MM-dd hh:mm:ss")
        self.txtFilter.text = self.filterDateTime
        self.view.endEditing(true)
    }
    
    fileprivate func formatDateForDisplay(date: Date, format: String) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar == self.searchBarFilter{
            
            detail = searchText.isEmpty ? self.FilterArray[filterSelectedIndex]["detail"] as? [String] ?? [] : detail.filter({(dataString: String) -> Bool in
                return dataString.range(of: searchText, options: .caseInsensitive) != nil
            })
            
            self.tblViewDetailFilter.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if searchBar == self.searchBarFilter{
            detail = self.FilterArray[filterSelectedIndex]["detail"] as? [String] ?? []
            self.tblViewDetailFilter.reloadData()
        }
    }
    
    @objc func handleInternetActivity()
    {
        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)!
        {
            getData()
            print("Internet Ok")
            self.vwInternetLose.alpha = 0
            
        } else {
            print("Not Ok")
            UIView.animate(withDuration: 0.2) {
                self.vwInternetLose.alpha = 1
            }
        }
    }
    
    @objc func txt(){
        let filterCategoryName = self.FilterArray[filterSelectedIndex]["name"] as? String ?? ""
        if filterCategoryName == "Property/Purity"{
            self.filterPropertyPurity = self.txtFilter.text ?? ""
        }
    }
    
    func checkApplyButtonValidation(){
        self.btnApplyForFilter.isEnabled = self.arrIngFilter.isEmpty == false || self.arrContactPerson.isEmpty == false || self.self.arrCompanyName.isEmpty == false || self.arrImportance.isEmpty == false || self.arrResult.isEmpty == false || self.filterDateTime.isEmpty == false || self.filterPropertyPurity.isEmpty == false
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField == self.txtFilter{
            filterDateTime = ""
        }
        return true
    }
    
    func getData(){
        let dic: dictionary = [
            "token": userInfo.access_token,
            "listing": segmentControler.titleForSegment(at: segmentControler.selectedSegmentIndex)!.lowercased(),
            "admin_id": userInfo.user_id,
            "ingredients": arrIngFilter.joined(separator: ","),
            "contact_persons": arrContactPerson.joined(separator: ","),
            "company_names": arrCompanyName.joined(separator: ","),
            "importances": arrImportance.joined(separator: ","),
            "results": arrResult.joined(separator: ","),
            "property_purities": filterPropertyPurity,
            "filter_date_time": filterDateTime,
        ]
        
        API.shared.getExperimentListingAdmin(parameters: dic) { [self] (listing, notiCount) in
            self.stopLoader()
            self.dataArray.removeAll()
            self.TotalNotificationCount = notiCount
            listing.forEach { (data) in
                self.dataArray.append(ExperimentListing(data: data))
            }
            
            if self.dataArray.count == 0
            {
                if self.isFilterApply
                {
                    self.vwNoResultFilter.alpha = 1
                }
                else
                {
                    self.vwCreateExperiment.alpha = 0
                }
                
                self.stVWScreen12.alpha = 0
                self.btnCreateExperiment.alpha = 0
            }
            
            else
            {
                self.vwCreateExperiment.alpha = 0
                self.stVWScreen12.alpha = 1
                self.btnCreateExperiment.alpha = 1
            }
            
            self.lblTotalCount.text = "\(self.dataArray.count)"
            if self.TotalNotificationCount != 0
            {
                self.btnNotification.addBadge(number: self.TotalNotificationCount, withOffset: CGPoint(x: 10, y: 0), andColor: UIColor.mainColor!, andFilled: true)
            }
            
            self.tblViewExperince.reloadData()
        }
        
    }
    
    @IBAction func btnStartCreatingExperimentForView(_ sender: Any)
    {
        let vc: CreateEx1ViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getNotification(){
        let dic: dictionary = [
            "user_id" : self.userId,
            "token": self.userInfo.access_token
        ]
        
        API.shared.getNotification(parameters: dic) { (arr) in
            self.arrNotification.removeAll()
            self.arrNotification = arr
            self.tblViewNotification.reloadData()
        }
    }
    
    func getFilter(){
        let dic: dictionary = [
            "user_id" : self.userId,
            "token": self.userInfo.access_token
        ]
        
        API.shared.getFilter(parameters: dic) { (arr) in
            self.FilterArray.removeAll()
            self.FilterArray = arr
            self.detail = self.FilterArray[self.filterSelectedIndex]["detail"] as? [String] ?? []
            self.tblViewFliterCategory.reloadData()
            self.tblViewDetailFilter.reloadData()
            
        }
        
    }
    
    func getFinalResultData()
    {
        let dic : dictionary = [
            "token" : self.userInfo.access_token,
            "admin_id" : self.userId
        ]
        
        API.shared.getResultExp(parameters: dic) { (listing) in
            self.FinalResultArray.removeAll()
            listing.forEach { (data) in
                self.FinalResultArray.append(RegisterExperimentResult(data: data))
            }
            self.txtRecipeName.text = self.FinalResultArray[0].name
            self.lblFinalResultDate.text = self.FinalResultArray[0].date
            self.lblFinalResultDescription.text = self.FinalResultArray[0].description
            self.cvParameter.reloadData()
        }
    }
    
    func RequestSendForAction(ExperimentID: Int, WhichAction: String, Reason: String? = "")
    {
        let dic: dictionary = [
            "token": self.userInfo.access_token,
            "admin_id":self.userId,
            "experiment_id":ExperimentID,
            "action":WhichAction,
            "reason_of_view":Reason ?? ""
        ]
        
        API.shared.RequestForAction(parameters: dic) { (k) in
            if k
            {
                print("Success")
                self.alertRegisterSuccessForModifyRecipe()
                self.getData()
                self.tblViewExperince.reloadData()
            }
            
            else
            {
                print("failed")
            }
        }
    }
    
    @objc func doneClick()
    {
        view.endEditing(true)
    }
    
    func RecipeName()
    {
        pickarViewForRecipeName.delegate = self
        txtRecipeName.inputView = pickarViewForRecipeName
    }
    
    @objc func OpenPickerView()
    {
        pickarViewForRecipeName.isHidden = false
    }
    
    @objc func valueChangeSegment(_ sender: UISegmentedControl)
    {
        
        switch sender.selectedSegmentIndex{
        case 0 :
            getData()
            doneClick()
            btnCreateExperiment.isHidden = false
            lblScreenHeading.text = "\("Experiments".localizableString(loc: SelectedLanguage))"
            stVWScreen12.isHidden = false
            tblViewScreen12.isHidden = false
            vwResultSegment.isHidden = true
            
        case 1 :
            getData()
            doneClick()
            btnCreateExperiment.isHidden = false
            lblScreenHeading.text = "\("Experiments".localizableString(loc: SelectedLanguage))"
            stVWScreen12.isHidden = false
            tblViewScreen12.isHidden = false
            vwResultSegment.isHidden = true
            
        case 2 :
            lblScreenHeading.text = "\("Experiments".localizableString(loc: SelectedLanguage))"
            getFinalResultData()
            doneClick()
            btnCreateExperiment.isHidden = true
            stVWScreen12.isHidden = true
            tblViewScreen12.isHidden = true
            vwResultSegment.isHidden = false
            
        case 3 :
            getData()
            doneClick()
            btnCreateExperiment.isHidden = true
            lblScreenHeading.text = "\("Experiments".localizableString(loc: SelectedLanguage))"
            stVWScreen12.isHidden = false
            tblViewScreen12.isHidden = false
            vwResultSegment.isHidden = true
            
        default:
            return
        }
    }
    
    @IBAction func btnTryAginNoInternet(_ sender: Any)
    {
        
    }
    
    @IBAction func btnCreateExperiment(_ sender: Any)
    {
        let vc: CreateEx1ViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func OpenFliter(_ sender: Any)
    {
        self.vwFliter.alpha = 1
        self.vwFliter.slideInCustom()
        getFilter()
        checkApplyButtonValidation()
    }
    
    @IBAction func btnAdminNotification(_ sender: Any)
    {
        if isNotificationOpen{
            self.vwNotification.slideInCustom()
            self.getNotification()
        }
        else{
            self.vwNotification.slideOutCustom()
        }
        self.isNotificationOpen.toggle()
    }
    
    @IBAction func btnNotification(_ sender: Any)
    {
        self.vwNotification.alpha = 1
        
        if isNotificationOpen{
            self.vwNotification.slideInCustom()
            self.getNotification()
        }
        else{
            self.vwNotification.slideOutCustom()
        }
        self.isNotificationOpen.toggle()
        
    }
    
    @IBAction func btnCloseFliter()
    {
        self.vwFliter.slideOutCustom()
        checkApplyButtonValidation()
        
        if !btnApplyForFilter.isEnabled
        {
            self.isFilterApply = false
            self.redDotFilter.isHidden = true
            self.getData()
        }
        
    }
    
    @IBAction func btnApply(_ sender: Any)
    {
        self.isFilterApply = true
        self.redDotFilter.isHidden = false
        print("\(isFilterApply)")
        getData()
        self.btnCloseFliter()
    }
    
    @IBAction func btnClearFilter(_ sender: Any)
    {
        self.isFilterApply = false
        self.arrIngFilter = []
        self.arrContactPerson = []
        self.arrCompanyName = []
        self.arrImportance = []
        self.arrResult = []
        self.filterDateTime = ""
        self.filterPropertyPurity = ""
        self.redDotFilter.isHidden = true
        self.btnCloseFliter()
        
    }
    @IBAction func btnToggleScreen(_ sender: Any)
    {
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    @IBAction func btnToggle(_ sender: Any)
    {
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.splitViewController?.preferredDisplayMode = self.isHiddenMenu ? .allVisible : .primaryHidden
            self.isHiddenMenu.toggle()
        }
    }
    
    func languageChanged(strLan: String)
    {
        self.lblScreenHeading.text = "Experiments".localizableString(loc: strLan)
        self.lblUsers.text = "User".localizableString(loc: strLan)
        self.lblTotal.text = "Total".localizableString(loc: strLan)
        self.lblResultRecipeName.text = "Recipe Name".localizableString(loc: strLan)
        self.lblResultRegisterDate.text = "Registered Date".localizableString(loc: strLan)
        self.lblResultParameter.text = "Parameters".localizableString(loc: strLan)
        self.lblResultDescription.text = "Description".localizableString(loc: strLan)
        self.lblMainHeadingNotification.text = "Notifications".localizableString(loc: strLan)
        self.lblMainHeadingFliter.text = "Fliters".localizableString(loc: strLan)
        self.btnApplyForFilter.setTitle("Apply".localizableString(loc: strLan), for: .normal)
        self.btnCancelForFilter.setTitle("Cancel".localizableString(loc: strLan), for: .normal)
    }
}

extension AdminHomeViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView == tblViewExperince
        {
            return 1
        }
        
        else if tableView == tblViewFliterCategory
        {
            return 1
        }
        
        else if tableView == tblViewDetailFilter
        {
            return 1
        }
        
        else if tableView == tblViewNotification
        {
            return self.arrNotification.count
        }
        
        else
        {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblViewExperince
        {
            return dataArray.count
        }
        
        else if tableView == tblViewFliterCategory
        {
            return FilterArray.count
        }
        
        else if tableView == tblViewDetailFilter
        {
            let detail = FilterArray.count > 0 ? self.detail : []
            return detail.count
        }
        
        else if tableView == tblViewNotification
        {
            let detail = self.arrNotification[section]["detail"] as? [dictionary] ?? []
            
            return detail.count
        }
        
        else
        {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblViewExperince
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdminExperimentcell", for: indexPath) as! AdminExperimentcell
            
            cell.lblTitle.text = dataArray[indexPath.row].name
            cell.lblTime.text = dataArray[indexPath.row].created_date
            cell.lblDesc.text = dataArray[indexPath.row].reason_for_development
            cell.btnState.setTitle(dataArray[indexPath.row].types_of_recipe, for: .normal)
            
            if dataArray[indexPath.row].priority == "urgent"
            {
                cell.lblPriority.text = dataArray[indexPath.row].priority
                cell.lblPriority.textColor = UIColor.urgentColor
                cell.vwDotPriority.backgroundColor = UIColor.urgentColor
            }
            
            else if dataArray[indexPath.row].priority == "normal"
            {
                cell.lblPriority.text = dataArray[indexPath.row].priority
                cell.lblPriority.textColor = UIColor.normalColor
                cell.vwDotPriority.backgroundColor = UIColor.normalColor
            }
            
            if segmentControler.selectedSegmentIndex == 0
            {
                cell.vwRequestAction.isHidden = true
                cell.lblViewResult.isHidden = true
            }
            
            else if segmentControler.selectedSegmentIndex == 3
            {
                cell.vwRequestAction.isHidden = true
                cell.lblViewResult.isHidden = true
            }
            
            else if segmentControler.selectedSegmentIndex == 1
            {
                cell.lblViewResult.isHidden = false
                
                if dataArray[indexPath.row].permissions["is_view_requested"] == 1
                {
                    cell.vwRequestAction.isHidden = false
                    
                    if dataArray[indexPath.row].permissions["is_view_requested_approved"] == 1
                    {
                        cell.lblWhichRequest.text = "View recipe"
                        cell.imgForAnyRequest.image = UIImage(named: "eyeActive")
                    }
                    
                    else
                    {
                        cell.lblWhichRequest.text = "Requested for view Recipe"
                        cell.imgForAnyRequest.image = UIImage(named: "eyeforrequest")
                    }
                }
                
                else if dataArray[indexPath.row].permissions["is_modify_requested"] == 1
                {
                    cell.vwRequestAction.isHidden = false
                    
                    if dataArray[indexPath.row].permissions["is_modify_requested_approved"] == 1
                    {
                        cell.lblWhichRequest.text = "Modify Recipe"
                        cell.imgForAnyRequest.image = UIImage(named: "checkmark")
                    }
                    
                    else
                    {
                        cell.lblWhichRequest.text = "Requested for modification"
                        cell.imgForAnyRequest.image = UIImage(named: "modify")
                    }
                    
                }
                
                else
                {
                    cell.vwRequestAction.isHidden = true
                }
                
            }
            
            return cell
        }
        
        else if tableView == tblViewFliterCategory
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdminFilterCategoryTableCell", for: indexPath) as! AdminFilterCategoryTableCell
            
            cell.lblFilterCategory.text = self.FilterArray[indexPath.row]["name"] as? String ?? ""
            cell.lblFilterCategory.textColor = UIColor(hex: "#222222ff")
            cell.layer.backgroundColor = UIColor.clear.cgColor
            
            if indexPath.row == filterSelectedIndex
            {
                cell.lblFilterCategory.textColor = UIColor.mainColor
                cell.layer.backgroundColor = UIColor(hex: "#ebeaecff")?.cgColor
            }
            
            return cell
            
        }
        
        else if tableView == tblViewDetailFilter
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdminIngredientTableCell", for: indexPath) as! AdminIngredientTableCell
            
            cell.lblIngredient.text = detail[indexPath.row]
            
            if isFilterApply{
                
            }
            
            let filterCategoryName = self.FilterArray[filterSelectedIndex]["name"] as? String ?? ""
            switch filterCategoryName {
            case "Ingredients":
                if arrIngFilter.contains(self.detail[indexPath.row]){
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
            case "Contact Person":
                if arrContactPerson.contains(self.detail[indexPath.row]){
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
            case "Company Name":
                if arrCompanyName.contains(self.detail[indexPath.row]){
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
            case "Importance":
                if arrImportance.contains(self.detail[indexPath.row]){
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
            case "Result":
                if arrResult.contains(self.detail[indexPath.row]){
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
            default:
                print("Filter Not Found")
                cell.accessoryType = .none
            }
            
            return cell
        }
        
        else
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
            
            let detail = self.arrNotification[indexPath.section]["detail"] as? [dictionary] ?? []
            cell.lblExperimentName.text = detail[indexPath.row]["experiment_name"] as? String ?? ""
            cell.lblTime.text = detail[indexPath.row]["date"] as? String ?? ""
            
            cell.lblResult.text = "Add Your result"
            cell.btnEditReminder.setTitle("\("Edit Reminder".localizableString(loc: SelectedLanguage))", for: .normal)
            cell.btnNotNow.setTitle("\("Not Now".localizableString(loc: SelectedLanguage))", for: .normal)
            cell.layer.cornerRadius = 13
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == tblViewNotification
        {
            return 35
        }
        
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if tableView == tblViewNotification
        {
            return 20
        }
        
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 20, y: 2, width: 320, height: 30)
        myLabel.font = UIFont.init(name: "SFProText-Medium", size: 24)
        myLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        myLabel.textColor = #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1333333333, alpha: 1)
        
        let headerView = UIView()
        headerView.addSubview(myLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if tableView == tblViewNotification
        {
            return self.arrNotification[section]["lable"] as? String ?? ""
        }
        
        else
        {
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tblViewDetailFilter
        {
            let filterCategoryName = self.FilterArray[filterSelectedIndex]["name"] as? String ?? ""
            self.detail = self.FilterArray[self.filterSelectedIndex]["detail"] as? [String] ?? []
            
            let cell = tableView.cellForRow(at: indexPath)!
            if cell.accessoryType == .checkmark {
                
                cell.accessoryType = .none
                switch filterCategoryName {
                case "Ingredients":
                    self.arrIngFilter = self.arrIngFilter.filter {$0 != self.detail[indexPath.row]}
                    print(arrIngFilter)
                case "Contact Person":
                    self.arrContactPerson = self.arrContactPerson.filter {$0 != self.detail[indexPath.row]}
                    print(arrContactPerson)
                case "Company Name":
                    self.arrCompanyName = self.arrCompanyName.filter {$0 != self.detail[indexPath.row]}
                    print(arrCompanyName)
                case "Importance":
                    self.arrImportance = self.arrImportance.filter {$0 != self.detail[indexPath.row]}
                    print(arrImportance)
                case "Result":
                    self.arrResult = self.arrResult.filter {$0 != self.detail[indexPath.row]}
                    print(arrResult)
                default:
                    print("Filter Not Found")
                }
                
            } else {
                
                cell.accessoryType = .checkmark
                switch filterCategoryName {
                case "Ingredients":
                    arrIngFilter.append(self.detail[indexPath.row])
                    print(arrIngFilter)
                case "Contact Person":
                    arrContactPerson.append(self.detail[indexPath.row])
                    print(arrContactPerson)
                case "Company Name":
                    arrCompanyName.append(self.detail[indexPath.row])
                    print(arrCompanyName)
                case "Importance":
                    arrImportance.append(self.detail[indexPath.row])
                    print(arrImportance)
                case "Result":
                    arrResult.append(self.detail[indexPath.row])
                    print(arrResult)
                default:
                    print("Filter Not Found")
                }
                
            }
        }
        
        else if tableView == tblViewFliterCategory
        {
            
            self.filterSelectedIndex = indexPath.row
            self.detail = self.FilterArray[self.filterSelectedIndex]["detail"] as? [String] ?? []
            if detail.isEmpty{
                self.tblViewDetailFilter.isHidden = true
                self.txtFilter.isHidden = false
                
                let filterCategoryName = self.FilterArray[filterSelectedIndex]["name"] as? String ?? ""
                if filterCategoryName == "Date & Time"{
                    self.txtFilter.inputView = dateTimePicker
                    dateTimePicker.datePickerMode = .dateAndTime
                    dateTimePicker.maximumDate = Date()
                    dateTimePicker.addTarget(self, action: #selector(onClickDate), for: .valueChanged)
                    self.txtFilter.text = filterDateTime
                    self.txtFilter.clearButtonMode = .always
                }else if filterCategoryName == "Property/Purity"{
                    self.txtFilter.keyboardType = .decimalPad
                    self.txtFilter.text = filterPropertyPurity
                    self.txtFilter.clearButtonMode = .never
                }
            }else{
                self.txtFilter.isHidden = true
                self.tblViewDetailFilter.isHidden = false
                self.tblViewDetailFilter.reloadData()
            }
            tableView.reloadData()
        }
        
        else if segmentControler.selectedSegmentIndex == 3
        {
            let vc: EditExpViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.experiment_id = dataArray[indexPath.row].id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        else if segmentControler.selectedSegmentIndex == 0
        {
            let vc: ListExpViewController = UIStoryboard(name: "Home", bundle: nil).instantiateVC()!
            vc.experiment_id = dataArray[indexPath.row].experiment_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        else if segmentControler.selectedSegmentIndex == 1
        {
//            if tableView == tblViewExperince
//            {
//                alert(title: "\("Choose an option".localizableString(loc: SelectedLanguage))", message: "\("Confirmation will be send to admin".localizableString(loc: SelectedLanguage))", actions: ["\("Modify Recipe".localizableString(loc: SelectedLanguage))","\("View Recipe".localizableString(loc: SelectedLanguage))","\("Cancel".localizableString(loc: SelectedLanguage))"]) { (k) in
//
//                    if k == 0
//                    {
//                        self.dismiss(animated: true, completion: nil)
//
//                        if self.dataArray[indexPath.row].permissions["is_modify_requested"] == 1
//                        {
//                            self.alert(title: "Can not request for Modify Recipe.", message: "your one modify request is already been sent")
//                        }
//
//                        else if self.dataArray[indexPath.row].permissions["is_view_requested"] == 1
//                        {
//                            self.alert(title: "Can not request for view Recipe.", message: "your one view Recipe request is already been sent")
//                        }
//
//                        else
//                        {
//                            self.RequestSendForAction(ExperimentID: self.dataArray[indexPath.row].id, WhichAction: "modify")
//                        }
//
//                    }
//
//                    else if k == 1
//                    {
//                        self.dismiss(animated: true, completion: nil)
//
//                        if self.dataArray[indexPath.row].permissions["is_view_requested"] == 1
//                        {
//                            self.alert(title: "Can not request for view Recipe.", message: "your one view Recipe request is already been sent")
//                        }
//
//                        else if self.dataArray[indexPath.row].permissions["is_modify_requested"] == 1
//                        {
//                            self.alert(title: "Can not request for Modify Recipe.", message: "your one modify request is already been sent")
//                        }
//
//                        else
//                        {
//                            self.alertForViewRecipePassword(index: indexPath.row)
//                        }
//                    }
//                }
//            }
            
            if tableView == tblViewExperince
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AdminSubRegisterViewController") as! AdminSubRegisterViewController
                
                vc.SelectedExperimentID = dataArray[indexPath.row].id
                vc.PreviousSelectedProjectName = dataArray[indexPath.row].name
            
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension AdminHomeViewController: UIPickerViewDataSource, UIPickerViewDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return FinalResultArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if txtRecipeName.isEditing == true
        {
            return FinalResultArray[row].name
        }
        
        else
        {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if txtRecipeName.isEditing == true
        {
            PickerViewIndex = row
            txtRecipeName.text = FinalResultArray[row].name
            lblFinalResultDate.text = FinalResultArray[row].date
            lblFinalResultDescription.text = FinalResultArray[row].description
            cvParameter.reloadData()
            doneClick()
        }
    }
}

extension AdminHomeViewController: UICollectionViewDelegate,UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if FinalResultArray.count > 0
        {
            return FinalResultArray[PickerViewIndex].Parameter.count
        }
        
        else
        {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResultParameterCell", for: indexPath) as! ParameterFinalResultCell
        
        let para = FinalResultArray[PickerViewIndex].Parameter
        
        cell.lblHeadingParameter.text = "\(para[indexPath.row]["name"] as! String)"
        cell.lblParameterCount.text = "\(para[indexPath.row]["value"] as! String)"
        
        return cell
    }
}

extension AdminHomeViewController
{
    func alertRegisterSuccessForModifyRecipe()
    {
        
        let alert = UIAlertController(title: "\("Request for modify recipe has been sent successfully".localizableString(loc: SelectedLanguage))", message: nil, preferredStyle: .alert)
        
        let viewRecipeAction = UIAlertAction(title: "\("Got it!".localizableString(loc: SelectedLanguage))", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
            
        }
        
        let image = UIImageView(frame: CGRect(x: 10, y: 80, width: 100, height: 100))
        image.contentMode = .scaleAspectFit
        image.image = #imageLiteral(resourceName: "checkmark")
        image.translatesAutoresizingMaskIntoConstraints = false
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .centerX, relatedBy: .equal, toItem: alert.view, attribute: .centerX, multiplier: 1, constant: 0))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .top, relatedBy: .equal, toItem: alert.view, attribute: .top, multiplier: 1, constant: 80))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100.0))
        
        let label = UILabel(frame: CGRect(x: 10, y: 200, width: 250, height: 80))
        label.text = "\("You will be able to modify recipe once all admin confirm your request.".localizableString(loc: SelectedLanguage))"
        label.numberOfLines = 0
        label.font = UIFont(name: "SFProText-Regular", size: 13)
        label.textAlignment = .center
        
        alert.view.addSubview(image)
        alert.view.addSubview(label)
        
        let height = NSLayoutConstraint(item: alert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 350)
        
        alert.view.addConstraint(height)
        
        alert.addAction(viewRecipeAction)
        
        self.present(alert, animated: true, completion: nil)
        
        getData()
        tblViewExperince.reloadData()
        
    }
    
    func alertRegisterSuccessForViewRecipe()
    {
        let alert = UIAlertController(title: "\("Request for View recipe has been sent successfully".localizableString(loc: SelectedLanguage))", message: nil, preferredStyle: .alert)
        
        let viewRecipeAction = UIAlertAction(title: "\("Got it!".localizableString(loc: SelectedLanguage))", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
            
        }
        
        let image = UIImageView(frame: CGRect(x: 10, y: 80, width: 100, height: 100))
        image.contentMode = .scaleAspectFit
        image.image = #imageLiteral(resourceName: "checkmark")
        image.translatesAutoresizingMaskIntoConstraints = false
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .centerX, relatedBy: .equal, toItem: alert.view, attribute: .centerX, multiplier: 1, constant: 0))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .top, relatedBy: .equal, toItem: alert.view, attribute: .top, multiplier: 1, constant: 80))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100.0))
        
        let label = UILabel(frame: CGRect(x: 10, y: 200, width: 250, height: 80))
        label.text = "\("You will be able to view recipe once all admin confirm your request.".localizableString(loc: SelectedLanguage))"
        label.numberOfLines = 0
        label.font = UIFont(name: "SFProText-Regular", size: 13)
        label.textAlignment = .center
        
        alert.view.addSubview(image)
        alert.view.addSubview(label)
        
        let height = NSLayoutConstraint(item: alert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 350)
        
        alert.view.addConstraint(height)
        
        alert.addAction(viewRecipeAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func alertViewRecipeReson(actionHandler: ((_ text: String?) -> Void)? = nil)
    {
        let alert = UIAlertController(title: "\("View Recipe".localizableString(loc: SelectedLanguage))",
                                      message: "\("Confirmation will be send to admin".localizableString(loc: SelectedLanguage))",
                                      preferredStyle: .alert)
        
        let txtArea = UITextView(frame: CGRect(x: 10, y: 80, width: 250, height: 148))
        txtArea.placeholder = "\("Enter reason to view recipe".localizableString(loc: SelectedLanguage))"
        txtArea.font = UIFont(name: "SFProText-Regular", size: 15)
        txtArea.layer.cornerRadius = 11
        txtArea.doneAccessory = true
        
        let height = NSLayoutConstraint(item: alert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 338)
        alert.view.addConstraint(height)
        alert.view.addSubview(txtArea)
        
        let saveAction = UIAlertAction(title: "\("Submit Request".localizableString(loc: SelectedLanguage))", style: .default) { (action) in
            
            actionHandler?(txtArea.text)
        }
        
        let cancelAction = UIAlertAction(title: "\("Cancel".localizableString(loc: SelectedLanguage))", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func alertFinalForOTP(actionHandler: ((_ text: String?) -> Void)? = nil,WhichTitle: String)
    {
        let alert = UIAlertController(title: "\("\(WhichTitle)".localizableString(loc: SelectedLanguage))",
                                      message: "\("OTP has been sent to Admin. Kindly enter 3 OTP one by one.\nDuration for each OTP is 60seconds.".localizableString(loc: SelectedLanguage))",
                                      preferredStyle: .alert)
        
        let txtArea = UITextField(frame: CGRect(x: 10, y: 105, width: 250, height: 54))
        txtArea.placeholder = "\("  Enter OTP".localizableString(loc: SelectedLanguage))"
        txtArea.textAlignment = .left
        txtArea.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        txtArea.isSecureTextEntry = true
        txtArea.font = UIFont(name: "SFProText-Regular", size: 15)
        txtArea.layer.cornerRadius = 11
        txtArea.doneAccessory = true
        
        lblOTP.font = UIFont(name: "SFProText-Regular", size: 15)
        lblOTP.textColor = #colorLiteral(red: 0, green: 0.537254902, blue: 0.9450980392, alpha: 1)
        lblOTP.text = "OTP: \(RemainingAdminEnterOTP) of \(TotalAdminEnterOTP)"
        
        lblTimeRemaining.font = UIFont(name: "SFProText-Regular", size: 15)
        lblTimeRemaining.textColor = #colorLiteral(red: 0, green: 0.537254902, blue: 0.9450980392, alpha: 1)
        lblTimeRemaining.text = "Time Remaining: \(OTPTimeCounter)seconds"
        
        let height = NSLayoutConstraint(item: alert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 293)
        
        alert.view.addConstraint(height)
        alert.view.addSubview(txtArea)
        alert.view.addSubview(lblOTP)
        alert.view.addSubview(lblTimeRemaining)
        
        let cancelAction = UIAlertAction(title: "\("Cancel".localizableString(loc: SelectedLanguage))", style: .default) { (action) in
            self.OTPTimer.invalidate()
            self.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(cancelAction)
        
        if RemainingAdminEnterOTP == TotalAdminEnterOTP
        {
            let saveAction = UIAlertAction(title: "\("Submit".localizableString(loc: SelectedLanguage))", style: .default) { (action) in
                actionHandler?(txtArea.text)
                
                if txtArea.text?.count == 0
                {
                    txtArea.placeholder = "\("  Please Enter OTP".localizableString(loc: SelectedLanguage))"
                }

                else
                {
                    self.StopOTPTimer()
                    self.RemainingAdminEnterOTP += 1
                    
                    if self.RemainingAdminEnterOTP < self.TotalAdminEnterOTP + 1
                    {
                        self.OTPTimeCounter = self.FinalTimeForOTPEnter
                        self.alertFinalForOTP(WhichTitle: "Modify Recipe".localizableString(loc: SelectedLanguage))
                        
                        self.StartOTPTimer()
                    }
                    
               }
                
            }
            
            alert.addAction(saveAction)
            
            alert.addTextField { (textfiled) in
                txtArea.text = ""
                saveAction.isEnabled = false
                NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: txtArea, queue: .main) { (notification) in
                    saveAction.isEnabled = txtArea.text!.count > 0
                }
            }
        }
        
        else
        {
            let saveAction = UIAlertAction(title: "\("Next".localizableString(loc: SelectedLanguage))", style: .default) { (action) in
                actionHandler?(txtArea.text)
                
                if txtArea.text?.count == 0
                {
                    txtArea.placeholder = "\("  Please Enter OTP".localizableString(loc: SelectedLanguage))"
                }

                else
                {
                    self.StopOTPTimer()
                    self.RemainingAdminEnterOTP += 1
                    
                    if self.RemainingAdminEnterOTP < self.TotalAdminEnterOTP + 1
                    {
                        self.OTPTimeCounter = self.FinalTimeForOTPEnter
                        self.alertFinalForOTP(WhichTitle: "Modify Recipe".localizableString(loc: SelectedLanguage))
                        
                        self.StartOTPTimer()
                    }
                }
            }
                alert.addAction(saveAction)
            
            alert.addTextField { (textfiled) in
                txtArea.text = ""
                saveAction.isEnabled = false
                NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: txtArea, queue: .main) { (notification) in
                    saveAction.isEnabled = txtArea.text!.count > 0
                }
            }
        
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func alertForViewRecipePassword(index: Int)
    {
        self.showInputDialogAdmin(title: "\("View Recipe".localizableString(loc: SelectedLanguage))", subtitle: "\("Enter your password to continue".localizableString(loc: SelectedLanguage))", actionTitle: "\("Next".localizableString(loc: SelectedLanguage))", cancelTitle: "\("Cancel".localizableString(loc: SelectedLanguage))",ispasword: true ,inputPlaceholder: "\("Enter your password".localizableString(loc: SelectedLanguage))", inputKeyboardType: .default, cancelHandler: { (action) in
            
            
            self.dismiss(animated: true, completion: nil)
        }) { (str) in
            
            print(str)
            
            let dic : dictionary = [
                
                "token" : self.userInfo.access_token,
                "admin_id" : self.userId,
                "password" : str as? String ?? "",
                
            ]
            
            API.shared.VerifyPassword(parameters: dic) { (k) in
                if k
                {
                    print("true")
                    
                    self.alertViewRecipeReson { (str) in
                        
                        self.RequestSendForAction(ExperimentID: self.dataArray[index].id, WhichAction: "view", Reason: str)
                    }
                }
                
                else
                {
                    self.alert(title: "Please Try again...", message: "The password you entered did not match our records. Please double-check and try again.")
                    print("false")
                }
            }
        }
    }
}

class AdminExperimentcell : UITableViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblViewResult: UILabel!
    @IBOutlet weak var vwRequestAction: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnState: UIButton!{
        didSet{
            self.btnState.roundCorners(corners: [.topRight, .bottomLeft], radius: 10)
        }
    }
    @IBOutlet weak var vwDotPriority: UIView!
    @IBOutlet weak var imgForAnyRequest: UIImageView!
    @IBOutlet weak var lblWhichRequest: UILabel!
    @IBOutlet weak var lblPriority: UILabel!
    @IBOutlet weak var btnChain: UIButton!{
        didSet{
            self.btnChain.roundCorners(corners: [.topLeft, .bottomRight], radius: 10)
        }
    }
}

class AdminIngredientTableCell: UITableViewCell
{
    @IBOutlet weak var lblIngredient: UILabel!
}

class AdminFilterCategoryTableCell: UITableViewCell
{
    @IBOutlet weak var lblFilterCategory: UILabel!
}

class ParameterFinalResultCell: UICollectionViewCell
{
    @IBOutlet weak var lblHeadingParameter: UILabel!
    @IBOutlet weak var lblParameterCount: UILabel!
}

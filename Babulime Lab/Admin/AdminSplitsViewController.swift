//
//  AdminSplitsViewController.swift
//  Babulime Lab
//
//  Created by Ashish Mandaliya on 02/09/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class AdminSplitsViewController:  UISplitViewController, UISplitViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        if AuthenticationManager.sharedInstance.needsAuthentication{
            willEnterForeground()
        }

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
    }
    
    @objc func willEnterForeground(){
        let tView = UIView(frame: self.view.frame)
        tView.backgroundColor = .clear
        tView.isUserInteractionEnabled = false
        self.view.addSubview(tView)
        
        if AuthenticationManager.sharedInstance.needsAuthentication{
            let vc: AdminSecureViewController = UIStoryboard(name: "Admin", bundle: nil).instantiateVC()!
            self.add(vc, parent: self.view)
            vc.dismissVC = {
                vc.view.removeFromSuperview()
                vc.removeFromParent()
                AuthenticationManager.sharedInstance.needsAuthentication = false
                self.viewWillAppear(true)
            }
        }
    }

}

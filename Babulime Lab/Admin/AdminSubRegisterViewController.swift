//
//  AdminSubRegisterViewController.swift
//  Babulime Lab
//
//  Created by Ashish Mandaliya on 07/09/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class AdminSubRegisterViewController: BaseViewController {
    
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> origin/master
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPurity: UILabel!
    @IBOutlet weak var lblPurpose: UILabel!
    @IBOutlet weak var lblContactName: UILabel!
    @IBOutlet weak var lblContactNumber: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var imgProductDetailSheet: UIImageView!
    @IBOutlet weak var imgCertificate: UIImageView!
<<<<<<< HEAD
=======
=======
    @IBOutlet weak var lblPurpose: UILabel!
    @IBOutlet weak var imgCertificate: UIImageView!
    @IBOutlet weak var lblContactNumber: UILabel!
    @IBOutlet weak var imgProductDetailSheet: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblContactName: UILabel!
    @IBOutlet weak var lblPurity: UILabel!
>>>>>>> 2c50902ba94781086bc63e41aa40a822616dc54c
>>>>>>> origin/master
    @IBOutlet weak var lblDescriptionTestResult: UILabel!
    @IBOutlet weak var lblHeadingIngrdients: UILabel!
    @IBOutlet weak var lblMainHeading: UILabel!
    @IBOutlet weak var vwAdminResult: UIView!
    @IBOutlet weak var cvForTestResult: UICollectionView!
    @IBOutlet weak var vwAdminIngrdients: UIView!
    @IBOutlet weak var lblPriorityAdminSub: UILabel!
    @IBOutlet weak var lblCertificateAdminSub: UILabel!
    @IBOutlet weak var lblProductSheetAdminSub: UILabel!
    @IBOutlet weak var lblContactNumberAdminSub: UILabel!
    @IBOutlet weak var vwInternetLoss: UIView!
    @IBOutlet weak var lblContactNameAdminSub: UILabel!
    @IBOutlet weak var vwServerdown: UIView!
    @IBOutlet weak var lblPurposeAdminSub: UILabel!
    @IBOutlet weak var tblViewAdminIngrdient: UITableView!
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var lblNameAdminSub: UILabel!
    @IBOutlet weak var segmentAdminSubRegister: UISegmentedControl!{
        didSet{
            self.segmentAdminSubRegister.addTarget(self, action: #selector(valueChangeSegment), for: .valueChanged)
        }
    }
    
    var Heading = ["Water","Sourness","CoA"]
    var Percentage = ["10","20","40"]
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> origin/master
    var arrParameter: [dictionary] = []
    var SelectedExperimentID : Int = 0
    var data = RegisterResult(data: [:])
    var IngredientList = [AddedIngredient]()
    var selectedIndex = 0
    var PreviousSelectedProjectName = ""
    
=======
    var IngredientList = [AddedIngredient]()
    var experiment_id = 0
    var selectedIndex = 0
>>>>>>> 2c50902ba94781086bc63e41aa40a822616dc54c
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vwAdminResult.isHidden = true
        vwServerdown.isHidden = true
        vwInternetLoss.alpha = 0
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleInternetActivity), name: NSNotification.Name(rawValue: "ReachabilityManagerUpdate"), object: nil)
        
        let cellWidth : CGFloat = 135
        let cellheight : CGFloat = 90
        let cellSize = CGSize(width: cellWidth , height:cellheight)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        self.cvForTestResult.setCollectionViewLayout(layout, animated: true)
        self.cvForTestResult.reloadData()
        
        lblMainHeading.text = "Limewater Breathe Experiment"
        
<<<<<<< HEAD
=======
    }
    
    fileprivate func viewData(data: AddedIngredient){
        self.lblName.text = data.name
        self.lblPurity.text = data.property_or_purity
        self.lblPurpose.text = data.purpose
        self.lblContactName.text = data.contact_name
        self.lblContactNumber.text = data.contact_number
        //self.lblCompanyName.text = data.company_name
        if let data = try? Data(contentsOf: URL(string: data.product_detail_sheet) ?? URL(fileURLWithPath: "")){
            self.imgProductDetailSheet.image = UIImage(data: data)
        }
        if let data = try? Data(contentsOf: URL(string: data.certificate) ?? URL(fileURLWithPath: "")){
            self.imgCertificate.image = UIImage(data: data)
        }
>>>>>>> origin/master
    }
    
    @objc func handleInternetActivity()
    {
        if (Utility.getUserDefaults(forKey: "isInternetConnected")?.boolValue)!
        {
            print("Internet Ok")
            self.vwInternetLoss.alpha = 0
            
        }
        else
        {
            print("Not Ok")
            UIView.animate(withDuration: 0.2) {
                self.vwInternetLoss.alpha = 1
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        self.lblDescriptionTestResult.text = "Description".localizableString(loc: SelectedLanguage)
        self.lblHeadingIngrdients.text = "Ingredients".localizableString(loc: SelectedLanguage)
        segmentAdminSubRegister.setTitle("Ingredients".localizableString(loc: SelectedLanguage), forSegmentAt: 0)
        segmentAdminSubRegister.setTitle("Test Result".localizableString(loc: SelectedLanguage), forSegmentAt: 1)
        self.lblNameAdminSub.text = "Name".localizableString(loc: SelectedLanguage)
        self.lblPriorityAdminSub.text = "Property/Purity".localizableString(loc: SelectedLanguage)
        self.lblPurposeAdminSub.text = "Purpose".localizableString(loc: SelectedLanguage)
        self.lblProductSheetAdminSub.text = "Product detail sheet".localizableString(loc: SelectedLanguage)
        self.lblCertificateAdminSub.text = "Certificate".localizableString(loc: SelectedLanguage)
        self.lblContactNameAdminSub.text = "Contact name".localizableString(loc: SelectedLanguage)
        self.lblContactNumberAdminSub.text = "Contact number".localizableString(loc: SelectedLanguage)
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        
        lblMainHeading.text = PreviousSelectedProjectName
        getIngredientList()
        getDataTestResult()
        
    }
    
    func getIngredientList(){
        self.startLoader()
        let dic:dictionary = [
            "token": userInfo.access_token,
            "experiment_id": SelectedExperimentID
        ]

        API.shared.getIngredientInExp(parameters: dic) { (arrData) in
            self.stopLoader()
            self.IngredientList.removeAll()
            arrData.forEach { (data) in
                let dic: dictionary = [
                    "token": self.userInfo.access_token,
                    "ingredient_id": data["id"] as? Int ?? -1
                ]
                API.shared.getIngredientDetail(parameters: dic) { (data) in
                    self.IngredientList.append(AddedIngredient(data: data))
                    self.tblViewAdminIngrdient.reloadData()
                    if !self.IngredientList.isEmpty
                    {
                        self.viewData(data: self.IngredientList[self.selectedIndex])
                    }
                }
            }
            
        }
    }
    
    func getDataTestResult(){
        let dic: dictionary = [
            "experiment_id": SelectedExperimentID,
            "token": userInfo.access_token
            ]
        
        API.shared.getResultExp(parameters: dic) { (dic) in
            if !dic.isEmpty{
                self.data = RegisterResult(data: dic[0])
                self.lblDescriptionTestResult.text = self.data.description
                self.arrParameter = self.data.parameter
                self.cvForTestResult.reloadData()
            }else {
                self.alert(title: self.getLanguageData(strKey: "alertNotApprovedTitle"), message: self.getLanguageData(strKey: "alertNotApprovedMsg")) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    fileprivate func viewData(data: AddedIngredient){
        self.lblName.text = data.name
        self.lblPurity.text = data.property_or_purity
        self.lblPurpose.text = data.purpose
        self.lblContactName.text = data.contact_name
        self.lblContactNumber.text = data.contact_number
        self.lblCompanyName.text = data.company_name
        let sheet = URL(string: data.product_detail_sheet)
        let certificate = URL(string: data.certificate)
        DispatchQueue.global().async {
                    let imgSheet = try? Data(contentsOf: sheet!)
                    let imgCertificate = try? Data(contentsOf: certificate!)//make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.imgProductDetailSheet.image = UIImage(data: imgSheet!)
                        self.imgCertificate.image = UIImage(data: imgCertificate!)
                        //parentVC.stopLoader()
                    }
                }
    }
    
    @objc func valueChangeSegment(_ sender: UISegmentedControl)
    {
        switch sender.selectedSegmentIndex{
        case 0 :
            UIView.animate(withDuration: 0.1) {
                self.vwAdminResult.isHidden = true
                self.vwAdminIngrdients.isHidden = false
            }
        case 1 :
            UIView.animate(withDuration: 0.1) {
                self.vwAdminResult.isHidden = false
                self.vwAdminIngrdients.isHidden = true
            }
            
        default:
            return
        }
    }
    
    @IBAction func btnBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension AdminSubRegisterViewController : UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrParameter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TestResultCell", for: indexPath) as! TestResultCell
        
        cell.lblHeading.text = arrParameter[indexPath.row]["name"] as? String
        cell.lblValue.text = arrParameter[indexPath.row]["value"] as? String
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        self.heightCollectionView.constant = cvForTestResult.contentSize.height
    }
}

extension AdminSubRegisterViewController : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return IngredientList.count
    }
    
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> origin/master
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AdminIngredientCell", for: indexPath) as? AdminIngredientCell else { return UITableViewCell() }
        if indexPath.row == selectedIndex{
            cell.viewBack.backgroundColor = UIColor.mainColor
            cell.lblName.textColor = UIColor.secondaryColor
        }else{
            cell.viewBack.backgroundColor = UIColor.clear
            cell.lblName.textColor = UIColor.black
        }
        cell.lblName.text = IngredientList[indexPath.row].name
        return cell
<<<<<<< HEAD
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
=======
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        viewData(data: IngredientList[selectedIndex])
        tableView.reloadData()
=======
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return IngredientList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AdminIngredient", for: indexPath) as? IngredientCell else { return UITableViewCell() }
        if indexPath.row == selectedIndex{
            cell.viewBack.backgroundColor = UIColor.mainColor
            cell.lblName.textColor = UIColor.secondaryColor
        }else{
            cell.viewBack.backgroundColor = UIColor.clear
            cell.lblName.textColor = UIColor.black
        }
        cell.lblName.text = IngredientList[indexPath.row].name
        return cell
>>>>>>> 2c50902ba94781086bc63e41aa40a822616dc54c
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
>>>>>>> origin/master
        selectedIndex = indexPath.row
        viewData(data: IngredientList[selectedIndex])
        tableView.reloadData()
    }
}

class TestResultCell: UICollectionViewCell
{
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblValue: UILabel!
}

class AdminIngredientCell: UITableViewCell
{
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblName: UILabel!
}

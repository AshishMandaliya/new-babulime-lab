//
//  AdminSecureViewController.swift
//  Babulime Lab
//
//  Created by Ashish Mandaliya on 05/10/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit
import LocalAuthentication

class AdminSecureViewController: BaseViewController {

    var dismissVC: ()->() = {}
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        let context = LAContext()
        var error: NSError?

        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!"

            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [weak self] success, authenticationError in

                DispatchQueue.main.async {
                    if success {
                        // navigate
                        self?.dismissVC()
                        
                    } else {
                        // error
                        self?.alert(title: "Authentication failed", message: "You could not be verified; please try again.")
                    }
                }
            }
        } else {
            self.alert(title: "Biometry unavailable", message: "Your device is not configured for biometric authentication.")
        }
    }
}

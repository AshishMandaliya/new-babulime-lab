//
//  UserData.swift
//  FinalProjectShopping
//
//  Created by Mac on 10/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit


enum UserInfoKeys:String {
    case email,pswd
}


class UserData {
   
    private var defaults: UserDefaults
    
    init(defaults: UserDefaults = .standard) {
        self.defaults = defaults
    }
    
    static var shared = UserData()
    
    func getUser() -> (userInfo: User?, isLogin:Bool){
        if let user = defaults.value(forKey: "userinfo") as? dictionary{
            return (User(data: user), true)
        }else{
            return (nil, false)
        }
        
    }
    
    
    
    @discardableResult
    func setUser(userinfo: dictionary) -> Bool {
       defaults.set(userinfo, forKey: "userinfo")
       return defaults.synchronize()
    }
    
    @discardableResult
    func removeUser() -> Bool{
        defaults.set(nil, forKey: "userinfo")
        return defaults.synchronize()
    }
    
    
    var language: String{
//      return UserDefaults().object(forKey: "AppLanguage") as? String ?? ""
        return defaults.value(forKey: "AppLanguage") as? String ?? ""
    }
    func setLanguage(language:String) {
//      UserDefaults().set(language, forKey: "AppLanguage")
        defaults.set(language, forKey: "AppLanguage")
        defaults.synchronize()
    }
    var languageID: String{
//      return UserDefaults().object(forKey: "AppLanguageID") as? String ?? ""
        return defaults.value(forKey: "AppLanguageID") as? String ?? ""
    }
    func setLanguageID(languageID:String) {
//      UserDefaults().set(languageID, forKey: "AppLanguageID")
//      UserDefaults().synchronize()
        defaults.set(languageID, forKey: "AppLanguageID")
        defaults.synchronize()
    }
    
    @discardableResult
    func setTouchId(enable: Bool) -> Bool {
       defaults.set(enable, forKey: "touchId")
       return defaults.synchronize()
    }
    
    func getTouchId() -> Bool{
        return defaults.value(forKey: "touchId") as? Bool ?? false
    }
    
}

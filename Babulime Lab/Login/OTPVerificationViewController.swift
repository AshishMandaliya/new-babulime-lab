//
//  OTPVerificationViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 20/10/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class OTPVerificationViewController: BaseViewController {
    
    @IBOutlet weak var txtOTP: UITextField!{
        didSet{
//            self.txtOTP.becomeFirstResponder()
            self.txtOTP.addTarget(self, action: #selector(txt), for: .editingChanged)

        }
    }

    @IBOutlet weak var btnContinue: UIButton!{
        didSet{
            self.btnContinue.setTitleColor(.init(white: 1, alpha: 0.5), for: .disabled)
            self.btnContinue.setTitleColor(.white, for: .normal)
            self.btnContinue.isEnabled = false
        }
    }
    
    @IBOutlet weak var btnWrongMobileNumber: UIButton!
    @IBOutlet weak var lblTitleVerifyOTP: UILabel!
    @IBOutlet weak var lblSubTitleVerifyOTP: UILabel!
    @IBOutlet weak var lblTimerResend: UILabel!
    
    var mobileNumber = ""
    var countTimer = 59
    var timer: Timer?
    var userInfoDictionary: dictionary = [:]
    
    var btnResend: UIButton = {
        let size: CGFloat = 30.0
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0.0, width: size*3.3, height: size)
//        button.setTitle(getLanguageData(strKey: "btnResend"), for: .normal)
//        button.setTitle(getLanguageData(strKey: "btnContinue"), for: .disabled)
        button.setTitleColor(UIColor.mainColor, for: .normal)
        button.setTitleColor(UIColor.mainColor?.withAlphaComponent(0.5), for: .disabled)
        button.addTarget(self, action: #selector(onClickResend), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let viewContainer = UIView()
        viewContainer.frame = btnResend.bounds
        viewContainer.addSubview(btnResend)
        
        txtOTP.rightView = viewContainer
        txtOTP.rightViewMode = .always
        setLanguage()
        btnResend.isEnabled = false
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    private func setLanguage(){
        self.lblTitleVerifyOTP.text = self.getLanguageData(strKey: "titleOTPVerify")
        self.lblSubTitleVerifyOTP.text = UserData.shared.language == "en" ? "\(self.getLanguageData(strKey: "subTitleOTPVerify")) \(mobileNumber)" : "\(mobileNumber) \(self.getLanguageData(strKey: "subTitleOTPVerify"))"
        self.txtOTP.placeholder = self.getLanguageData(strKey: "titleOTPPlace")
        self.btnContinue.setTitle(self.getLanguageData(strKey: "btnContinue"), for: .normal)
        self.btnContinue.setTitle(self.getLanguageData(strKey: "btnContinue"), for: .disabled)
        self.btnWrongMobileNumber.setTitle(self.getLanguageData(strKey: "btnWrongNumber"), for: .normal)
//        self.btnWrongMobileNumber.setTitle(self.getLanguageData(strKey: "btnWrongNumber"), for: .disabled)
        self.lblTimerResend.text = UserData.shared.language == "en" ? "\(self.getLanguageData(strKey: "titleResendTimer")) 00:\(String(format: "%02d", countTimer))" : "00:\(String(format: "%02d", countTimer)) \(self.getLanguageData(strKey: "titleResendTimer"))"
        self.btnResend.setTitle(getLanguageData(strKey: "btnResend"), for: .normal)
        self.btnResend.setTitle(getLanguageData(strKey: "btnResend"), for: .disabled)
    }
    
    @IBAction func onClickWrongNumber(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickContinue(_ sender: UIButton){
        self.startLoader()
        let dic: dictionary = [
            "otp": self.txtOTP.text ?? "",
            "mobile_number": self.mobileNumber,
        ]
        
//        API.shared.varifyOTP(parameters: dic) { (data, msg) in
//            self.stopLoader()
//            if let timer = self.timer {
//                timer.invalidate()
//                self.timer = nil
//                let vc: ResetPasswordViewController = UIStoryboard(name: "Main", bundle: nil).instantiateVC()!
//                vc.userInfoDictionary = data
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//        }
        
        API.shared.varifyOTP(parameters: dic) { (data, msg) in
            self.stopLoader()
            if let _ = msg{
                //show alert
                self.alert(title: self.getLanguageData(strKey: "alertTitleIncorrectOTP"), message: self.getLanguageData(strKey: "alertMsgIncorrectOTP"))
            }else if let data = data{
                if let timer = self.timer {
                    timer.invalidate()
                    self.timer = nil
                    let vc: ResetPasswordViewController = UIStoryboard(name: "Main", bundle: nil).instantiateVC()!
                    vc.userInfoDictionary = data
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    @objc func onClickResend(_ sender: UIButton){
        print("Resend")
        self.startLoader()
        let dic: dictionary = [
            "mobile_number": self.mobileNumber,
        ]
        
        API.shared.forgotPassword(parameters: dic) { (otp, msg) in
            self.stopLoader()
            self.txtOTP.text = ""
            self.txt()
            print("OTP has been resend successfully")
            self.alert(title: self.getLanguageData(strKey: "alertTitleOTPResend"), message: self.getLanguageData(strKey: "alertMsgOTPResend")) {
                self.countTimer = 59
                self.btnResend.isEnabled = false
                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
            }
        }
    }

    @objc func updateTimer(_ sender: Timer){
        self.lblTimerResend.text = UserData.shared.language == "en" ? "\(self.getLanguageData(strKey: "titleResendTimer")) 00:\(String(format: "%02d", countTimer))" : "00:\(String(format: "%02d", countTimer)) \(self.getLanguageData(strKey: "titleResendTimer"))"
        if countTimer != 0 {
            countTimer -= 1
            self.btnResend.isEnabled = false
        } else {
            if let timer = self.timer {
                timer.invalidate()
                self.timer = nil
                self.btnResend.isEnabled = true
            }
        }
        self.txt()
    }
    
    @objc func txt(){
        self.btnContinue.isEnabled = self.txtOTP.text?.isEmpty == false && self.btnResend.isEnabled == false
    }
}


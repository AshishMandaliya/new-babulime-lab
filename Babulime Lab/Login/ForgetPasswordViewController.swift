//
//  ForgetPasswordViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 19/10/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseViewController {
    
    @IBOutlet weak var txtMobile: UITextField!{
        didSet{
            self.txtMobile.becomeFirstResponder()
            self.txtMobile.addTarget(self, action: #selector(txt), for: .editingChanged)

        }
    }
    
    @IBOutlet weak var btnContinue: UIButton!{
        didSet{
            self.btnContinue.setTitleColor(.init(white: 1, alpha: 0.5), for: .disabled)
            self.btnContinue.setTitleColor(.white, for: .normal)
            self.btnContinue.isEnabled = false
        }
    }
    
    @IBOutlet weak var btnBackLogin: UIButton!
    @IBOutlet weak var lblTitleForgetPassword: UILabel!
    @IBOutlet weak var lblSubTitleForgetPassword: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // setLanguage()
    }
    
    fileprivate func setLanguage(){
        
    }
    
    @IBAction func onClickContinue(_ sender: UIButton){
        //API Call and redirect to NextVC
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func txt(sender:UITextField){
        self.btnContinue.isEnabled = txtMobile.text?.isEmpty == false
    }

}

//
//  ForgetPasswordViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 19/10/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseViewController {
    
    @IBOutlet weak var txtMobile: UITextField!{
        didSet{
//            self.txtMobile.becomeFirstResponder()
            self.txtMobile.addTarget(self, action: #selector(txt), for: .editingChanged)

        }
    }
    
    @IBOutlet weak var btnContinue: UIButton!{
        didSet{
            self.btnContinue.setTitleColor(.init(white: 1, alpha: 0.5), for: .disabled)
            self.btnContinue.setTitleColor(.white, for: .normal)
            self.btnContinue.isEnabled = false
        }
    }
    
    @IBOutlet weak var btnBackLogin: UIButton!
    @IBOutlet weak var lblTitleForgetPassword: UILabel!
    @IBOutlet weak var lblSubTitleForgetPassword: UILabel!
    
    var otp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         setLanguage()
    }
    
    fileprivate func setLanguage(){
        self.lblTitleForgetPassword.text = self.getLanguageData(strKey: "titleForgotPassword")
        self.lblSubTitleForgetPassword.text = self.getLanguageData(strKey: "subTitleForgotPassword")
        self.txtMobile.placeholder = self.getLanguageData(strKey: "titleMobileNumberPlace")
        self.btnContinue.setTitle(self.getLanguageData(strKey: "btnContinue"), for: .normal)
        self.btnBackLogin.setTitle(self.getLanguageData(strKey: "btnBackToLogin"), for: .normal)
        self.btnContinue.setTitle(self.getLanguageData(strKey: "btnContinue"), for: .disabled)
        self.btnBackLogin.setTitle(self.getLanguageData(strKey: "btnBackToLogin"), for: .disabled)
    }
    
    @IBAction func onClickContinue(_ sender: UIButton){
        //API Call and redirect to NextVC
        self.startLoader()
        let dic: dictionary = [
            "mobile_number": self.txtMobile.text ?? "",
        ]
        
        API.shared.forgotPassword(parameters: dic) { (otp, msg) in
            self.stopLoader()
            if let _ = msg{
                //show alert
                self.alert(title: self.getLanguageData(strKey: "alertTitleMobileNumber"), message: self.getLanguageData(strKey: "alertMsgMobileNumber"))
            }else if let otp = otp{
                self.otp = "\(otp)"
                let vc: OTPVerificationViewController = UIStoryboard(name: "Main", bundle: nil).instantiateVC()!
                vc.mobileNumber = self.txtMobile.text ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
    @IBAction func onClickBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func txt(sender: UITextField){
        self.btnContinue.isEnabled = txtMobile.text?.isEmpty == false && self.isValidateMobile(value: txtMobile.text ?? "")
    }

}

//
//  ResetPasswordViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 21/10/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit

class ResetPasswordViewController: BaseViewController {
    
    @IBOutlet weak var txtPassword: UITextField!{
        didSet{
//            self.txtPassword.becomeFirstResponder()
            self.txtPassword.addTarget(self, action: #selector(txt), for: .editingChanged)
            self.txtPassword.rightViewWithButton()

        }
    }
    
    @IBOutlet weak var txtConfirmPassword: UITextField!{
        didSet{
            self.txtConfirmPassword.addTarget(self, action: #selector(txt), for: .editingChanged)
            self.txtConfirmPassword.rightViewWithButton()
        }
    }
    
    @IBOutlet weak var btnSaveChanges: UIButton!{
        didSet{
            self.btnSaveChanges.setTitleColor(.init(white: 1, alpha: 0.5), for: .disabled)
            self.btnSaveChanges.setTitleColor(.white, for: .normal)
            self.btnSaveChanges.isEnabled = false
        }
    }

    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var lblTitleSetNewPassword: UILabel!
    var userInfoDictionary: dictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setLanguage()
    }
    
    private func setLanguage(){
        self.lblTitleSetNewPassword.text = self.getLanguageData(strKey: "titleSetNewPassword")
        self.txtPassword.placeholder = self.getLanguageData(strKey: "txtNewPasswordPlace")
        self.txtConfirmPassword.placeholder = self.getLanguageData(strKey: "txtRepeatePasswordPlace")
        self.btnSkip.setTitle(self.getLanguageData(strKey: "btnSkip"), for: .normal)
        self.btnSaveChanges.setTitle(self.getLanguageData(strKey: "btnSaveChanges"), for: .normal)
        self.btnSaveChanges.setTitle(self.getLanguageData(strKey: "btnSaveChanges"), for: .disabled)
    }
    
    @IBAction func onClickSaveChanges(_ sender: UIButton){
        self.startLoader()
        let dic: dictionary = [
            "user_id": userInfoDictionary["user_id"] as? Int ?? -1,
            "new_password": self.txtPassword.text ?? "",
            "repeate_password": self.txtConfirmPassword.text ?? "",
        ]
        
        API.shared.setNewPassword(parameters: dic) { (str) in
            self.stopLoader()
            print(str)
            UserData.shared.setUser(userinfo: self.userInfoDictionary)
            if UserData.shared.getUser().userInfo?.is_admin == 1
            {
                appDelegate.rootToAdminHome()
            }
            
            else
            {
                appDelegate.rootToHome()
            }
        }
    }
    
    @IBAction func onClickSkip(_ sender: UIButton){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func txt(){
        self.btnSaveChanges.isEnabled = self.txtPassword.text?.isEmpty == false && self.txtConfirmPassword.text?.isEmpty == false && self.txtPassword.text == self.txtConfirmPassword.text
    }

}

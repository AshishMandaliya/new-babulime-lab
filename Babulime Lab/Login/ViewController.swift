//
//  ViewController.swift
//  Babulime Lab
//
//  Created by Gopi Donga on 05/08/20.
//  Copyright © 2020 Gopi Donga. All rights reserved.
//

import UIKit
import LocalAuthentication

class User{
    var user_id:Int
    var is_admin:Int
    var email:String
    var phone:String
    var name:String
    var profile_image: String
    var player_id:String
    var access_token:String
    var token_type:String
    var expires_in:String
    
    init(data: dictionary) {
        self.user_id = data["user_id"] as? Int ?? -1
        self.is_admin = data["is_admin"] as? Int ?? -1
        self.email = data["email"] as? String ?? ""
        self.phone = data["phone"] as? String ?? ""
        self.name = data["name"] as? String ?? ""
        self.profile_image = data ["profile_image"] as? String ?? ""
        self.player_id = data["player_id"] as? String ?? ""
        self.access_token = data["access_token"] as? String ?? ""
        self.token_type = data["token_type"] as? String ?? ""
        self.expires_in = data["expires_in"] as? String ?? ""
    }
}

class ViewController: BaseViewController {
    
    @IBOutlet weak var segLanguage: UISegmentedControl!{
        didSet{
            self.segLanguage.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.secondaryColor, NSAttributedString.Key.font: UIFont.regular14,], for: .selected)
            self.segLanguage.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.mainColor, NSAttributedString.Key.font: UIFont.regular14,], for: .normal)
            self.segLanguage.backgroundColor = UIColor.secondaryColor
            self.segLanguage.addTarget(self, action: #selector(valueChangeLanguage), for: .valueChanged)

        }
    }
    
    @IBOutlet weak var txtEmail: UITextField!{
        didSet{
//            self.txtEmail.becomeFirstResponder()
            self.txtEmail.addTarget(self, action: #selector(txt), for: .editingChanged)

        }
    }
    @IBOutlet weak var txtPswd: UITextField!{
        didSet{
            self.txtPswd.addTarget(self, action: #selector(txt), for: .editingChanged)
            self.txtPswd.rightViewWithButton()
        }
    }
    @IBOutlet weak var btnLogin: UIButton!{
        didSet{
            self.btnLogin.setTitleColor(.init(white: 1, alpha: 0.5), for: .disabled)
            self.btnLogin.setTitleColor(.white, for: .normal)
            self.btnLogin.isEnabled = false
        }
    }
    
    @IBOutlet weak var lblTitleLanguage: UILabel!
    @IBOutlet weak var btnTitleForgetPwd: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor.secondaryColor
        self.segLanguage.selectedSegmentIndex = UserData.shared.language == "en" ? 1 : 0
        setLanguage()
    }
    
    func setLanguage(){
        self.lblTitleLanguage.text = self.getLanguageData(strKey: "title_Language")
        self.btnTitleForgetPwd.setTitle(self.getLanguageData(strKey: "forgetPassword"), for: .normal)
        self.txtEmail.placeholder = self.getLanguageData(strKey: "ID")
        self.txtPswd.placeholder = self.getLanguageData(strKey: "password")
        self.btnLogin.setTitle(self.getLanguageData(strKey: "login"), for: .normal)
//        self.btnLogin.setTitle(self.getLanguageData(strKey: "login"), for: .disabled)
    }
    
    
    
    @IBAction func onClickLogin(_ sender: UIButton){
        self.startLoader()
        let loginDic : dictionary = [
            "username" : txtEmail.text ?? "",
            "password" : txtPswd.text ?? ""
        ]
        
        
        API.shared.login(parameters: loginDic) { (data, err) in
            guard let data = data else { return }
            guard let status = data["success"] as? Bool else { return }
            if status == true{
                self.stopLoader()
                UserData.shared.setUser(userinfo: data["data"] as? dictionary ?? [:])
                print(data)
                if UserData.shared.getUser().userInfo?.is_admin == 1
                {
                    appDelegate.rootToAdminHome()
                }
                
                else
                {
                    appDelegate.rootToHome()
                }
<<<<<<< HEAD:Babulime Lab/Login/ViewController.swift
=======
<<<<<<< HEAD:Babulime Lab/Login/ViewController.swift
=======
                
>>>>>>> 2c50902ba94781086bc63e41aa40a822616dc54c:Babulime Lab/ViewController.swift
>>>>>>> origin/master:Babulime Lab/ViewController.swift
            }else{
                self.stopLoader()
                self.alert(title: self.getLanguageData(strKey: "alertError"), message: self.getLanguageData(strKey: "alertWentWrong"))
            }
        }
    }
    
    @IBAction func onClickForgot(_ sender: UIButton){
        let vc: ForgotPasswordViewController = UIStoryboard(name: "Main", bundle: nil).instantiateVC()!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func valueChangeLanguage(_ sender: UISegmentedControl){
        if sender.selectedSegmentIndex == 0{
<<<<<<< HEAD:Babulime Lab/Login/ViewController.swift
=======
<<<<<<< HEAD:Babulime Lab/Login/ViewController.swift
=======
            
>>>>>>> 2c50902ba94781086bc63e41aa40a822616dc54c:Babulime Lab/ViewController.swift
>>>>>>> origin/master:Babulime Lab/ViewController.swift
            if UserData.shared.getUser().userInfo?.is_admin == 1
            {
                UserData.shared.setLanguage(language: "gu")
            }
            
            else
            {
                UserData.shared.setLanguage(language: "gu-IN")
            }
<<<<<<< HEAD:Babulime Lab/Login/ViewController.swift
=======
<<<<<<< HEAD:Babulime Lab/Login/ViewController.swift
=======
            
>>>>>>> 2c50902ba94781086bc63e41aa40a822616dc54c:Babulime Lab/ViewController.swift
>>>>>>> origin/master:Babulime Lab/ViewController.swift
        }else {
            UserData.shared.setLanguage(language: "en")
        }
        setLanguage()
    }

    @IBAction func onClickTouchID(_ sender: UIButton){
        let context = LAContext()
        var error: NSError?

        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!"

            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [weak self] success, authenticationError in

                DispatchQueue.main.async {
                    if success {
                        // navigate
                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                        appdelegate.rootToHome()
                    } else {
                        // error
                        self?.alert(title: "Authentication failed", message: "You could not be verified; please try again.")
                    }
                }
            }
        } else {
            // no biometry
            self.alert(title: "Biometry unavailable", message: "Your device is not configured for biometric authentication.")
        }
    }
    
    @objc func txt(sender:UITextField){
        self.btnLogin.isEnabled = txtEmail.text?.isEmpty == false && txtPswd.text?.isEmpty == false
//            && self.isValidEmail(emailStr: txtEmail.text ?? "")
    }
    
}


extension String
{
    func localizableString(loc: String) -> String
    {
        let path = Bundle.main.path(forResource: loc, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}
